<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $settings = Setting::find(MASTER_COMPANY_ID);

        if ($settings) {
            $config = array(
                'driver'     => $settings->transport_mail,
                'host'       => $settings->host_mail,
                'port'       => $settings->port_mail,
                'username'   => $settings->username_mail,
                'password'   => $settings->password_mail,
                'encryption' => null,
                'from'       => array('address' => $settings->username_mail, 'name' => "Master Key Education"),
                'sendmail'   => '/usr/sbin/sendmail -bs',
                'pretend'    => false,
            );

            Config::set('mail', $config);
        }
    }
}
