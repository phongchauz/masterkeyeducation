<?php

namespace App\Helpers;

use App\Company;
use App\FunctionMenu;
use App\Position;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Use_;

class MasterDataService
{



    public static function getFuntionTree($conditions = []) {

        $listFunction = FunctionMenu::where($conditions)->get();

        $data[] = [
            "id"        => 0,
            "parent_id" => -1,
            "name"      => __("All")
        ];

        foreach ($listFunction as $function){

            if($function->parent_id){continue;}

            $data[] = [
                "id" => $function->id,
                "parent_id" => 0,
                "type" => "function",
                "name" => $function->name,
                "data" => $function->toArray()
            ];

            foreach ($listFunction as $child) {

                if(!$child->parent_id){continue;}

                $data[] = [
                    "id" => $child->id,
                    "parent_id" => $child->parent_id,
                    "type" => "function",
                    "name" => $child->name,
                    "data" => $child->toArray()
                ];



            }

        }


        return BaseService::buildTreeMaster($data);
    }


}
