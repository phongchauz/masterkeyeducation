<?php


namespace App\Helpers;



use App\CashIncome;
use App\CashOutcome;
use App\CashPeriod;
use App\CashWallet;
use App\Company;
use App\FunctionMenu;
use App\User;
use Facade\FlareClient\Enums\MessageLevels;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\URL;
use InvalidArgumentException;
use function GuzzleHttp\Psr7\_caseless_remove;

class CashService
{

    public static function updateCashPeriod($user_id, $wallet_id, $month, $year, $type){

        $conditions = [
            ['user_id', $user_id],
            ['wallet_id', $wallet_id],
            ['month', $month],
            ['year', $year],
        ];

        $cashPeriod = CashPeriod::where($conditions)->first();

        if(!$cashPeriod){
            $cashPeriod = new CashPeriod();
            $cashPeriod->name = $month.'/'.$year;
            $cashPeriod->user_id = $user_id;
            $cashPeriod->wallet_id = $wallet_id;
            $cashPeriod->month = $month;
            $cashPeriod->year = $year;
            $cashPeriod->created_user = $user_id;
            $cashPeriod->save();
        }

        $field = "income";
        if($type == TYPE_CASH_INCOME){
            $listData = CashIncome::where($conditions)->get();
        }else{
            $field = "outcome";
            $listData = CashOutcome::where($conditions)->get();
        }

        $total = 0;
        foreach ($listData as $key => $item){
            $total += $item->amount;
        }

        $revenue = $type == TYPE_CASH_INCOME ? $total - $cashPeriod->outcome :
            $cashPeriod->income - $total;

        $cashPeriod->$field = $total;
        $cashPeriod->revenue = $revenue;
        $cashPeriod->updated_user = $user_id;
        $cashPeriod->save();

        return true;
    }

    public static function getWalletId(){
        $object = CashWallet::where('is_selected', STATUS_ACTIVE)->first();
        return $object ? $object->id : null;
    }






}
