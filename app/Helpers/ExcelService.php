<?php
/**
 * Author: chaunp
 * Date: 2020-03-24
 */

namespace App\Helpers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use InvalidArgumentException;
use PhpOffice\PhpSpreadsheet\Helper\Html;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\RichText;
class ExcelService
{
    public static function defaultInit($objPHPExcel, $sheetIndex = 0, $creator = null, $pageSize = null, $pageSetup = null){

        $creator = $creator ? $creator : "Z756";

        $pSize = PageSetup::PAPERSIZE_A4;
        if($pageSize){
            switch($pageSize){

                case PAGE_A3:
                    $pSize = PageSetup::PAPERSIZE_A3;
                    break;
                case PAGE_A5:
                    $pSize = PageSetup::PAPERSIZE_A5;
                    break;

            }
        }

        $pSetup = $pageSetup && $pageSetup == PAGE_ORIENTATION_LANDSCAPE ? PageSetup::ORIENTATION_LANDSCAPE : PageSetup::ORIENTATION_PORTRAIT;
        if($sheetIndex > 0) {
            $objPHPExcel->createSheet($sheetIndex);
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
        }

        $objWorkSheet = $objPHPExcel->getActiveSheet($sheetIndex);
        $objPHPExcel->getProperties ()->setCreator ( $creator );
        $objPHPExcel->getProperties ()->setLastModifiedBy ( $creator );
        $objPHPExcel->getProperties ()->setTitle ( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties ()->setSubject ( "Office 2007 XLSX Test Document" );
        $objPHPExcel->getProperties ()->setDescription ( "Test document for Office 2007 XLSX, generated using PHP classes." );
        $objPHPExcel->getProperties ()->setKeywords ( "office 2007 openxml php" );
        $objPHPExcel->getProperties ()->setCategory ( "Test result file" );

        $objWorkSheet->getPageSetup ()->setScale ( 100 );



        $objWorkSheet->getPageSetup ()->setPaperSize ( $pSize);
        $objWorkSheet->getPageSetup ()->setOrientation ( $pSetup );
        $objWorkSheet->getPageMargins ()->setTop ( 0.4 );
        $objWorkSheet->getPageMargins ()->setLeft ( 0.4 );
        $objWorkSheet->getPageMargins ()->setRight ( 0 );
        $objWorkSheet->getPageMargins ()->setBottom ( 0.2 );
        $objWorkSheet->getParent()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objWorkSheet->getParent()->getDefaultStyle()->getFont()->setSize(11);


        return $objWorkSheet;


    }

    public static function outputSpreadsheet($spreadsheet, $die = true, $fileName = "myfile.xlsx", $type = "Xlsx"){

        /* Here there will be some code where you create $spreadsheet */
        // redirect output to client browser
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, $type);
        ob_start ();
        $writer->save('php://output');

        if($die){
            ob_start ();
            die;
        }else{
            $content = ob_get_contents ();
            ob_end_clean ();

            $file = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode ( $content );
            return $file;
        }

    }

    /**
     * Style border
     * 0: parent
     * 1: child
     * 2: Last border
     * 3: Border cell like a formula
     */
    public static function renderStyleBorder($style){

        $arrStyle = array();
        switch($style) {
            case 0:
                $arrStyle = array(
                    'borders' => array(
                        'allBorders' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ),
                    )
                );
                break;
            case 1:
                $arrStyle = array(
                    'borders' => array(
                        'top' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT
                        ),
                        'bottom' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DASHDOT
                        ),
                        'left' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ),
                        'right' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ),
                        'vertical' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        )
                    )
                );
                break;
            case 2:
                $arrStyle = array(
                    'borders' => array(
                        'bottom' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        )
                    )
                );
                break;
            case 3:
                $arrStyle = array(
                    'borders' => array(
                        'top' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED
                        ),
                        'bottom' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED
                        ),
                        'left' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ),
                        'right' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ),
                        'vertical' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        )
                    )
                );
                break;
            case 4:
                $arrStyle = array(
                    'borders' => array(

                        'left' => array(
//							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                            'color' => array('rgb' => 'FFFFFF')
                        ),
                        'right' => array(
//							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                            'color' => array('rgb' => 'FFFFFF')
                        ),
                        'vertical' => array(
//							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    )
                );
                break;

        }

        return $arrStyle;

    }


    public static function outputExcel($spreadsheet, $fileName = "myfile.xlsx"){

        $writer = new Xlsx($spreadsheet);
        $writer->save($fileName);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Length: ' . filesize($fileName));
        header('Content-Disposition: attachment; filename='.$fileName);
        readfile($fileName); // send file
        unlink($fileName); // delete file

    }


    public static function setDefaultStyle($sheet){
        $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
        $sheet->getPageMargins()->setTop(0.5);
        $sheet->getPageMargins()->setBottom(0.5);
        $sheet->getPageMargins()->setLeft(0.25);
        $sheet->getPageMargins()->setRight(0.25);

        $sheet->getParent()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $sheet->getParent()->getDefaultStyle()->getFont()->setSize(11);
    }

    public static function convertHtmlToText($htmlContent) {
        $html = new Html();
        $run = new RichText\Run();
        $run->getFont()->setName("Times New Roman");
        $run->getFont()->setSize(13);
        return $html->toRichTextObject($htmlContent);
    }
}
