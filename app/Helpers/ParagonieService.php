<?php
/**
 * Author: chaunp
 * Date: 2020-03-24
 */

namespace App\Helpers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use InvalidArgumentException;

use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
use ParagonIE\HiddenString\HiddenString;
class ParagonieService
{

    public static function createKey(){
        $encKey = KeyFactory::generateEncryptionKey();
        KeyFactory::save($encKey, '/public/encryption.key');
    }

    public static function encrypt($string, $key){

    }
}
