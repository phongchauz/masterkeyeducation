<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\MasterDataService;
use App\Position;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class PositionController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Position List");

        $dataTree = MasterDataService::getFuntionTree();

        return view('Admin.Position.index', compact(
            'dataTree',
            'title'

        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = Position::orderBy($fieldOrder, $orderType)
            ->select(
                "positions.*"
            )
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('positions.code', 'like', "%$value%")
                    ->orWhere('positions.name', 'like', "%$value%")
                ;
            });


        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $company_id = BaseService::getCompanyId();

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $code          = isset($data['code']) ? $data['code'] : null;
        $name          = isset($data['name']) ? $data['name'] : null;

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ["code", $code];
        $conditions[] = ["company_id", $company_id];

        if($id) {
            $object = Position::find($id);
            $conditions[] = ["id", "<>", $id];
            $data['updated_user']  = $authUserId;

        } else {
            $object = new Position();
            $data['company_id']  = $company_id;
            $data['created_user']  = $authUserId;
        }

        $exist = Position::where($conditions)->get()->count();

        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_EXIST_CODE)
            ]);
        }


        if(empty($code) || empty($name)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }


        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = Position::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }

    public function saveAccess(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $company_id = BaseService::getCompanyId();

        $data           = $request->all();


        $id = isset($data['id']) ? $data['id'] : null;

        $object = Position::find($id);

        if(!$object) {
            return json_encode([
                "success" => false,
                "message" => __("Position not found!")
            ]);
        }

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __("Save access function failed!")
            ]);
        }


        return json_encode([
            'success'       => true,
            'message'       => __("Save access function successfully!")
        ]);


    }
    

}
