<?php

namespace App\Http\Controllers\Admin;

use App\ClassRoom;
use App\Course;
use App\Helpers\MasterDataService;
use App\ClassRoomRegistration;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class ClassRoomRegistrationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("ClassRoom Registration List");

        $listCourse = Course::where('status', STATUS_ACTIVE)->get()->toArray();
        $course_id = count($listCourse) ? $listCourse[0]['id'] : -1;
        $listClassroom = ClassRoom::where('course_id', $course_id)->where('status', STATUS_ACTIVE)->get()->toArray();

        return view('Admin.ClassRoomRegistration.index', compact(
            'listCourse',
            'listClassroom',
            'title'

        ));
    }

    public function search (Request $request) {

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $data = $request->all();

        $key_search     = $data['key_search'] ?? '';
        $course_id      = $data['course_id'] ?? 0;
        $status      = $data['status'] ?? -1;
        $class_room_id  = $data['class_room_id'] ?? 0;

        $list = ClassRoomRegistration::orderBy($fieldOrder, $orderType)
            ->select(
                "class_room_registrations.*",
                "class_rooms.code",
                "courses.name as course_name",
                "teachers.name as teacher_name"
            )
            ->leftJoin('class_rooms', 'class_rooms.id', 'class_room_registrations.class_room_id')
            ->leftJoin('teachers', 'teachers.id', 'class_rooms.teacher_id')
            ->leftJoin('courses', 'courses.id', 'class_rooms.course_id')
        ;

        if($course_id){
            $list = $list->where('class_room_registrations.course_id', $course_id);
        }

        if($class_room_id){
            $list = $list->where('class_room_registrations.class_room_id', $class_room_id);
        }

        if($status != -1){
            $list = $list->where('class_room_registrations.status', $status);
        }

        if ($key_search) {

            $list = $list->where( function ( $query ) use ($key_search){
                $query->where('class_room_registrations.fullname', 'like', "%$key_search%")
                    ->orWhere('class_room_registrations.phone', 'like', "%$key_search%")
                    ->orWhere('class_room_registrations.contact_email', 'like', "%$key_search%")
                    ->orWhere('class_room_registrations.message', 'like', "%$key_search%")
                ;
            });

        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function changeData(Request $request){

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data           = $request->all();
        $type = isset($data['type']) ? $data['type'] : null;
        $value_change = isset($data['value_change']) ? $data['value_change'] : null;


        $contentClassroomHtml = "";


        switch ($type){
            case 's_course_id':
                $conditions = [];
                if($value_change){$conditions[] = ["class_rooms.course_id", $value_change];}

                $listClassroom = ClassRoom::where($conditions)->get();

                $contentClassroomHtml = view('Elements.cbb', [
                    'domeHtml' 		=> 's_class_room_id',
                    'nameHtml' 		=> 's_class_room_id',
                    'displayField'  => 'code',
                    'isAll'  	    => true,
                    'valueField'  	=> 'id',
                    'listData' 		=> $listClassroom,
                ])->render();



                break;


        }





        return json_encode([
            'success'               => true,
            'type'                  => $type,
            'contentClassroomHtml'  => $contentClassroomHtml,
        ]);

    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $company_id = BaseService::getCompanyId();

        $data = $request->all();

        $id     = $data['id'] ?? -1;
        $status = $data['status'] ?? STATUS_INACTIVE;

        $object = ClassRoomRegistration::find($id);

        if(!$object) {
            return json_encode([
                "success" => false,
                "message" => __("Classroom registration information not found")
            ]);
        }


        $object->status = $status;
        $object->updated_user = $authUserId;

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        /**
         * Save log
         */
        $controllerName = substr(class_basename(Route::current()->controller), 0, -10);
        $contentLog = __("User: ")." <b>".$authUser->name."</b> - ID: ".$authUserId." <br>"
            .__("Approval Classroom Registration: ")." <br>"
            .__("Value: ")." <b>".json_encode($object, JSON_PRETTY_PRINT)."</b> "." <br>"
        ;
        $arrData = [];
        $arrData['user_id']      = $authUserId;
        $arrData['action_id']    = ACTION_UPDATE;
        $arrData['screen_name']  = BaseService::getFunctionName($controllerName, MODULE_ADMIN);
        $arrData['content']      = $contentLog;

        BaseService::writeLogSystem($arrData);

        return json_encode([
            'success'       => true,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = ClassRoomRegistration::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }

    public function saveAccess(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $company_id = BaseService::getCompanyId();

        $data           = $request->all();


        $id = isset($data['id']) ? $data['id'] : null;

        $object = ClassRoomRegistration::find($id);

        if(!$object) {
            return json_encode([
                "success" => false,
                "message" => __("ClassRoomRegistration not found!")
            ]);
        }

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __("Save access function failed!")
            ]);
        }


        return json_encode([
            'success'       => true,
            'message'       => __("Save access function successfully!")
        ]);


    }
    

}
