<?php

namespace App\Http\Controllers\Admin;

use App\SiteSetting;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class SiteSettingController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Site Label Setting");

        $site_setting = SiteSetting::where('company_id', MASTER_COMPANY_ID)->first();
        if(!$site_setting){
            $site_setting = new SiteSetting();
            $site_setting->company_id = MASTER_COMPANY_ID;
            $site_setting->status = STATUS_ACTIVE;
            $site_setting->save();
        }

        return view('Admin.SiteSetting.index', compact(
            'site_setting',
            'title'

        ));
    }


    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;


        $listColumnFile = [
            ['column' => "slogan_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/SloganPicture/"],
            ['column' => "mission_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/MissionPicture/"],
            ['column' => "registration_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/RegistrationPicture/"],
            ['column' => "teacher_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/TeacherPicture/"],
            ['column' => "pricing_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/PricingPicture/"],
            ['column' => "course_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/CoursePicture/"],
            ['column' => "experience_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/ExperiencePicture/"],
            ['column' => "comment_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/CommentPicture/"],
            ['column' => "registration_form_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/RegistrationFormPicture/"],
            ['column' => "blog_picture", "path_upload" => public_path()."/uploads/".$company_id."/SiteSetting/BlogPicture/"],

        ];

        $listColumnCheck = [
            'slogan_display_homepage',
            'mission_display_homepage',
            'registration_display_homepage',
            'teacher_display_homepage',
            'course_display_homepage',
            'experience_display_homepage',
            'comment_display_homepage',
            'registration_form_display_homepage',
            'pricing_display_homepage',
            'blog_display_homepage',
        ];

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;

        foreach ($listColumnFile as $cKey => $column_file){
            $column = $column_file['column'];
            $path_upload = $column_file['path_upload'];

            $file_upload = isset($data[$column]) ? $data[$column] : [];

            if($file_upload){
//                $fileName   = $file_upload->getClientOriginalName();
                $fileExt   = $file_upload->getClientOriginalExtension();
                $fileName = time().'c'.$cKey.".".$fileExt;
                $data[$column]  = $fileName;
                $file_upload->move($path_upload, $fileName);
            }

        }

        foreach ($listColumnCheck as $column_check){
            $value_check = isset($data[$column_check]) && $data[$column_check] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
            $data[$column_check]  = $value_check;
        }

        $object = SiteSetting::find($id);

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success'       => true,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }




}
