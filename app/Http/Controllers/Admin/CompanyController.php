<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Helpers\BaseService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Module;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class CompanyController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index() {
        $title = __("Company List");

        return view('Admin.Company.index', ['title' => $title]);
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = Company::orderBy($fieldOrder, $orderType)
            ->select(
                "companies.*"
            )
        ;

        if ($value) {
            $list = $list
                ->where('companies.code', 'like', "%$value%")
                ->orWhere('companies.name', 'like', "%$value%");
        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function profile($page = null) {

        $page = $page ? $page : "general";

        $title = trans("Company profile");

        $company = Company::find(MASTER_COMPANY_ID)->toArray();

        $listModules = [];
        $setting = [];

        switch ($page){
            case 'modules':
                $listModules = Module::where('status', STATUS_ACTIVE)
                    ->get();
                break;
            case 'sns':
            case 'mailer':
                $setting = Setting::where('company_id', MASTER_COMPANY_ID)
                    ->first();
                if(!$setting){
                    $setting = new Setting();
                    $setting->company_id = MASTER_COMPANY_ID;
                    $setting->save();
                }
                break;
        }

        return view('Admin.Company.profile', [
            'company_id'    => MASTER_COMPANY_ID,
            'company'       => $company,
            'listModules'   => $listModules,
            'setting'       => $setting,
            'page'          => $page,
            'title'         => $title
        ]);
    }

    public function saveData(Request $request) {

        $data = $request->all();

        $id             = MASTER_COMPANY_ID;
        $subject        = isset($data['subject']) ? $data['subject'] : null;
        $phone_number   = isset($data['phone_number']) ? $data['phone_number'] : null;
        $email          = isset($data['email']) ? $data['email'] : null;
        $address        = isset($data['address']) ? $data['address'] : null;

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $logo           = isset($data['logo']) ? $data['logo'] : [];
        $logoName       = $logo ? $logo->getClientOriginalName() : '';

        if(!empty($logoName)){
            $data['logo'] = $logoName;
        }

        unset($data['continue']);
        $data['status'] = $status;

        $object = new Company();
        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        $id = $object->id;
        if(
            !empty($logoName) &&
            isset($logo) &&
            $logo->getSize() > 0
        ){
            $path = public_path().'/uploads/'.$id.'/logo/';
            $logo->move($path, $logoName);
        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);

    }

    public function saveGeneral(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data       = $request->all();
        $id         = MASTER_COMPANY_ID;
        $name       = $data['name'] ?? null;
        $logo       = $data['logo'] ?? [];
        $favicon    = $data['favicon'] ?? [];

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;

        if(empty($name)){
            return json_encode([
                'success'       => false,
                'message'       => __("Data required can not be null")
            ]);
        }

        $object = Company::find($id);
        $data['updated_user'] = $authUserId;

        if(!$object){
            return json_encode([
                'success'       => false,
                'message'       => __("Can not detect company information")
            ]);
        }

        $fileName = "";
        if($logo){
            $fileName   = $logo->getClientOriginalName();
            $data['logo']  = $fileName;
        }

        $faviconName = "";
        if($favicon){
            $faviconName   = $favicon->getClientOriginalName();
            $data['favicon']  = $faviconName;
        }

        unset($data['continue']);

        $data['id']         = $id;
        $data['status']     = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __("Save data failed")
            ]);
        }


        if($logo){
            /** Upload file */
            $path = public_path()."/uploads/".$id.'/Logo/';
            $logo->move($path, $fileName);
        }

        if($favicon){
            /** Upload file */
            $path = public_path()."/uploads/".$id.'/Favicon/';
            $favicon->move($path, $faviconName);
        }

        return json_encode([
            'success'       => true,
            'id'            => $id,
            'message'       => __("Save data successfully")
        ]);


    }

    public function deletePicture($type) {

        $company = Company::all()->take(1)->toArray();
        $id         = $company[0]['id'];

        $favicon    = isset($company[0]['favicon']) ? $company[0]['favicon'] : '';
        $logo       = isset($company[0]['logo']) ? $company[0]['logo'] : '';
        $banner     = isset($company[0]['banner']) ? $company[0]['banner'] : '';
        $filePath   = "";

        switch($type){
            case 'logo':
                $filePath = public_path()."/uploads/company/logo/".$logo;
                break;
            case 'favicon':
                $filePath = public_path()."/uploads/company/favicon/".$favicon;
                break;
            case 'banner':
                $filePath = public_path()."/uploads/company/banner/".$banner;
                break;
        }

        if(empty($filePath)){
            return json_encode([
                "success"  => false,
            ]);
        }

        File::delete($filePath);
        $companyInfo = Company::find($id);
        $companyInfo[$type] = '';
        if($companyInfo->save()) {
            return json_encode([
                "success" => true
            ]);
        }

    }

    public function dashboard()
    {
        $title = trans("Dashboard");

        return view('Admin.Company.dashboard', compact('title'));
    }


    public function updateMailerSetting(Request $request) {


        $data = $request->all();

        $company_id = isset($data['company_id']) ? $data['company_id'] : -1;
        $host_mail = isset($data['host_mail']) ? $data['host_mail'] : null;
        $port_mail = isset($data['port_mail']) ? $data['port_mail'] : null;
        $username_mail = isset($data['username_mail']) ? $data['username_mail'] : null;
        $password_mail = isset($data['password_mail']) ? $data['password_mail'] : null;
        $transport_mail = isset($data['transport_mail']) ? $data['transport_mail'] : null;

        if(
            empty($company_id) ||
            empty($host_mail) ||
            empty($port_mail) ||
            empty($username_mail) ||
            empty($password_mail) ||
            empty($transport_mail)
        ){
            return json_encode([
                "success"  => false,
                "message"  => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        $object = Setting::where('company_id', $company_id)->first();

        if(!$object){
            $object = new Setting();
        }

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            $smsSuccess = trans('Save change successfully');
            return json_encode([
                "success"           => true,
                "message"             => $smsSuccess
            ]);
        }

    }

    public function updateSNSSetting(Request $request) {


        $data = $request->all();

        $id = isset($data['id']) ? $data['id'] : -1;

        $object = Setting::where('id', $id)->first();

        if(!$object){
            return json_encode([
                "success"           => false,
                "message"             => __("Setting not found")
            ]);
        }

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            $smsSuccess = trans('Save change successfully');
            return json_encode([
                "success"           => true,
                "message"             => $smsSuccess
            ]);
        }
        return json_encode([
            "success"           => false,
            "message"             => __("Save data failed")
        ]);
    }

    public function updateSocial(Request $request) {


        $data = $request->all();

        $id = isset($data['id']) ? $data['id'] : -1;

        $object = Company::where('id', $id)->first();

        if(!$object){
            return json_encode([
                "success"           => false,
                "message"             => __("Company not found")
            ]);
        }

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            $smsSuccess = trans('Save change successfully');
            return json_encode([
                "success"           => true,
                "message"             => $smsSuccess
            ]);
        }
        return json_encode([
            "success"           => false,
            "message"             => __("Save data failed")
        ]);
    }

    public function sendTestMail(Request $request)
    {

        $data = $request->all();
        $email = isset($data['email']) ? trim($data['email']) : '';
        if (empty($email)) {
            return json_encode([
                "success" => false,
                "message" => __("Email not empty"),
            ]);
        }

        $data_email = [
            'to_email' => $email,
            'template' => "Elements.EmailTemplates.test_mail",
        ];
        BaseService::sendEmail($data_email);

        return json_encode([
            "success" => true,
            "message" => __("Mail was sent successfully"),
        ]);
    }
}
