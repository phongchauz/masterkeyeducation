<?php

namespace App\Http\Controllers\Admin;

use App\CoreValue;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CoreValueController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Core Value List");


        return view('Admin.CoreValue.index', compact(
            'title'

        ));
    }

    public function search (Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = CoreValue::orderBy($fieldOrder, $orderType)
            ->select(
                "core_values.*"
            )
            ->where('core_values.company_id', $company_id)
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('core_values.title', 'like', "%$value%")
                    ->orWhere('core_values.summary', 'like', "%$value%")
                    ->orWhere('core_values.slug', 'like', "%$value%")
                ;
            });



        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $picture        = isset($data['picture']) ? $data['picture'] : [];

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;


        if($id) {
            $object = CoreValue::find($id);
            $data['updated_user']  = $authUserId;

        } else {
            $object = new CoreValue();
            $data['company_id']  = $company_id;
            $data['created_user']  = $authUserId;
        }



        $fileName = "";
        if($picture){
            $fileName   = $picture->getClientOriginalName();
            $mimeType   = $picture->getClientMimeType();
            $extension  = $picture->getClientOriginalExtension();
            $size       = $picture->getSize();
            $data['picture']  = $fileName;
        }




        unset($data['continue']);

        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($picture){
            /** Upload file */
            $path = public_path()."/uploads/".$company_id."/CoreValue/".$object->id.'/Picture/';
            $picture->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = CoreValue::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
