<?php

namespace App\Http\Controllers\Admin;

use App\Site;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class SiteController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function introduction()
    {
        $title = trans("Introduction");

        $site = Site::find(1);

        return view('Admin.Site.introduction', compact(
            'site',
            'title'

        ));
    }


    public function saveIntroduction(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data           = $request->all();
        $introduction_title             = isset($data['introduction_title']) ? $data['introduction_title'] : null;
        $id             = isset($data['id']) ? $data['id'] : null;
        $introduction_picture        = isset($data['introduction_picture']) ? $data['introduction_picture'] : [];

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;



        if($id) {
            $object = Site::find($id);
            $data['updated_user']  = $authUserId;

        } else {
            $object = new Site();
            $data['created_user']  = $authUserId;
        }

        $fileName = "";
        if($introduction_picture){
            $fileName   = $introduction_picture->getClientOriginalName();
            $mimeType   = $introduction_picture->getClientMimeType();
            $extension  = $introduction_picture->getClientOriginalExtension();
            $size       = $introduction_picture->getSize();
            $data['introduction_picture']  = $fileName;
        }

        if(empty($introduction_title)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['introduction_status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($introduction_picture){
            /** Upload file */
            $path = public_path()."/uploads/Site/".$object->id.'/Introduction/';
            $introduction_picture->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }



}
