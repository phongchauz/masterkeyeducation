<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\CourseGroup;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CourseController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Course List");

        $listCourseGroup = CourseGroup::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        return view('Admin.Course.index', compact(
            'listCourseGroup',
            'title'
        ));
    }

    public function search (Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = Course::orderBy($fieldOrder, $orderType)
            ->select(
                "courses.*",
                "course_groups.name as course_group_name"
            )
            ->leftJoin("course_groups", "course_groups.id", "courses.course_group_id")
            ->where('courses.company_id', $company_id)
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('courses.name', 'like', "%$value%")
                    ->orWhere('courses.summary', 'like', "%$value%")
                    ->orWhere('courses.slug', 'like', "%$value%")
                ;
            });



        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data    = $request->all();
        $id      = isset($data['id']) ? $data['id'] : null;
        $slug    = isset($data['slug']) ? $data['slug'] : null;
        $name    = isset($data['name']) ? $data['name'] : null;
        $picture = isset($data['picture']) ? $data['picture'] : [];
        $cover   = isset($data['cover']) ? $data['cover'] : [];

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isPopular = isset($data['is_popular']) && $data['is_popular'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;
        $displayHomepage = isset($data['display_homepage']) && $data['display_homepage'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ['company_id', $company_id];
        $conditions[] = ['slug', $slug];

        if($id) {
            $object = Course::find($id);
            $conditions[] = ["id", "<>", $id];
            $data['updated_user']  = $authUserId;

        } else {
            $object = new Course();
            $data['company_id']  = $company_id;
            $data['created_user']  = $authUserId;
        }

        $exist = Course::where($conditions)->get()->count();
        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __("Slug already exists")
            ]);
        }

        $fileName = "";
        if($picture){
            $fileName   = $picture->getClientOriginalName();
            $mimeType   = $picture->getClientMimeType();
            $extension  = $picture->getClientOriginalExtension();
            $size       = $picture->getSize();
            $data['picture']  = $fileName;
        }

        $coverName = "";
        if($cover){
            $coverName   = $cover->getClientOriginalName();
            $data['cover']  = $coverName;
        }

        if(empty($name)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['is_popular'] = $isPopular;
        $data['display_homepage'] = $displayHomepage;
        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($picture){
            /** Upload file */
            $path = public_path()."/uploads/".$company_id."/Course/".$object->id.'/Picture/';
            $picture->move($path, $fileName);
        }

        if($cover){
            /** Upload file */
            $pathCover = public_path()."/uploads/".$company_id.'/Course/'.$object->id.'/Cover/';
            $cover->move($pathCover, $coverName);

        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = Course::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
