<?php

namespace App\Http\Controllers\Admin;

use App\TourType;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class TourTypeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Tour Type List");


        return view('Admin.TourType.index', compact(
            'title'

        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = TourType::orderBy($fieldOrder, $orderType)
            ->select(
                "tour_types.*"
            )
        ;

        if ($value) {
            $list = $list
                ->where('tour_types.slug', 'like', "%$value%")
                ->orWhere('tour_types.name', 'like', "%$value%");
        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $slug           = isset($data['slug']) ? $data['slug'] : null;
        $name           = isset($data['name']) ? $data['name'] : null;
        $image        = isset($data['image']) ? $data['image'] : [];

        $is_show_menu     = isset($data['is_show_menu']) && $data['is_show_menu'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $display_homepage     = isset($data['display_homepage']) && $data['display_homepage'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ["slug", $slug];

        if($id) {
            $object = TourType::find($id);
            $conditions[] = ["id", "<>", $id];
            $data['updated_user']  = $authUserId;

        } else {
            $object = new TourType();
            $data['created_user']  = $authUserId;
        }

        $fileName = "";
        if($image){
            $fileName   = $image->getClientOriginalName();
            $mimeType   = $image->getClientMimeType();
            $extension  = $image->getClientOriginalExtension();
            $size       = $image->getSize();
            $data['image']  = $fileName;
        }

        $exist = TourType::where($conditions)->get()->count();

        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_EXIST_SLUG)
            ]);
        }

        if(empty($slug) || empty($name)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['status'] = $status;
        $data['display_homepage'] = $display_homepage;
        $data['is_show_menu'] = $is_show_menu;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($image){
            /** Upload file */
            $path = public_path()."/uploads/TourType/".$object->id.'/';
            $image->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = TourType::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }


    public function changeCheckData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data           = $request->all();
        $id    = isset($data['id']) ? $data['id'] : null;
        $field = isset($data['field']) ? $data['field'] : null;

        $object     = TourType::where('id', $id)->first();

        if(!$object) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_OBJECT_NOT_FOUND)
            ]);
        }

        $current_value = $object->{$field};
        $new_value = $current_value ? STATUS_INACTIVE : STATUS_ACTIVE;

        $object->{$field} = $new_value;
        $object->updated_user = $authUserId;

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        $message = __("Change value successfully!");

        return json_encode([
            'success'   => true,
            'message'   => $message,
        ]);


    }

}
