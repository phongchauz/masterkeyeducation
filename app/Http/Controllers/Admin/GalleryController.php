<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Gallery;
use App\Helpers\BaseService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class GalleryController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index()
    {
        $title = trans("Gallery List");

        return view('Admin.Gallery.index', compact(
            'title'
        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $data = $request->all();

        $product_id = $data['product_id'] ?? -1;

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = Gallery::orderBy($fieldOrder, $orderType)
            ->select(
                "galleries.*"
            )
        ;

        if ($value) {
            $list = $list
                ->where('galleries.picture', 'like', "%$value%")
                ->orWhere('galleries.description', 'like', "%$value%")
            ;
        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => $recordsTotal ?? count($results),
            'recordsFiltered'   => $recordsTotal ?? count($results)
        ], JSON_NUMERIC_CHECK);
    }


    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data         = $request->all();
        $galleries    = $data['galleries'] ?? [];

        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        foreach ($galleries as $key => $item){

            if(!$item){continue;}
            $fileName = "PG_".$key.time().".".$item->getClientOriginalExtension();

            $object = new Gallery();
            $object->company_id     = $company_id;
            $object->picture        = $fileName;
            $object->status         = STATUS_ACTIVE;
            $object->created_user   = $authUserId;
            $object->save();

            $path = public_path()."/uploads/".$company_id."/Galleries/".$object->id.'/';
            $item->move($path, $fileName);


        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __("Save data successfully")
        ]);


    }



    public function delete(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $id = $request['id'];
        $object = Gallery::find($id);
        $product_id = $object->product_id;
        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __("Can not detect this data")
            ]);
        }

        if(!$object->delete()) {
            return json_encode([
                'success' => false,
                'message' => __("Delete failed!")
            ]);
        }

        /**
         * Xóa toàn bộ uploads của company
         */
        $path = public_path()."/uploads/".$company_id."/Product/".$product_id."/Galleries/".$object->id.'/';
        BaseService::deleteDir($path);


        return json_encode([
            'success' => true,
            'message' => __("Delete successfully")
        ]);
    }

    public function deleteAll(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $product_id = $request['product_id'];

        $count = Gallery::count();

        Gallery::getQuery()->truncate();

        /**
         * Xóa toàn bộ uploads
         */
        $path = public_path()."/uploads/".$company_id."/Galleries/";
        BaseService::deleteDir($path);


        return json_encode([
            'success' => true,
            'message' => $count." ".__("rows have been deleted!")
        ]);
    }

}
