<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\ProductType;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class ProductController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Product List");

        $listProductType = ProductType::where('status', STATUS_ACTIVE)->get()->toArray();
        $listUnit = Unit::where('status', STATUS_ACTIVE)->get()->toArray();

        return view('Admin.Product.index', compact(
            'listProductType',
            'listUnit',
            'title'

        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $data = $request->all();

        $product_type_id = isset($data['product_type_id']) ? (int)$data['product_type_id'] : null;
        $search_text = isset($data['search_text']) ? trim($data['search_text']) : null;

        $list = Product::orderBy($fieldOrder, $orderType)
            ->select(
                "products.*",
                "units.name as unit_name",
                "product_types.name as product_type_name"
            )
            ->leftJoin("product_types", "product_types.id", "products.product_type_id")
            ->leftJoin("units", "units.id", "products.unit_id")
        ;

        if($product_type_id){
            $list = $list->where('product_type_id', $product_type_id);
        }

        if ($search_text) {

            $list = $list->where( function ( $query ) use ($search_text){
                $query->where('products.code', 'like', "%$search_text%")
                    ->orWhere('products.name', 'like', "%$search_text%")
                    ->orWhere('products.slug', 'like', "%$search_text%")
                ;
            });

        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $code           = isset($data['code']) ? $data['code'] : null;
        $name           = isset($data['name']) ? $data['name'] : null;
        $slug           = isset($data['slug']) ? $data['slug'] : null;
        $picture        = isset($data['picture']) ? $data['picture'] : [];

        $price = isset($data['price']) ? $data['price'] : 0;
        $price = str_replace(',', '', $price);

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ["code", $code];
        $conditions[] = ["company_id", $company_id];

        $is_new = true;

        if($id) {
            $object = Product::find($id);
            $data['updated_user']  = $authUserId;

            $conditions[] = ["id", "<>", $id];
            $is_new = false;
        } else {
            $object = new Product();
            $data['created_user']  = $authUserId;
            $data['company_id'] = $company_id;
        }

        $exist = Product::where($conditions)->get()->count();

        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_EXIST_CODE)
            ]);
        }

        $fileName = "";
        if($picture){
            $fileName   = $picture->getClientOriginalName();
            $mimeType   = $picture->getClientMimeType();
            $extension  = $picture->getClientOriginalExtension();
            $size       = $picture->getSize();
            $data['picture']  = $fileName;
        }


        unset($data['continue']);

        $data['price'] = $price;
        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($picture){
            /** Upload file */
            $path = public_path()."/uploads/".$company_id.'/Product/'.$object->id.'/';
            $picture->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'is_new'        => $is_new,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = Product::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }

    public function saveImportData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data           = $request->all();


        $file = isset($data['file']) ? $data['file'] : [];

        if(empty($file) || !$file->getSize()) {
            return json_encode([
                "success" => false,
                "message" => __("Tập tin tải lên không hợp lệ")
            ]);
        }

        $fileName   = $file->getClientOriginalName();
        $mimeType   = $file->getClientMimeType();
        $extension  = $file->getClientOriginalExtension();
        $size       = $file->getSize();

        $path = public_path()."/uploads/".$company_id.'/Product/files/';

        $file->move($path, $fileName);

        $pathFile = $path.$fileName;

        try {

            $ext = pathinfo($pathFile, PATHINFO_EXTENSION);

            if(strtolower($ext) == 'xlsx'){
                $reader =  new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }else{
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }

            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($pathFile);

        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($pathFile, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //  Get data default

        $listProduct = Product::get()->pluck('id', 'code')->toArray();
        $listProductType = ProductType::where('status', STATUS_ACTIVE)->get()->pluck('id', 'code')->toArray();
        $listUnit = Unit::where('status', STATUS_ACTIVE)->get()->pluck('id', 'code')->toArray();


        //  Get worksheet dimensions


        $objWorksheet   = $spreadsheet->getSheet(0);
        $highestRow     = $objWorksheet->getHighestRow();

        $colB  = 0;
        $colC  = 1;
        $colD  = 2;
        $colE  = 3;
        $colF  = 4;





        $beginRow   = 5;

        $arrDataError = [];
        $totalInsert = 0;
        $totalUpdate = 0;

        $list = [];
        for($row = $beginRow; $row <= $highestRow; $row++){

            $rowData   = $objWorksheet->rangeToArray("B$row:M$row", NULL, TRUE, FALSE);

            $code               = trim($rowData[0][$colB]);
            $name               = trim($rowData[0][$colC]);
            $unit_code          = trim($rowData[0][$colD]);
            $product_type_code  = trim($rowData[0][$colE]);
            $description        = trim($rowData[0][$colF]);

            if(empty($code)){
                array_push($arrDataError, $row);
                continue;
            }

            $unit_id         = isset($listUnit[$unit_code]) ? $listUnit[$unit_code] : null;
            $product_type_id = isset($listProductType[$product_type_code]) ? $listProductType[$product_type_code] : null;
            $id              = isset($listProduct[$code]) ? $listProduct[$code] : null;

            $data = [
                'company_id'        => $company_id,
                'code'              => $code,
                'slug'              => $code,
                'name'              => $name,
                'unit_id'           => $unit_id,
                'product_type_id'   => $product_type_id,
                'description'       => $description,

            ];

            if(!empty($id)){
                $data['id'] = $id;
            }

            array_push($list, $data);

        }


        $totalInsert = 0;
        $totalUpdate = 0;
        foreach($list as $key => $item){
            if(isset($item['id'])){
                $id = $item['id'];
                $object = Product::find($id);
                $totalUpdate++;
            }else{
                $object = new Product();
                $totalInsert++;
            }

            $object = BaseService::renderObject($object, $item);

            $object->save();

        }

        $message = "";
        if($totalInsert != 0){
            $message .= "* ".__('Number of rows inserted').": <b>".$totalInsert."</b>.";
        }

        if($totalUpdate != 0){
            $message .= "<br/>* ".__('Number of rows updated').": <b>".$totalUpdate."</b>.";
        }

        return json_encode([
            'success'       => true,
            'message'       => $message
        ]);
    }
    

}
