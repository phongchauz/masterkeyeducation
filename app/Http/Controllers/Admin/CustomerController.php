<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\FunctionMenu;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Module;
use App\Role;
use App\RoleAccess;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CustomerController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Danh sách khách hàng");

        return view('Admin.Customer.index', compact(
            'title'
        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = Customer::orderBy($fieldOrder, $orderType);

        if ($value) {
            $list = $list
                ->where('code', 'like', "%$value%")
                ->orWhere('name', 'like', "%$value%");
        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {
        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : '';
        $code           = isset($data['code']) ? $data['code'] : '';

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ["code", $code];

        if($id) {
            $object = Customer::find($id);
            $conditions[] = ["id", "<>", $id];
        } else {
            $object = new Customer();

        }
        $exist = Customer::where($conditions)->get()->count();

        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_EXIST_CODE)
            ]);
        }

        unset($data['continue']);

        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            return json_encode([
                'success'       => true,
                'is_continue'   => $isContinue,
                'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success'       => false,
            'message'       => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = Customer::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }



}
