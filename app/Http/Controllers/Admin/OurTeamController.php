<?php

namespace App\Http\Controllers\Admin;

use App\OurTeam;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class OurTeamController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("OurTeam List");


        return view('Admin.OurTeam.index', compact(
            'title'

        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = OurTeam::orderBy($fieldOrder, $orderType)
            ->select(
                "our_teams.*"
            )
        ;

        if ($value) {
            $list = $list
                ->where('our_teams.fullname', 'like', "%$value%")
                ->orWhere('our_teams.position', 'like', "%$value%")
            ;

        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $fullname          = isset($data['fullname']) ? $data['fullname'] : null;
        $picture        = isset($data['picture']) ? $data['picture'] : [];

        $is_default     = isset($data['is_default']) && $data['is_default'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;


        if($id) {
            $object = OurTeam::find($id);
            $data['updated_user']  = $authUserId;

        } else {
            $object = new OurTeam();
            $data['created_user']  = $authUserId;
        }

        $fileName = "";
        if($picture){
            $fileName   = $picture->getClientOriginalName();
            $mimeType   = $picture->getClientMimeType();
            $extension  = $picture->getClientOriginalExtension();
            $size       = $picture->getSize();
            $data['picture']  = $fileName;
        }


        if(empty($fullname)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['status'] = $status;
        $data['is_default'] = $is_default;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        if($picture){
            /** Upload file */
            $path = public_path()."/uploads/OurTeam/".$object->id.'/';
            $picture->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = OurTeam::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }


    

}
