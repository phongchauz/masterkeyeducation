<?php

namespace App\Http\Controllers\Admin;

use App\CoreValue;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CoreDataController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }



    public function renderSlug(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data  = $request->all();
        $str   = isset($data['str']) ? trim($data['str']) : null;
        $table = isset($data['table']) ? trim($data['table']) : null;

        if(empty($str)){
            return "";
        }

        $str = strtolower($str);
        $str = str_replace(' ', '-', $str);

        if($table){

        }

        return json_encode([
            'success'   => true,
            'slug'      => $str,
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = CoreValue::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
