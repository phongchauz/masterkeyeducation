<?php

namespace App\Http\Controllers\Admin;

use App\ClassRoom;
use App\Course;
use App\Helpers\BaseService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Teacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class ClassRoomController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Class Room List");
        $company_id = BaseService::getCompanyId();
        $listTeacher = Teacher::where('company_id', $company_id)->where('status', STATUS_ACTIVE)->get();
        $listCourse = Course::where('company_id', $company_id)->where('status', STATUS_ACTIVE)->get();

        return view('Admin.ClassRoom.index', compact(
            'listTeacher',
            'listCourse',
            'title'
        ));
    }

    public function search (Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = ClassRoom::orderBy($fieldOrder, $orderType)
            ->select(
                "class_rooms.*",
                "teachers.name as teacher_name",
                "courses.name as course_name"
            )
            ->leftJoin("teachers", "teachers.id", "class_rooms.teacher_id")
            ->leftJoin("courses", "courses.id", "class_rooms.course_id")
            ->where('class_rooms.company_id', $company_id)
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('courses.name', 'like', "%$value%")
                    ->orWhere('courses.summary', 'like', "%$value%")
                    ->orWhere('courses.slug', 'like', "%$value%")
                    ->orWhere('teachers.name', 'like', "%$value%")
                    ->orWhere('class_rooms.class_time', 'like', "%$value%")
                ;
            });



        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $class_time     = isset($data['class_time']) ? $data['class_time'] : null;
        $course_id      = isset($data['course_id']) ? $data['course_id'] : null;
        $slot           = isset($data['slot']) ? $data['slot'] : 0;
        $code           = isset($data['code']) ? $data['code'] : null;

        $status         = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue     = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $conditions = [];
        $conditions[] = ['company_id', $company_id];
        $conditions[] = ['code', $code];

        $free_slot = $slot;

        if(empty($course_id) || empty($code)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        if($id) {
            $object = ClassRoom::find($id);
            $conditions[] = ["id", "<>", $id];
            $data['updated_user']  = $authUserId;

            $slot_registered = $object->slot_registered;
            $free_slot = $slot - $slot_registered;

        } else {
            $object = new ClassRoom();
            $data['company_id']     = $company_id;
            $data['created_user']   = $authUserId;
        }

        $data['free_slot']      = $free_slot;

        $exist = ClassRoom::where($conditions)->get()->count();
        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __("Code already exists")
            ]);
        }




        unset($data['continue']);


        $data['status'] = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }



        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = ClassRoom::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
