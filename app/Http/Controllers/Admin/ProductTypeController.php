<?php

namespace App\Http\Controllers\Admin;

use App\ProductType;
use App\Helpers\BaseService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class ProductTypeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index()
    {
        $title = trans("ProductType List");

        return view('Admin.ProductType.index', compact(
            'title'
        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $list = ProductType::orderBy($fieldOrder, $orderType)
            ->select(
                "product_types.*"
            )
        ;

        if ($value) {
            $list = $list
                ->where('product_types.code', 'like', "%$value%")
                ->orWhere('product_types.name', 'like', "%$value%")
            ;
        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }


    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $company_id = $authUser->company_id;

        $data       = $request->all();
        $id         = isset($data['id']) ? $data['id'] : '';
        $name       = isset($data['name']) ? $data['name'] : null;
        $slug       = isset($data['slug']) ? $data['slug'] : null;
        $picture    = isset($data['picture']) ? $data['picture'] : [];

        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        if(empty($name)){
            return json_encode([
                'success'       => false,
                'message'       => __("Data required can not be null")
            ]);
        }

        $fileName = "";
        if($picture){
            $fileName   = $picture->getClientOriginalName();
            $mimeType   = $picture->getClientMimeType();
            $extension  = $picture->getClientOriginalExtension();
            $size       = $picture->getSize();
            $data['picture']  = $fileName;
        }

        $conditions = [];
        $conditions[] = ["slug", $slug];
        $conditions[] = ["company_id", $company_id];

        if($id) {
            $object = ProductType::find($id);
            $data['updated_user'] = $authUserId;

            $conditions[] = ["id", "<>", $id];
        } else {
            $object = new ProductType();
            $data['company_id'] = $company_id;
            $data['created_user'] = $authUserId;
        }

        $exist = ProductType::where($conditions)->get()->count();
        if($exist) {
            return json_encode([
                "success" => false,
                "message" => __("Slug already exists")
            ]);
        }


        unset($data['continue']);

        $data['status']     = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __("Save data failed")
            ]);
        }

        if($picture){
            /** Upload file */
            $path = public_path()."/uploads/".$company_id."/ProductType/".$object->id.'/';
            $picture->move($path, $fileName);
        }

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __("Save data successfully")
        ]);


    }



    public function delete(Request $request) {
        $id = $request['id'];
        $object = ProductType::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __("Can not detect this data")
            ]);
        }



        if(!$object->delete()) {
            return json_encode([
                'success' => false,
                'message' => __("Delete failed!")
            ]);
        }

        /**
         * Xóa toàn bộ uploads của company
         */
        $path = public_path()."/uploads/".$id.'/ProductType/';
        BaseService::deleteDir($path);


        /**
         * Xóa các dữ liệu phát sinh kèm theo
         * 1. products
         * ....
         */
        $path_product = public_path()."/uploads/".$id.'/Product/';
        BaseService::deleteDir($path_product);

        return json_encode([
            'success' => true,
            'message' => __("Delete successfully")
        ]);
    }



}
