<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helpers\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{

    public function index()
    {
        $company = Company::find(1);
        $current_domain = BaseService::getCurrentDomain();
        return view('Auth.index', compact('company', 'current_domain'));
    }

    public function registration()
    {
        return view('registration');
    }

    public function postLogin(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...

            BaseService::writeLoginLog();

            $authUser = Auth::user();
            $is_admin = $authUser->is_admin;

            $current_login = date('Y-m-d H:i:s');
            $objUser = User::find($authUser->id);
            $objUser->lasted_login = $current_login;
            $objUser->save();

            $url = "";
            switch ($is_admin){
                case MASTER_ADMIN:
                case EMPLOYEE_ADMIN:
                    $url = 'admin/dashboard/index';
                    break;
                case CUSTOMER_ADMIN:
                    $url = 'customer_portal/dashboard/index';
                    break;
            }


            return redirect()->intended($url);
        }


        return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
    }

    public function postRegistration(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            //'contact_number' => 'required|contact_number|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();

        $check = $this->create($data);

        return Redirect::to("login");
    }



    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
}
