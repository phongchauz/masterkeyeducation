<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\BaseService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\SiteCart;
use App\User;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use PhpParser\Node\Expr\Closure;

class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    public function redirectToProvider() {

        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $getInfo = Socialite::driver('facebook')->user();
        $user = $this->createUser($getInfo);
        auth()->login($user);
        return redirect()->to('/customer/dashboard/index');
    }

    function createUser($getInfo){
        $user = User::where('fb_id', $getInfo->id)->first();
        if (!$user) {
            $user = new User();
            $user->name = $getInfo->name;
            $user->email = $getInfo->email;
            $user->fb_id = $getInfo->id;
            $user->is_admin = USER_TYPE_CRM;
            $user->save();
        }
        return $user;
    }


    public function redirectTo() {

        $userAuth = Auth::user();

        $redirectUrl = "";

        $isAdmin = $userAuth->is_admin;
        $isAdmin = (int)$isAdmin;
        if($isAdmin){
            BaseService::recordUserLogonLogs($isAdmin);
        }

        switch ($isAdmin){
            case USER_TYPE_ADMIN:
                $redirectUrl = '/admin/site/index';
                break;
            case USER_TYPE_CRM:
                $redirectUrl = '/customer/dashboard/index';
                break;
            case USER_TYPE_EC_CUSTOMER:
                $redirectUrl = '/home';
                break;
        }
        //Update cart
        $userId = $userAuth->id;



        if(empty($redirectUrl)){
            session()->flush();
            return abort(403, 'Unauthorized action.');
        }

        return $redirectUrl;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $template = \App\TemplateSetting::where(['status' => 1])->value('name');
        $template = isset($template) ? $template : 'vesperr';

        if($template == 'dcode'){
            $loginRecaptcha = isset($this->company['login_recaptcha']) ? $this->company['login_recaptcha'] : 0;
            if($loginRecaptcha){
                $this->validate($request, [
                    $this->username() => 'required|string',
                    'password' => 'required',
                    'g-recaptcha-response' => 'required|recaptcha'
                ]);
            } else {
                $this->validate($request, [
                    $this->username() => 'required|string',
                    'password' => 'required'
                ]);
            }
        }

    }
}
