<?php

namespace App\Http\Controllers\Cash;

use App\CashIncome;
use App\CashOutcome;
use App\CashPeriod;
use App\CashWallet;
use App\Helpers\BaseService;
use App\Helpers\CashService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CashAppController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Period List");

        return view('Cash.CashPeriod.index', compact(
            'title'

        ));
    }


    public function changeWallet(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;

        $object     = CashWallet::where('id', $id)->where('user_id', $authUserId)->first();

        if(!$object) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_OBJECT_NOT_FOUND)
            ]);
        }

        DB::table('cash_wallets')->update(array('is_selected' => STATUS_INACTIVE));

        $object->is_selected = STATUS_ACTIVE;

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }


        return json_encode([
            'success'       => true,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }





}
