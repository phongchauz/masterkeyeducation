<?php

namespace App\Http\Controllers\Cash;

use App\CashIncome;
use App\CashOutcome;
use App\CashPeriod;
use App\Helpers\BaseService;
use App\Helpers\CashService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CashPeriodController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Period List");

        return view('Cash.CashPeriod.index', compact(
            'title'

        ));
    }

    public function search (Request $request) {
        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $wallet_id = CashService::getWalletId();

        $data = $request->all();
        $year = isset($data['year']) ? $data['year'] : date("Y");
        $value = $request['search']['value'];

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $conditions = [
            ['user_id', $authUserId],
            ['wallet_id', $wallet_id],
            ['year', $year],
        ];

        $list = CashPeriod::orderBy('month', 'asc')
            ->orderBy($fieldOrder, $orderType)
            ->where($conditions)
            ->select(
                "cash_periods.*"
            )
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('cash_periods.month', 'like', "%$value%")
                    ->orWhere('cash_periods.year', 'like', "%$value%")
                    ->orWhere('cash_periods.name', 'like', "%$value%");
            });


        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $wallet_id = CashService::getWalletId();

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $year          = isset($data['year']) ? $data['year'] : null;

        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;

        $listCashPeriod = CashPeriod::where('year', $year)->where('wallet_id', $wallet_id)->where('user_id', $authUserId)->get();


        for($i = 1; $i <=12; $i++){

            $exist = false;
            foreach ($listCashPeriod as $cashPeriod){
                if($cashPeriod->month == $i){
                    $exist = true;
                    break;
                }
            }

            if($exist){continue;}

            $object = new CashPeriod();

            $name = ($i < 10 ? "0".$i : $i).'/'.$year;
            $object->name = $name;
            $object->month = $i;
            $object->year = $year;
            $object->user_id = $authUserId;
            $object->wallet_id = $wallet_id;
            $object->income = 0;
            $object->outcome = 0;
            $object->revenue = 0;

            $object->save();

        }

        unset($data['continue']);

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }

    public function calculator(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $wallet_id = CashService::getWalletId();

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;


        $conditions = [
            ['id' , $id],
            ['wallet_id' , $wallet_id],
            ['user_id' , $authUserId],
        ];
        $object = CashPeriod::where($conditions)->first();

        if(!$object){
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_OBJECT_NOT_FOUND)
            ]);
        }

        $year = $object->year;
        $month = $object->month;

        $conditions = [
            ['id' , $id],
            ['wallet_id' , $wallet_id],
            ['user_id' , $authUserId],
            ['year' , $year],
            ['month' , $month],
        ];

        $income = CashIncome::where($conditions)->sum('amount');
        $outcome = CashOutcome::where($conditions)->sum('amount');
        $revenue = $income - $outcome;

        $object->income = $income;
        $object->outcome = $outcome;
        $object->revenue = $revenue;
        $object->updated_user = $authUserId;

        if(!$object->save()){
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_CALCULATOR_FAIL)
            ]);
        }

        return json_encode([
            'success'       => true,
            'message'       => __(MESSAGE_CALCULATOR_SUCCESSFULLY)
        ]);


    }

    public function delete(Request $request) {
        $id = $request['id'];
        $object = CashPeriod::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
