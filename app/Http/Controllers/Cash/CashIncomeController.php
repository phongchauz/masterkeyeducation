<?php

namespace App\Http\Controllers\Cash;

use App\CashIncome;
use App\Helpers\BaseService;
use App\Helpers\CashService;
use App\Helpers\ExcelService;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class CashIncomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    public function index()
    {
        $title = trans("Income List");

        return view('Cash.CashIncome.index', compact(
            'title'

        ));
    }

    public function search (Request $request) {
        $value = $request['search']['value'];

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $wallet_id = CashService::getWalletId();

        $start = (int)$request->get('start');
        $length = (int)$request->get('length');
        $fieldOrder = isset($request["columns"][$request["order"][0]["column"]]["name"]) ? $request["columns"][$request["order"][0]["column"]]["name"] : null;
        $orderType = isset($request["order"][0]["dir"]) ? $request["order"][0]["dir"] : null;

        $conditions = [
            ['user_id', $authUserId],
            ['wallet_id', $wallet_id],
        ];

        $list = CashIncome::orderBy($fieldOrder, $orderType)
            ->select(
                "cash_incomes.*"
            )
            ->where($conditions)
        ;

        if ($value) {

            $list = $list->where( function ( $query ) use ($value){
                $query->where('cash_incomes.code', 'like', "%$value%")
                    ->orWhere('cash_incomes.name', 'like', "%$value%");
            });

        }

        $recordsTotal = $list->count();
        if ($length) {
            $list = $list->skip($start)->take($length);
        }
        $list = $list->get();
        $results = $list->toArray();

        return json_encode([
            'data'              => $results,
            'recordsTotal'      => isset($recordsTotal) ? $recordsTotal : count($results),
            'recordsFiltered'   => isset($recordsTotal) ? $recordsTotal : count($results)
        ]);
    }

    public function saveData(Request $request) {

        $authUser = Auth::user();
        $authUserId = $authUser->id;
        $wallet_id = CashService::getWalletId();

        $data           = $request->all();
        $id             = isset($data['id']) ? $data['id'] : null;
        $name           = isset($data['name']) ? $data['name'] : null;
        $amount           = isset($data['amount']) ? $data['amount'] : 0;
        $of_date           = isset($data['of_date']) ? $data['of_date'] : date('m/d/Y');

        $amount = str_replace(',', '', $amount);
        $of_date = BaseService::reformatDate($of_date);

        $is_salary  = isset($data['is_salary']) && $data['is_salary'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $status     = isset($data['status']) && $data['status'] == "on" ? STATUS_ACTIVE : STATUS_INACTIVE;
        $isContinue = isset($data['continue']) && $data['continue'] == 'on' ? STATUS_ACTIVE : STATUS_INACTIVE;



        if($id) {
            $object = CashIncome::find($id);
        } else {
            $object = new CashIncome();
            unset($data['id']);
        }

        $arrDate = explode('-', $of_date);
        if(count($arrDate) != 3){
            return json_encode([
                "success" => false,
                "message" => __("Date income is not valid")
            ]);
        }

        $year = $arrDate[0];
        $month = $arrDate[1];

        if(empty($amount) || empty($name)) {
            return json_encode([
                "success" => false,
                "message" => __(MESSAGE_DATA_REQUIRED_IS_NULL)
            ]);
        }

        unset($data['continue']);

        $data['user_id']    = $authUserId;
        $data['wallet_id']    = $wallet_id;
        $data['of_date']    = $of_date;
        $data['month']      = $month;
        $data['year']       = $year;
        $data['amount']     = $amount;
        $data['is_salary']  = $is_salary;
        $data['status']     = $status;

        $object = BaseService::renderObject($object, $data);

        if(!$object->save()) {
            return json_encode([
                'success'       => false,
                'message'       => __(MESSAGE_SAVE_UNSUCCESSFULLY)
            ]);
        }

        CashService::updateCashPeriod($authUserId, $wallet_id, $month, $year, TYPE_CASH_INCOME);

        return json_encode([
            'success'       => true,
            'is_continue'   => $isContinue,
            'message'       => __(MESSAGE_SAVE_SUCCESSFULLY)
        ]);


    }


    public function delete(Request $request) {
        $id = $request['id'];
        $object = CashIncome::find($id);

        if(!$object) {
            return json_encode([
                'success' => false,
                'alert'   => __(MESSAGE_OBJECT_NOT_FOUNT)
            ]);
        }

        if($object->delete()) {
            return json_encode([
                'success' => true,
                'message' => __(MESSAGE_DELETE_SUCCESSFULLY)
            ]);
        }

        return json_encode([
            'success' => false,
            'message' => __(MESSAGE_DELETE_UNSUCCESSFULLY)
        ]);
    }




}
