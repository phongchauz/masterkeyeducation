<?php

namespace App\Http\Controllers;


use App\Article;
use App\Banner;
use App\Comment;
use App\CoreValue;
use App\Teacher;
use App\Mission;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\BaseController;

class TeacherController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $title = "";

        $listTeacher = Teacher::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();

        return view('Teacher.index', compact(
                'listTeacher',
                'title'
        ));
    }




}
