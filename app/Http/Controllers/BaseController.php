<?php

namespace App\Http\Controllers;


use App\Company;
use App\Helpers\BaseService;
use App\Module;
//use App\Setting;
use App\FunctionMenu;
use App\SiteSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BaseController extends Controller
{
    protected $company;
    protected $site_setting;

    public function __construct()
    {

        $this->middleware(function ($request, $next) {
            $authUser = Auth::user();

            $listMenu = [];
//            $listMenu = BaseService::getMenuListByUser($authUser, 1);
            $activeMenu = [];
            $is_show_back_url = true;
            $currentFunction = BaseService::getCurrentFunction($request);

            if ($authUser) {
                $moduleId = BaseService::getCurrentModuleId($request);

//                $listMenu = BaseService::getMenuListByUser($authUser, $moduleId);

                if(!Session::has('listMenu')){
                    $listMenu = BaseService::getMenuListByUser($authUser, $moduleId);

                    $request->session()->put('listMenu', $listMenu);
                }else{
                    $listMenu = Session::get('listMenu');
                }

                $allow_access_menu = true;
                if(!$listMenu){$allow_access_menu = false;}

                $arr_access_menu = explode(',', $authUser->functions_access);
                if($currentFunction && !in_array($currentFunction['id'], $arr_access_menu)){$allow_access_menu = false;}

                if(!$allow_access_menu && $authUser->is_admin == MASTER_ADMIN){
                    $allow_access_menu = 1;
                }

                if(!$allow_access_menu){
                    abort(404);
                }

                $activeMenu = BaseService::activeMenu($request);

                if(
                    $authUser->is_admin == EMPLOYEE_ADMIN && $currentFunction && $currentFunction['controller'] == 'user' && $currentFunction['action'] == 'profile'
                ){

                    $is_show_back_url = false;
                }

            }


            $locale = (Session::has('locale')) ? Session::get('locale') : 'vi';


            View::share([
                'listMenu'          => $listMenu,
                'currentFunction'   => $currentFunction,
                'activeMenu'        => $activeMenu,
                'locale'            => $locale,
                'is_show_back_url'  => $is_show_back_url,
                'current_domain'    => BaseService::getCurrentDomain(),
                'company_id'        => $authUser->company_id ?? ''
            ]);

            return $next($request);
        });

        $template = "kiddos";
        View::share(['template' => $template]);

        $listModule = Module::all()->toArray();
        View::share(['listModule' => $listModule]);
        $company = Company::find(MASTER_COMPANY_ID)->toArray();

        $this->company = $company;
        View::share(['company' => $company]);

        $site_setting = SiteSetting::where('company_id', MASTER_COMPANY_ID)->first();
        $this->site_setting = $site_setting;
        View::share(['site_setting' => $site_setting]);



        $current_page = BaseService::getCurrentPage();
        View::share(['current_page' => $current_page]);

    }

}
