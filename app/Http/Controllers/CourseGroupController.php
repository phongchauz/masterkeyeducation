<?php

namespace App\Http\Controllers;


use App\Article;
use App\Banner;
use App\ClassRoom;
use App\ClassRoomRegistration;
use App\Comment;
use App\Company;
use App\CoreValue;
use App\Course;
use App\CourseGroup;
use App\Helpers\BaseService;
use App\Mission;
use App\Site;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\BaseController;

class CourseGroupController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $title = "";

        $listCourseGroup = CourseGroup::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();

        return view('CourseGroup.index', compact(
                'listCourseGroup',
                'title'
        ));
    }


    public function course($slug){

        $slug = trim(strtolower($slug));
        if(empty($slug)){
            abort(404);
        }

        $courseGroup = CourseGroup::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->where('slug', $slug)->first();

        $course_group_id = $courseGroup->id ?? -1;

        $listCourse = Course::where('company_id', MASTER_COMPANY_ID)
            ->where('course_group_id', $course_group_id)
            ->where('status', STATUS_ACTIVE)->get();
        $title = $courseGroup->name ?? "";

        $listPopularCourse = Course::where('company_id', MASTER_COMPANY_ID)
            ->where('status', STATUS_ACTIVE)
            ->where('is_popular', STATUS_ACTIVE)->get();

        return view('CourseGroup.course_group', compact(
            'listCourse',
            'listPopularCourse',
            'courseGroup',
            'title'
        ));
    }
    public function registration($code){



        if(empty($code)){
            abort(404);
        }

        $classroom = ClassRoom::select('class_rooms.*', 'courses.name', 'courses.slug', 'courses.summary', 'courses.picture', 'courses.cover')
            ->leftJoin('courses', 'courses.id', 'class_rooms.course_id')
            ->where('class_rooms.company_id', MASTER_COMPANY_ID)
            ->where('class_rooms.status', STATUS_ACTIVE)
            ->where('class_rooms.code', $code)->first()
        ;


        $title = $classroom->name ?? "";

        $listPopularCourseGroup = CourseGroup::where('company_id', MASTER_COMPANY_ID)
            ->where('status', STATUS_ACTIVE)
            ->where('is_popular', STATUS_ACTIVE)->get();

        return view('CourseGroup.registration', compact(
            'classroom',
            'listPopularCourseGroup',
            'title'
        ));
    }

    public function saveRegistration(Request $request) {



        $data    = $request->all();

        $captcha = $data["g-recaptcha-response"] ?? null;

        if(empty($captcha)){
            return json_encode([
                "success" => false,
                "message" => __("Please check captcha before!")
            ]);
        }

        $allow_captcha = BaseService::verifyGRecaptcha($captcha);

        if(!$allow_captcha && 1 == 0){
            return json_encode([
                "success" => false,
                "message" => __("Captcha wrong, please try again!")
            ]);
        }

        $class_room_id  = $data['class_room_id'] ?? null;
        $course_id      = $data['course_id'] ?? null;
        $fullname       = $data['fullname'] ?? null;
        $phone          = $data['phone'] ?? null;
        $contact_email  = $data['contact_email'] ?? null;
        $message        = $data['message'] ?? null;

        if(
            empty($fullname) ||
            empty($phone) ||
            empty($contact_email)
        ){
            return json_encode([
                "success" => false,
                "message" => __("Data required can not be null!")
            ]);
        }

        $class_room = ClassRoom::select('class_rooms.*', 'teachers.name as teacher_name', 'courses.name as course_name')
            ->leftJoin('teachers', 'teachers.id', 'class_rooms.teacher_id')
            ->leftJoin('courses', 'courses.id', 'class_rooms.course_id')
            ->where('class_rooms.status', STATUS_ACTIVE)
            ->where('class_rooms.id', $class_room_id)
            ->where('class_rooms.course_id', $course_id)
            ->first()
        ;

        if(!$class_room){
            return json_encode([
                "success" => false,
                "message" => __("Classroom not found, please press F5 and try again!")
            ]);
        }


        if(!$class_room->free_slot){
            return json_encode([
                "success" => false,
                "message" => __("This class have full, please choose another classroom to register!")
            ]);
        }
        $ip_address = BaseService::getSourceIp();

        $object = new ClassRoomRegistration();

        $object->company_id     = MASTER_COMPANY_ID;
        $object->course_id      = $class_room->course_id;
        $object->teacher_id     = $class_room->teacher_id;
        $object->class_room_id  = $class_room->id;
        $object->fullname       = $fullname;
        $object->phone          = $phone;
        $object->contact_email  = $contact_email;
        $object->message        = $message;
        $object->ip_address     = $ip_address;
        $object->status         = 1;

        $object->save();

        $company_info = Company::find(MASTER_COMPANY_ID);
        $company_lang = $company->lang_key ?? "en";
        /**
         * Send mail for admin
         */

        if(
            !empty($company_info->email) &&
            filter_var($company_info->email, FILTER_VALIDATE_EMAIL)
        ){


            if($company_lang == "en"){
                $summary = "The system has just recorded 1 registration for <b>".$class_room->code
                    ."</b> class of <b>".$class_room->course_name
                    ."</b> subject by teacher <b>".$class_room->teacher_name."</b>.";
            }else{
                $summary = "Hệ thống vừa ghi nhận có 1 đăng ký cho lớp <b>".$class_room->code
                    ."</b> của môn học <b>".$class_room->course_name
                    ."</b> do giáo viên <b>".$class_room->teacher_name."</b> phụ trách.";
            }

            $email_data = [
                "summary"       => $summary,
                "fullname"      => $fullname,
                "phone"         => $phone,
                "created_at"    => $object->created_at,
                "message"       => $message,
            ];

            $data_email = [
                'to_email'  => "chaunp1991@gmail.com",
//                'to_email'  => $company_info->email,
                'subject'   => __("Notify New Registration Classroom"),
                'template'  => "Elements.EmailTemplates.admin_notify_customer_reg_classroom",
                'email_data'=> $email_data,
            ];
            BaseService::sendEmail($data_email);
            sleep(3);
        }



        /**
         * Send mail for parents
         */
        if(
            !empty($contact_email) &&
            filter_var($contact_email, FILTER_VALIDATE_EMAIL)
        ){



            if($company_lang == "en"){
                $summary = "Thank you for your interest and registration for <b>".$class_room->code
                    ."</b> class of <b>".$class_room->course_name
                    ."</b> subject by teacher <b>".$class_room->teacher_name."</b>.";
            }else{
                $summary = "Cảm ơn bạn đã quan tâm và đăng ký lớp <b>".$class_room->code
                    ."</b> của môn học <b>".$class_room->course_name
                    ."</b> do giáo viên <b>".$class_room->teacher_name."</b> phụ trách.";
            }

            $email_data = [
                "summary"       => $summary,
                "fullname"      => $fullname,
                "signature"     => $company_info->name,
            ];

            $data_email = [
                'to_email'  => $contact_email,
                'subject'   => __("Notify New Registration Classroom"),
                'template'  => "Elements.EmailTemplates.customer_notify_reg_classroom",
                'email_data'=> $email_data,
            ];


            BaseService::sendEmail($data_email);
        }



        return json_encode([
            'success' => true,
            'message'       => __("Registration class room successfully")
        ]);


    }


}
