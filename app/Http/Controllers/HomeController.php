<?php

namespace App\Http\Controllers;


use App\Article;
use App\Banner;
use App\Comment;
use App\CoreValue;
use App\Course;
use App\CourseGroup;
use App\CustomerRequest;
use App\Gallery;
use App\Helpers\BaseService;
use App\Mission;
use App\Site;
use App\Subscribe;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\BaseController;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $listBanner = Banner::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        $listCoreValue = CoreValue::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        $listTeacher = Teacher::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        $listCourse = Course::where('company_id', MASTER_COMPANY_ID)->where('display_homepage', STATUS_ACTIVE)->where('status', STATUS_ACTIVE)->get();
        $listComment = Comment::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        $listMission = Mission::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();
        $listCourseGroup = CourseGroup::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->where('display_homepage', STATUS_ACTIVE)->get();
        $listGalleries = Gallery::where('company_id', MASTER_COMPANY_ID)->get();

        return view('Home.index', compact(
                'listBanner',
                'listCoreValue',
                'listCourseGroup',
                'listTeacher',
                'listCourse',
                'listComment',
                'listGalleries',
                'listMission'
        ));
    }

    public function home()
    {
        return view('home');
    }

    public function saveSubscribe(Request $request) {


        $data = $request->all();

        $email = isset($data['email']) ? $data['email'] : "";

        if(empty($email)){
            return json_encode([
                "success" => false,
                "message" => __("Email can not be null")
            ]);
        }

        $exist = Subscribe::where('company_id', MASTER_COMPANY_ID)->where('email', $email)->first();

        if($exist){
            return json_encode([
                "success"           => false,
                "message"             => __("You have subscribed!")
            ]);
        }

        unset($data['_token']);

        $object = new Subscribe();
        $object->company_id = MASTER_COMPANY_ID;

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            $smsSuccess = trans('Subscribe successfully');
            return json_encode([
                "success"           => true,
                "message"             => $smsSuccess
            ]);
        }
        return json_encode([
            "success"           => false,
            "message"             => __("Subscribe failed")
        ]);
    }

    public function saveCustomerRequest(Request $request) {


        $data = $request->all();

        $phone = isset($data['phone']) ? $data['phone'] : "";

        if(empty($phone)){
            return json_encode([
                "success" => false,
                "message" => __("Phone can not be null")
            ]);
        }



        unset($data['_token']);

        $object = new CustomerRequest();
        $object->company_id = MASTER_COMPANY_ID;

        $object = BaseService::renderObject($object, $data);

        if($object->save()) {
            $smsSuccess = trans('Request successfully');
            return json_encode([
                "success"           => true,
                "message"             => $smsSuccess
            ]);
        }
        return json_encode([
            "success"           => false,
            "message"             => __("Request failed")
        ]);
    }
}
