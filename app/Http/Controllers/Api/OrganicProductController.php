<?php

namespace App\Http\Controllers\Api;


use App\Helpers\BaseService;
use App\Helpers\ExcelService;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Motel;
use App\OrganicCategory;
use App\OrganicProduct;
use App\OrganicUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Database\Eloquent\Builder;
class OrganicProductController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
//        $this->middleware('auth');
        $this->middleware('auth:api')->except([
            'getProducts'
        ]);
    }



    public function getProducts (Request $request) {

        $data = $request->all();
        $company_id = isset($data['company_id']) ? $data['company_id'] : null;
        $category_id = isset($data['category_id']) ? $data['category_id'] : null;
        $search_text = isset($data['search_text']) ? $data['search_text'] : null;

        if(empty($company_id)){
            return response(json_encode([
                "success" => false,
                "message" => __("Data require can not be null")
            ]));
        }

        $list = OrganicProduct::where('organic_products.company_id', $company_id)
        ->orderBy('organic_products.name', 'asc');

        if($category_id){
           $list = $list->where('organic_products.category_id', $category_id);
        }

        if ($search_text) {
            $list = $list->where(function ($query) use ($search_text) {
                $query->where('organic_products.code', 'like', "%$search_text%")
                    ->orWhere('organic_products.name', 'like', "%$search_text%")
                ;
            });
        }

        $list = $list->get();

        return response(json_encode([
                    "success" => true,
                    "data" => $list
                ]));
    }




}
