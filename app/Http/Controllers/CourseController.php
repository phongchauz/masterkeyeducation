<?php

namespace App\Http\Controllers;


use App\Article;
use App\Banner;
use App\ClassRoom;
use App\ClassRoomRegistration;
use App\Comment;
use App\Company;
use App\CoreValue;
use App\Course;
use App\Helpers\BaseService;
use App\Mission;
use App\Site;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\BaseController;

class CourseController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $title = "";

        $listCourse = Course::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->get();

        return view('Course.index', compact(
                'listCourse',
                'title'
        ));
    }


    public function course($slug){

        $slug = trim(strtolower($slug));


        if(empty($slug)){
            abort(404);
        }

        $course = Course::where('company_id', MASTER_COMPANY_ID)->where('status', STATUS_ACTIVE)->where('slug', $slug)->first();

        $course_id = $course->id ?? -1;

        $listClassroom = ClassRoom::where('company_id', MASTER_COMPANY_ID)
            ->where('course_id', $course_id)
            ->where('status', STATUS_ACTIVE)->get();
        $title = $course->name ?? "";

        $listPopularCourse = Course::where('company_id', MASTER_COMPANY_ID)
            ->where('status', STATUS_ACTIVE)
            ->where('is_popular', STATUS_ACTIVE)->get();

        return view('Course.course', compact(
            'listClassroom',
            'listPopularCourse',
            'course',
            'title'
        ));
    }
    public function registration($slug){

        if(empty($slug)){
            abort(404);
        }

        $course = Course::select('courses.*')
            ->where('courses.company_id', MASTER_COMPANY_ID)
            ->where('courses.status', STATUS_ACTIVE)
            ->where('courses.slug', $slug)->first()
        ;


        $title = $course->name ?? "";

        $listPopularCourse = Course::where('company_id', MASTER_COMPANY_ID)
            ->where('status', STATUS_ACTIVE)
            ->where('is_popular', STATUS_ACTIVE)->get();

        return view('Course.registration', compact(
            'course',
            'listPopularCourse',
            'title'
        ));
    }

    public function saveRegistration(Request $request) {



        $data    = $request->all();

        $captcha = $data["g-recaptcha-response"] ?? null;

        if(empty($captcha)){
            return json_encode([
                "success" => false,
                "message" => __("Please check captcha before!")
            ]);
        }

        $allow_captcha = BaseService::verifyGRecaptcha($captcha);

        if(!$allow_captcha && 1 == 0){
            return json_encode([
                "success" => false,
                "message" => __("Captcha wrong, please try again!")
            ]);
        }

        $course_id      = $data['course_id'] ?? null;
        $fullname       = $data['fullname'] ?? null;
        $phone          = $data['phone'] ?? null;
        $contact_email  = $data['contact_email'] ?? null;
        $message        = $data['message'] ?? null;

        if(
            empty($fullname) ||
            empty($phone) ||
            empty($contact_email)
        ){
            return json_encode([
                "success" => false,
                "message" => __("Data required can not be null!")
            ]);
        }

        $course = Course::select('courses.*')
            ->where('courses.status', STATUS_ACTIVE)
            ->where('courses.id', $course_id)
            ->first()
        ;

        if(!$course){
            return json_encode([
                "success" => false,
                "message" => __("Course not found, please press F5 and try again!")
            ]);
        }



        $ip_address = BaseService::getSourceIp();

        $object = new ClassRoomRegistration();

        $object->company_id     = MASTER_COMPANY_ID;
        $object->course_id      = $course->id;
        $object->teacher_id     = null;
        $object->class_room_id  = null;
        $object->fullname       = $fullname;
        $object->phone          = $phone;
        $object->contact_email  = $contact_email;
        $object->message        = $message;
        $object->ip_address     = $ip_address;
        $object->status         = 1;

        $object->save();

        $company_info = Company::find(MASTER_COMPANY_ID);
        $company_lang = $company->lang_key ?? "en";
        /**
         * Send mail for admin
         */

        if(
            !empty($company_info->email) &&
            filter_var($company_info->email, FILTER_VALIDATE_EMAIL)
        ){


            if($company_lang == "en"){
                $summary = "The system has just recorded 1 registration for a course with name <b>".$course->name;
            }else{
                $summary = "Hệ thống vừa ghi nhận có 1 đăng ký cho  môn học <b>".$course->name;
            }

            $email_data = [
                "summary"       => $summary,
                "fullname"      => $fullname,
                "phone"         => $phone,
                "created_at"    => $object->created_at,
                "message"       => $message,
            ];

            $data_email = [
//                'to_email'  => "chaunp1991@gmail.com",
                'to_email'  => $company_info->email,
                'subject'   => __("Notify New Registration Classroom"),
                'template'  => "Elements.EmailTemplates.admin_notify_customer_reg_classroom",
                'email_data'=> $email_data,
            ];
            BaseService::sendEmail($data_email);
            sleep(3);
        }



        /**
         * Send mail for parents
         */
        if(
            !empty($contact_email) &&
            filter_var($contact_email, FILTER_VALIDATE_EMAIL)
        ){



            if($company_lang == "en"){
                $summary = "Thank you for your interest and registration for  class of course <b>".$course->name;
            }else{
                $summary = "Cảm ơn bạn đã quan tâm và đăng ký lớp của môn học <b>".$course->name;
            }

            $email_data = [
                "summary"       => $summary,
                "fullname"      => $fullname,
                "signature"     => $company_info->name,
            ];

            $data_email = [
                'to_email'  => $contact_email,
                'subject'   => __("Notify New Registration Classroom"),
                'template'  => "Elements.EmailTemplates.customer_notify_reg_classroom",
                'email_data'=> $email_data,
            ];


            BaseService::sendEmail($data_email);
        }



        return json_encode([
            'success' => true,
            'message'       => __("Registration class room successfully")
        ]);


    }


}
