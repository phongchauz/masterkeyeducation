<?php

# Date
const YMD = 'Y-m-d';
const YMD_FULL = 'Y-m-d H:i:s';
const DMY = 'd-m-Y';

# Status
const STATUS_NO = 0;
const STATUS_INACTIVE = 0;
const STATUS_ACTIVE = 1;
const LIVE_MODE = 0;
const CACHE_VARIABLE = 1;



const MAX_FILE_UPLOAD = 1024;

const PLUGIN_DEFAULT = 'admin';

const GRAPH_FACEBOOK = 'https://graph.facebook.com/v3.1/me';

const REDIRECT_ADMIN_URL = '/admin/company/dashboard';


# Sort
const ORDER_ASC = 'asc';
const ORDER_DESC = 'desc';

const ACTION_INSERT  = 1;
const ACTION_UPDATE  = 2;
const ACTION_DELETE  = 3;
const ACTION_IMPORT  = 4;
const ACTION_EXPORT  = 5;

# Is Admin
const MASTER_ADMIN = 1;
const MASTER_COMPANY_ID = 1;
const CUSTOMER_ADMIN = 2;
const EMPLOYEE_ADMIN = 3;
const CUSTOMER_EMPLOYEE_ADMIN = 4;

# Room type
const TYPE_BLOCK = 1;
const TYPE_FLOOR = 2;
const TYPE_ROOM = 3;


const PAGE_ORIENTATION_LANDSCAPE = 'LANDSCAPE';
const ORIENTATION_PORTRAIT = 'PORTRAIT';
const PAGE_A3 = 'A3';
const PAGE_A5 = 'A5';
const SYSTEM_CACHE = 1;

//GVIEW
const GVIEW = "https://docs.google.com/gview?url=";
const ACCEPTED_EXCEL = ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel";



//Type file or folder
const TYPE_FOLDER = 1;
const TYPE_FILE = 2;
//Type delete
const TYPE_DELETE = 1;
const TYPE_EMPTY = 2;
//Number Character code
const LENGTH_CODE = 8;

//HR SECTION
const PAGE_SIZE = 20;


const MODULE_ADMIN  = 1;
const MODULE_CUSTOMER_PORTAL  = 2;
const MODULE_CASH  = 3;

const TYPE_CASH_INCOME = 1;
const TYPE_CASH_OUTCOME = 2;

/**
 * Define all message of system here
 */
const MESSAGE_SAVE_SUCCESSFULLY = "Save data successfully";
const MESSAGE_CALCULATOR_FAIL = "Calculator data unsuccessfully";
const MESSAGE_CALCULATOR_SUCCESSFULLY = "Calculator data successfully";
const MESSAGE_SAVE_UNSUCCESSFULLY = "Save data unsuccessfully";
const MESSAGE_DATA_REQUIRED_IS_NULL = "Data required can not be null";
const MESSAGE_EXIST_CODE = "Code already exist";
const MESSAGE_EXIST_SLUG = "Slug already exist";
const MESSAGE_EXIST_IS_ADMIN = "Is admin already exist";
const MESSAGE_EXIST_NAME = "Name already exist";
const MESSAGE_EXIST_ACCOUNT = "Account already exist";
const MESSAGE_EXIST_MEMBER = "Member already exist";
const MESSAGE_EXIST_TASK = "Task already exist";
const MESSAGE_OBJECT_NOT_FOUNT = "Can not detect this data";
const MESSAGE_OBJECT_NOT_FOUND = "Can not detect this data";
const MESSAGE_DELETE_UNSUCCESSFULLY = "Delete data unsuccessfully";
const MESSAGE_DELETE_SUCCESSFULLY = "Delete data successfully";
const MESSAGE_TASK_NOT_FOUND = "Task not found";
const MESSAGE_CONFIRM_PROCESSING_TASK_SUCCESS = "Confirm processing task successfully";
const MESSAGE_CONFIRM_PROCESSING_TASK_FAIL = "Confirm processing task unsuccessfully";
const MESSAGE_CONFIRM_RESOLVED_TASK_SUCCESS = "Confirm resolved task successfully";
const MESSAGE_CONFIRM_RESOLVED_TASK_FAIL = "Confirm resolved task unsuccessfully";
const MESSAGE_ROOM_NOT_FOUND = "Room not found!";
const MESSAGE_EXIST_CASH_PERIOD_BY_YEAR = "You have created cash period for this year.";
/**********************************************************************************************************************/
const ROOM_STATUS_NOT_USED = 0;
const ROOM_STATUS_READY = 1;
const ROOM_STATUS_LEASED = 2;
const ROOM_STATUS_CLEANUP = 3;

const TASK_STATUS_NEW = 1;
const TASK_STATUS_PROCESSING = 2;
const TASK_STATUS_RESOLVED = 3;
const TASK_STATUS_CANCELED = 4;

return [


    'LIST_STATUS' => [
        ['id' => 0, 'name' => "Đã khóa"],
        ['id' => 1, 'name' => "Hoạt động"],
    ],
    'LIST_GRADE' => [
        ['id' => 0, 'name' => "Đã khóa"],
        ['id' => 1, 'name' => "Hoạt động"],
    ],
    'LIST_TASK_STATUS' => [
        ['id' => TASK_STATUS_NEW, 'name' => "New" ],
        ['id' => TASK_STATUS_PROCESSING, 'name' => "Processing"],
        ['id' => TASK_STATUS_RESOLVED, 'name' => "Resolved"],
        ['id' => TASK_STATUS_CANCELED, 'name' => "Canceled"],
    ],'LIST_MOTEL_DETAIL_MENU' => [
        ['id' => 1, 'name' => "General Information", 'page' => 'general', 'cls_icon' => "far fa-home" ],
        ['id' => 2, 'name' => "Blocks", 'page' => 'block', 'cls_icon' => "fas fa-th" ],
        ['id' => 3, 'name' => "Floors", 'page' => 'floor', 'cls_icon' => "fab fa-buffer" ],
        ['id' => 4, 'name' => "Rooms", 'page' => 'room', 'cls_icon' => "fas fa-door-open" ],
        ['id' => 5, 'name' => "Introduction", 'page' => 'introduction', 'cls_icon' => "fas fa-folder-tree" ],
        ['id' => 6, 'name' => "Settings", 'page' => 'setting', 'cls_icon' => "fas fa-cog" ],
        ['id' => 7, 'name' => "Histories", 'page' => 'history', 'cls_icon' => "fas fa-history" ],
    ], 'LIST_PAGE' => [
        ['page' => "home", 'controller' => "HomeController"],
        ['page' => "course", 'controller' => "CourseController"],
        ['page' => "pricing", 'controller' => "PricingController"],
        ['page' => "teacher", 'controller' => "TeacherController"],
        ['page' => "blog", 'controller' => "BlogController"],
        ['page' => "about", 'controller' => "AboutController"],

    ],'LIST_SYSTEM_ROLE' => [
        ['id' => MASTER_ADMIN, 'name' => "Master admin"],
        ['id' => EMPLOYEE_ADMIN, 'name' => "System employee"],

    ],
];


