<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('address');
            $table->double('address_lat')->nullable();
            $table->double('address_lng')->nullable();
            $table->string('province')->nullable();
            $table->string('email');
            $table->string('phone_number');
            $table->string('website')->nullable();
            $table->string('favicon')->nullable();
            $table->string('logo')->nullable();
            $table->string('logo_alt')->nullable();
            $table->string('logo_title')->nullable();
            $table->string('copyright')->nullable();
            $table->string('description')->nullable();
            $table->string('title')->nullable();
            $table->string('keyword')->nullable();
            $table->string('slogan')->nullable();
            $table->string('hotline')->nullable();
            $table->string('banner')->nullable();
            $table->integer("status")->nullable()->comment("0: Inactive | 1: Active")->index();
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
