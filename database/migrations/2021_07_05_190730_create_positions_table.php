<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->comment("Mã công ty");
            $table->string("code", 50)->nullable()->comment("Mã chức danh");
            $table->string("name", 100)->nullable()->comment("Tên chức danh");

            $table->string("modules_access")->nullable()->comment("Các module được truy cập");
            $table->text("functions_access")->nullable()->comment("Các chức năng được truy cập");

            $table->text("description")->nullable()->comment("Diễn giải");
            $table->tinyInteger("status")->default(1)->comment("Trạng thái");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions');
    }
}
