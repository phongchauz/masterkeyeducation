<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->string("title")->nullable()->comment("Tiêu đều")->index();
            $table->string("slug")->nullable()->comment("Slug")->index();
            $table->string("background_color", 20)->nullable()->comment("Backgroung color")->index();
            $table->string("summary")->nullable()->comment("Tóm tắt");
            $table->string("content")->nullable()->comment("Nội dung");
            $table->string("picture")->nullable()->comment("Ảnh");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_values');
    }
}
