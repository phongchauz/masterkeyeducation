<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("ip_address", 30)->nullable()->comment("Địa chỉ IP truy cập")->index();
            $table->string("url")->nullable()->comment("Màn hình truy cập");
            $table->string("browser", 100)->nullable()->comment("Trình duyệt");
            $table->string("device_name")->nullable()->comment("Ảnh");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_logs');
    }
}
