<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassRoomRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_room_registrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->integer("course_id")->nullable()->comment("")->index();
            $table->integer("teacher_id")->nullable()->comment("")->index();
            $table->integer("class_room_id")->nullable()->comment("")->index();
            $table->string("fullname", 50)->nullable()->comment("Họ và tên");
            $table->string("phone", 50)->nullable()->comment("Điện thoại");
            $table->string("contact_email")->nullable()->comment("Thư liên hệ");
            $table->string("message")->nullable()->comment("Tin nhắn");
            $table->string("ip_address", 50)->nullable()->comment("Ip address");


            $table->tinyInteger("status")->default(1)->comment("1: New | 0: Block | 2: Processed");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_room_registrations');
    }
}
