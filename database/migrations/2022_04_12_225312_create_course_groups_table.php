<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->string("name")->nullable()->comment("Tên")->index();
            $table->string("slug")->nullable()->comment("")->index();
            $table->string("picture")->nullable()->comment("")->index();
            $table->string("cover")->nullable()->comment("")->index();
            $table->string("summary")->nullable()->comment("")->index();
            $table->text("content")->nullable()->comment("Nội dung");

            $table->tinyInteger("display_homepage")->default(1)->comment("1: Active | 0: Inactive");
            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_groups');
    }
}
