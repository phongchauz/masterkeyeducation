<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->integer("course_id")->nullable()->comment("")->index();
            $table->integer("teacher_id")->nullable()->comment("")->index();
            $table->string("code", 50)->nullable()->comment("Mã lớp");
            $table->string("monday", 50)->nullable()->comment("Thứ 2");
            $table->string("tuesday", 50)->nullable()->comment("Thứ 3");
            $table->string("wednesday", 50)->nullable()->comment("Thứ 4");
            $table->string("thursday", 50)->nullable()->comment("Thứ 5");
            $table->string("friday", 50)->nullable()->comment("Thứ 6");
            $table->string("saturday", 50)->nullable()->comment("Thứ 7");
            $table->string("sunday", 50)->nullable()->comment("Chủ nhật");
            $table->tinyInteger("slot")->nullable()->default(0)->comment("Số chỗ");
            $table->tinyInteger("slot_registered")->nullable()->default(0)->comment("Số chỗ đã đăng ký");
            $table->tinyInteger("free_slot")->nullable()->default(0)->comment("Số chỗ còn trống");
            $table->string("note")->nullable()->comment("ghi chú");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_rooms');
    }
}
