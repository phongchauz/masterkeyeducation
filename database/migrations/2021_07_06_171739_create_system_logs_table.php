<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer("action_id")->nullable()->comment("1: Insert | 2: Update | 3: Delete | 4: Import | 5: Export");
            $table->string("screen_name")->nullable();
            $table->string("source_ip")->nullable();
            $table->string("url")->nullable();
            $table->text("content")->nullable();
            $table->tinyInteger('is_active')->default(1)->nullable();
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs');
    }
}
