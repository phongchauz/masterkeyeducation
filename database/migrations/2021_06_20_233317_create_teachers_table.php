<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->string("code", 50)->nullable()->comment("Mã")->index();
            $table->string("name")->nullable()->comment("Tên")->index();
            $table->string("position")->nullable()->comment("Chức danh")->index();
            $table->string("introduction")->nullable()->comment("Giới thiệu");
            $table->string("picture")->nullable()->comment("Ảnh");
            $table->string("twitter_url")->nullable()->comment("");
            $table->string("facebook_url")->nullable()->comment("");
            $table->string("youtube_url")->nullable()->comment("");
            $table->string("instagram_url")->nullable()->comment("");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
