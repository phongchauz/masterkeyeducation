<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsCoverForCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string("cover")->nullable()->comment("Cover")->after('picture')->index();
            $table->tinyInteger("is_popular")->nullable()->default(0)->comment("0: No : 1 Yes")->after('cover')->index();
            $table->tinyInteger("display_homepage")->nullable()->default(0)->comment("0: No : 1 Yes")->after('is_popular')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            //
        });
    }
}
