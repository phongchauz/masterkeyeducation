<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsSocialNetworkForCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string("facebook_url")->nullable()->comment("Fanpage facebook")->after('banner')->index();
            $table->string("youtube_url")->nullable()->comment("Youtube chanel")->after('facebook_url')->index();
            $table->string("twitter_url")->nullable()->comment("Twitter chanel")->after('youtube_url')->index();
            $table->string("instagram_url")->nullable()->comment("Instagram")->after('twitter_url')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}
