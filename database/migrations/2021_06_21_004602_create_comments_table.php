<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->string("name")->nullable()->comment("Tên")->index();
            $table->string("relationship")->nullable()->comment("Quan hệ")->index();
            $table->text("content")->nullable()->comment("Nội dung");
            $table->string("picture")->nullable()->comment("Ảnh");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive | 2: Waiting for approved");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
