<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("role_id")->nullable()->comment("Mã phân quyền");
            $table->integer("is_admin")->nullable()->comment("Giá trị phân quyền = field is_admin trong bảng user");
            $table->integer("module_id")->nullable()->comment("Id của bảng module");
            $table->string("functions_access", 300)->nullable()->comment("Định nghĩa trong nhóm quyền này sẽ được truy cập những chức năng nào");

            $table->tinyInteger("status")->default(1)->comment("Trạng thái");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_accesses');
    }
}
