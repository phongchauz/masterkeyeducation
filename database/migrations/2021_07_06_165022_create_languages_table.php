<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("code", 50)->nullable()->comment("Mã ngôn ngữ");
            $table->string("name", 100)->nullable()->comment("Tên ngôn ngữ");

            $table->string("flag")->nullable()->comment("Cờ");
            $table->tinyInteger("status")->default(1)->comment("Trạng thái");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
