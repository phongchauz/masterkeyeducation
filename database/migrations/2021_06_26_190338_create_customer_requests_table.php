<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->string("first_name", 50)->nullable()->comment("Họ")->index();
            $table->string("last_name", 50)->nullable()->comment("Tên")->index();
            $table->integer("course_id")->nullable()->comment("Khóa học")->index();
            $table->string("email")->nullable()->comment("Email")->index();
            $table->string("phone", 100)->nullable()->comment("Điện thoại")->index();
            $table->string("message")->nullable()->comment("Lời nhắn");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_requests');
    }
}
