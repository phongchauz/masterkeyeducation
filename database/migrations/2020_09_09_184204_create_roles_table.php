<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("is_admin")->nullable()->comment("Số phân quyền");
            $table->string("code", 50)->nullable()->comment("Mã phân quyền");
            $table->string("name", 100)->nullable()->comment("Tên phân quyền");
            $table->string("description")->nullable()->comment("Diễn giải");
            $table->tinyInteger("status")->default(1)->comment("Trạng thái");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
