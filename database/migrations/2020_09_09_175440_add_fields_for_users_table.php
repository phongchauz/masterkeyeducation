<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsForUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer("company_id")->nullable()->comment("Mã công ty")->index()->after('id');
            $table->string("code", 20)->nullable()->comment("Mã")->index()->after('company_id');
            $table->string("avatar")->nullable()->comment("Ảnh đại diện")->after('name');
            $table->string("phone", 20)->nullable()->comment("Ảnh đại diện")->after('avatar');
            $table->date("birthday")->nullable()->comment("Ngày sinh")->after('phone');
            $table->string("address")->nullable()->comment("Ảnh đại diện")->after('birthday');
            $table->string("contact_email", 100)->nullable()->comment("Ảnh đại diện")->after('address');
            $table->text("self_introduction")->nullable()->comment("Giới thiệu")->after('contact_email');

            $table->string("functions_access")->nullable()->comment("Quyền truy cập chức năng")->after('remember_token');
            $table->string("modules_access")->nullable()->comment("Quyền truy cập module")->after('functions_access');
            $table->integer("is_admin")->nullable()->comment("Mã phân quyền")->index()->after('modules_access');
            $table->integer("status")->default(1)->comment("1: Hoạt động | 0: Đã khóa")->after('is_admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
