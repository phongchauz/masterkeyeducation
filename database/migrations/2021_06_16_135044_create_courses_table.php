<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();
            $table->integer("course_group_id")->nullable()->index();
            $table->string("name")->nullable()->comment("Tên")->index();
            $table->string("slug")->nullable()->comment("Slug")->index();
            $table->string("summary")->nullable()->comment("Tóm tắt");
            $table->string("class_time")->nullable()->comment("Giờ học");
            $table->string("class_level")->nullable()->comment("Khối lớp");
            $table->string("content")->nullable()->comment("Nội dung");
            $table->string("picture")->nullable()->comment("Ảnh");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
