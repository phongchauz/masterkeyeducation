<?php

use App\Helpers\BaseService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id")->nullable()->index();

            $table->string("slogan_label")->nullable()->comment("Nhãn tuyên ngôn");
            $table->text("slogan_content")->nullable()->comment("Nội dung tuyên ngôn");
            $table->string("slogan_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("slogan_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("mission_label")->nullable()->comment("Nhãn sứ mệnh");
            $table->text("mission_content")->nullable()->comment("Nội dung sứ mệnh");
            $table->string("mission_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("mission_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("registration_label")->nullable()->comment("Nhãn đăng ký");
            $table->text("registration_content")->nullable()->comment("Nội dung đăng ký");
            $table->string("registration_picture")->nullable()->comment("Ảnh nền đăng ký");
            $table->tinyInteger("registration_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("teacher_label")->nullable()->comment("Nhãn giáo viên");
            $table->text("teacher_content")->nullable()->comment("Nội dung giáo viên");
            $table->string("teacher_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("teacher_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("course_label")->nullable()->comment("Nhãn khóa học");
            $table->text("course_content")->nullable()->comment("Nội dung khóa học");
            $table->string("course_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("course_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("experience_label")->nullable()->comment("Nhãn kinh nghiệm");
            $table->text("experience_content")->nullable()->comment("Nội dung kinh nghiệm");
            $table->string("experience_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("experience_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("comment_label")->nullable()->comment("Nhãn ý kiến");
            $table->text("comment_content")->nullable()->comment("Nội dung ý kiến");
            $table->string("comment_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("comment_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("registration_form_label")->nullable()->comment("Nhãn form đăng ký");
            $table->text("registration_form_content")->nullable()->comment("Nội dung form đăng ký");
            $table->string("registration_form_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("registration_form_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("pricing_label")->nullable()->comment("Nhãn bảng giá");
            $table->text("pricing_content")->nullable()->comment("Nội dung bảng giá");
            $table->string("pricing_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("pricing_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->string("blog_label")->nullable()->comment("Nhãn blog");
            $table->text("blog_content")->nullable()->comment("Nội dung blog");
            $table->string("blog_picture")->nullable()->comment("Ảnh kinh nghiệm");
            $table->tinyInteger("blog_display_homepage")->default(0)->nullable()->comment("Hiện trang chủ");

            $table->tinyInteger("status")->default(1)->comment("1: Active | 0: Inactive");
            BaseService::commonFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
