<?php

use App\FunctionMenu;
use App\Helpers\BaseService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FunctionMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('function_menus')->truncate();

        $list = [

            // ================================================================== Admin Portal================================================================================================== //
            // Dashboard
            ['code' => 'adm_dashboard', 'module_id' => 1, "parent_code" => null, "semi_parent_code" => null, "name" => "Dashboard",'plugin' => "admin", "controller" => "dashboard", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-chart-pie", "link" => "admin/dashboard/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Homepage
            ['code' => 'adm_homepage', 'module_id' => 1, "parent_code" => null, "semi_parent_code" => null, "name" => "Homepage",'plugin' => "", "controller" => "", 'action' => '', "params" => null, "cls_icon" => "fas fa-globe-americas", "link" => null, "is_link" => 0, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 2],
            // Business
            ['code' => 'adm_business', 'module_id' => 1, "parent_code" => null, "semi_parent_code" => null, "name" => "Business",'plugin' => "", "controller" => "", 'action' => '', "params" => null, "cls_icon" => "fas fa-business-time", "link" => null, "is_link" => 0, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 3],
            // Settings
            ['code' => 'adm_setting', 'module_id' => 1, "parent_code" => null, "semi_parent_code" => null, "name" => "Settings",'plugin' => "", "controller" => "", 'action' => '', "params" => null, "cls_icon" => "fas fa-cog", "link" => null, "is_link" => 0, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 3],


        ];

        $listLevel2 = [
            // ================================================================== Admin Portal Level 2 ================================================================================================== //
            // Language Translation
            ['code' => 'adm_language_translation', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Language Translation",'plugin' => "admin", "controller" => "languageTranslation", 'action' => 'index', "params" => null, "cls_icon" => "fal fa-language", "link" => "admin//languageTranslation/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Company List
//            ['code' => 'adm_company', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Company List",'plugin' => "admin", "controller" => "company", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-building", "link" => "admin/company/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_company_profile', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Company Profile",'plugin' => "admin", "controller" => "company", 'action' => 'profile', "params" => null, "cls_icon" => "fas fa-building", "link" => "admin/company/profile", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // User List
            ['code' => 'adm_user', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "User List",'plugin' => "admin", "controller" => "user", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-users", "link" => "admin/user/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Modules
            ['code' => 'adm_module', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Modules",'plugin' => "admin", "controller" => "module", 'action' => 'index', "params" => null, "cls_icon" => "fab fa-uncharted", "link" => "admin/module/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Plan Package
            ['code' => 'adm_plan_package', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Plan Packages",'plugin' => "admin", "controller" => "planPackage", 'action' => 'index', "params" => null, "cls_icon" => "fab fa-battle-net", "link" => "admin/planPackage/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Plan Package
            ['code' => 'adm_position', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => null, "name" => "Positions",'plugin' => "admin", "controller" => "position", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-chair-office", "link" => "admin/position/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            // Mission

            // Article
            ['code' => 'adm_article', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Article List",'plugin' => "admin", "controller" => "article", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-newspaper", "link" => "admin/article/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            //Banner
            ['code' => 'adm_banner', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Banner",'plugin' => "admin", "controller" => "banner", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-presentation", "link" => "admin/banner/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_homepage_setting', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Setting",'plugin' => "admin", "controller" => "siteSetting", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-cog", "link" => "admin/siteSetting/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_course', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Courses Group",'plugin' => "admin", "controller" => "courseGroup", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-books", "link" => "admin/courseGroup/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_course', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Courses",'plugin' => "admin", "controller" => "course", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-books", "link" => "admin/course/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_class_room', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Class Rooms",'plugin' => "admin", "controller" => "classRoom", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-door-closed", "link" => "admin/classRoom/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_teacher', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Teachers",'plugin' => "admin", "controller" => "teacher", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-user-tie", "link" => "admin/teacher/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_comment', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Comments",'plugin' => "admin", "controller" => "comment", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-comments", "link" => "admin/comment/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_gallery', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Galleries",'plugin' => "admin", "controller" => "gallery", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-images", "link" => "admin/gallery/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_core_value', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Core Values",'plugin' => "admin", "controller" => "coreValue", 'action' => 'index', "params" => null, "cls_icon" => "fab fa-creative-commons-remix", "link" => "admin/coreValue/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_mission', 'module_id' => 1, "parent_code" => 'adm_homepage', "semi_parent_code" => null, "name" => "Missions",'plugin' => "admin", "controller" => "mission", 'action' => 'index', "params" => null, "cls_icon" => "fab fa-stack-overflow", "link" => "admin/mission/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],

            ['code' => 'adm_customer_request', 'module_id' => 1, "parent_code" => 'adm_business', "semi_parent_code" => null, "name" => "Customer Request",'plugin' => "admin", "controller" => "customerRequest", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-hand-paper", "link" => "admin/customerRequest/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_customer_registration', 'module_id' => 1, "parent_code" => 'adm_business', "semi_parent_code" => null, "name" => "Classroom Registration",'plugin' => "admin", "controller" => "classRoomRegistration", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-file-signature", "link" => "admin/classRoomRegistration/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],
            ['code' => 'adm_subscribe', 'module_id' => 1, "parent_code" => 'adm_business', "semi_parent_code" => null, "name" => "Subscribes",'plugin' => "admin", "controller" => "subscribe", 'action' => 'index', "params" => null, "cls_icon" => "fas fa-bells", "link" => "admin/subscribe/index", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 1, "ordinal" => 1],


        ];

        $listLevel3 = [

            // ================================================================== Admin Portal Level 3 ================================================================================================== //
            // Company Details
            ['code' => 'adm_company_detail', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => 'adm_company', "name" => "Company Detail",'plugin' => "admin", "controller" => "company", 'action' => 'detail', "params" => null, "cls_icon" => "fas fa-building", "link" => "admin/company/detail", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 0, "ordinal" => 1],
            // User Details
            ['code' => 'adm_user_detail', 'module_id' => 1, "parent_code" => 'adm_setting', "semi_parent_code" => 'adm_user', "name" => "User Detail",'plugin' => "admin", "controller" => "user", 'action' => 'profile', "params" => null, "cls_icon" => "fas fa-user", "link" => "admin/user/profile", "is_link" => 1, "is_fullscreen" => 0, "is_ajax" => 0, "open_new_tab" => 0, "display" => 0, "ordinal" => 2],


        ];

        foreach ($list as $key => $item){

            $temp = $item;
            unset($temp['code']);
            unset($temp['parent_code']);
            unset($temp['semi_parent_code']);

            $object = new FunctionMenu();
            $object = BaseService::renderObject($object, $temp);
            $object->save();

            $id = $object->id;
            $list[$key]['id'] = $id;

        }

        foreach ($listLevel2 as $key2 => $level2){

            $code = $level2['code'];
            $parent_code = $level2['parent_code'];
            $semi_parent_code = $level2['semi_parent_code'];

            $temp = $level2;
            unset($temp['code']);
            unset($temp['parent_code']);
            unset($temp['semi_parent_code']);

            $parent_id = null;

            foreach ($list as $parent){
                if($parent['code'] == $parent_code){
                    $parent_id = $parent['id'];
                    break;
                }
            }

            if(!$parent_id){
                continue;
            }

            $temp['parent_id'] = $parent_id;
            $object = new FunctionMenu();
            $object = BaseService::renderObject($object, $temp);
            $object->save();
            $id = $object->id;

            $listLevel2[$key2]['parent_id'] = $parent_id;
            $listLevel2[$key2]['id'] = $id;
        }

        foreach ($listLevel3 as $key3 => $level3){

            $code = $level3['code'];
            $parent_code = $level3['parent_code'];
            $semi_parent_code = $level3['semi_parent_code'];

            $temp = $level3;
            unset($temp['code']);
            unset($temp['parent_code']);
            unset($temp['semi_parent_code']);

            $semi_parent_id = null;
            $parent_id = null;
            foreach ($listLevel2 as $level2){
                if($level2['code'] == $semi_parent_code){
                    $parent_id = $level2['parent_id'];
                    $semi_parent_id = $level2['id'];
                    break;
                }
            }

            if(!$parent_id){
                continue;
            }

            $temp['semi_parent_id'] = $semi_parent_id;
            $temp['parent_id'] = $parent_id;

            $object = new FunctionMenu();
            $object = BaseService::renderObject($object, $temp);
            $object->save();
            $id = $object->id;

        }

    }
}
