<?php

use App\Helpers\BaseService;
use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('companies')->truncate();

        $list = [
            ['name' => "Master Key Education", 'address' => "24/5/4 Trương Phước Phan, Bình Trị Đông, Bình Tân, Hồ Chí Minh", "email" => 'infor@thanhsocla.com', 'phone_number' => "0399 980 036", 'status' => STATUS_ACTIVE],
        ];

        foreach ($list as $key => $item){
            $object = new Company();
            $object = BaseService::renderObject($object, $item);
            $object->save();
        }
    }
}
