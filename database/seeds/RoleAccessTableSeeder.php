<?php

use App\Helpers\BaseService;
use App\RoleAccess;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleAccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_accesses')->truncate();

        $list = [
            ['name' => "Master Admin", 'is_admin' => 1],
            ['name' => "Company", 'is_admin' => 2],
            ['name' => "Staff", 'is_admin' => 3],
            ['name' => "Guest", 'is_admin' => 4],
        ];

        foreach ($list as $key => $item){
            $object = new RoleAccess();
            $object = BaseService::renderObject($object, $item);
            $object->save();
        }
    }
}
