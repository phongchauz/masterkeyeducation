<?php

use App\Helpers\BaseService;
use App\Module;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('modules')->truncate();

        $list = [
            ['code' => "adm", 'name' => "Admin Portal", 'status' => STATUS_ACTIVE],
        ];

        foreach ($list as $key => $item){
            $object = new Module();
            $object = BaseService::renderObject($object, $item);
            $object->save();
        }
    }
}
