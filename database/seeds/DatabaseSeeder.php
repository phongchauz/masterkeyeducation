<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $toTruncate = ['modules', 'function_menus'];

    public function run()
    {

        Model::unguard();

        $this->call(ModulesTableSeeder::class);
        $this->call(FunctionMenuTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        Model::reguard();

        // $this->call(UsersTableSeeder::class);
    }
}
