
$(document).ready(function () {

    if(firstTimes){
        loadDataRoom();
        firstTimes = 0;
    }

    $(document).on("click", ".btn-new-rent", function (e) {
        var room_id = $(this).attr('data-id');
        $("input[name='room_id']").val(room_id);

        showModal('#modal-new-rent');
    });

    focusInputModal('#modal-new-rent', $('input[name="tenant"]'));



    saveDataRent();

});



function loadDataRoom() {

    loadingStart();
    $.ajax({
        url: APP.ApiUrl('customer_portal/room/loadDataRoom'),
        type: 'POST',
        data: {
            _token: _token,
            motel_id: motel_id,
        },
        success: function (data) {
            var response = $.parseJSON(data);
            loadingEnd();
            if (response.success) {
                $('#card-room').html(response.contentHtml);
                $("#total_ready").text(response.total_ready);
                $("#total_leased").text(response.total_leased);
                $("#total_clean").text(response.total_clean);
                $("#total_not_use").text(response.total_not_use);
            }else{
                slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
            }
        },
        cache: false
    });
}

function saveDataRent() {

    $("form#form-data-rent").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('customer_portal/roomContract/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data-rent")[0].reset();

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}

