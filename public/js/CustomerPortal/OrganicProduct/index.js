var searchTable;

$(document).ready(function () {

    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "picture",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderImage(row);
            }
        },{
            class: "td-no-wrap text-center",
            orderable: true,
            name: "code",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return row.code;
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "name",
            defaultContent: "",
            data: "name"
        },{
            class: "td-no-wrap text-center",
            orderable: true,
            name: "unit_name",
            defaultContent: "",
            data: "unit_name"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "price",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return formatNumber(row.price, 0);
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "category_name",
            defaultContent: "",
            data: "category_name"
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "description",
            defaultContent: "",
            data: "description"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a href="javascript:;" class="btn btn-sm btn-round btn-primary btn-edit tooltips mr-1" title="' + TRANSLATED_LABELS.lblEdit + '"><i class="fas fa-pencil"></i></a>';
                html += '<a class="btn btn-sm btn-round btn-danger btn-delete tooltips" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="far fa-trash-alt"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('customer_portal/organicProduct/search'),
            type: "post",
            data: function (d) {
                d._token = _token;
                d.category_id = $("#s_category_id").val();
            }
        },
        columns: column
    }, true);

    $('#modalEdit').on('shown.bs.modal', function () {
        $("input[name='code']").focus();
    });

    $(document).on("click", ".btn-refresh", function (e) {
        searchTable.ajax.reload();
    });

    $(document).on("click", ".btn-add", function (e) {
        $(".continue-add").removeClass("hide");
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h5").text(titleAdd);

        $(".chk-continue").removeClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-edit", function(e){
        e.preventDefault();
        $(".continue-add").addClass("hide");
        $("#modalEdit").find("h5").text(titleEdit);
        $("input[name='status']").prop("checked", false);

        var data = getRowDataMaster(this,searchTable);
        $("input[name='id']").val(data.id);
        $("input[name='code']").val(data.code);
        $("input[name='name']").val(data.name);
        $("input[name='price']").val(data.price);
        $("select[name='category_id']").val(data.category_id);
        $("select[name='unit_id']").val(data.unit_id);
        $("textarea[name='description']").val(data.description);

        if(data.picture != "" && data.picture != null) {
            var pathFile = APP.ApiUrl("public/uploads/CustomerPortal/OrganicProduct/" + data.id + "/" + data.picture);
            var listPathFile = [pathFile];

            var initialPreviewConfig = [
                {"caption": data.picture, "downloadUrl": pathFile, "key": 0}
            ];

            reInitFileInput($("#picture"), listPathFile, initialPreviewConfig);
        } else {
            reInitFileInput($("#picture"));
        }

        if(data.status){
            $("input[name='status']").prop("checked", true);
        }

        $('.combobox-search').selectpicker('refresh');
        $("input[name='continue']").prop("checked", false);
        $(".chk-continue").addClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        var id = $(this).attr("data-id");
        deleteData(id);
    });

    $(document).on("click", ".btn-import", function(){
        showModal("#modalImport") ;
    });

    $(document).on("submit", "#form-data-import", function(e){
        e.preventDefault();
        $.loadingStart();

        var formData = new FormData(this);
        $.ajax({
            url: APP.ApiUrl("customer_portal/organicProduct/saveImportData"),
            dataType: "json",
            type: "POST",
            data: formData,
            success: function(resp){
                var type_danger = "danger";
                if(resp.success){
                    type_danger = "success";
                    hideModal("#modalImport");
                    searchTable.ajax.reload(function(){
                        $.loadingEnd();
                    });
                }
                $(".fileinput-remove-button").click();
                $("#form-data-import")[0].reset();
                slideMessage("Thông báo", resp.message, type_danger);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $(document).on('click', '.btn-export', function(e) {
        e.preventDefault();
        var buttonDownload = $("#btn-download-file");
        $('#header-waiting').text("Process Data Report");
        $('#content-waiting').text("Please wait a few seconds when system processing data report...");
        $('#modal-waiting').modal('show');
        buttonDownload.addClass('hide');

        var valueSearch = $("form#searchBox").find('input.form-control').val();
        $.ajax({
            url: APP.ApiUrl('admin/user/export'),
            type: 'POST',
            data: {q: valueSearch},
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage("Success", response.alert, 'success');
                    buttonDownload.removeClass('hide');
                    buttonDownload.attr('href', response.file);
                    buttonDownload.attr('download', response.filename);
                    $('#content-waiting').text("Process data complete, click Download to download file report.");
                }else{
                    slideMessage("Lỗi", response.alert, 'danger');
                }
            },
            cache: false,
            //contentType: false,
            //processData: false
        });
    });

    saveData();
});


function renderStatus(status) {
    if (status == 0) {

        return '<span class="badge badge-danger">' + TRANSLATED_LABELS.lblLock + '</span>';

    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblActive + '</span>';
}

function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('customer_portal/organicProduct/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();

                    if(response.is_update){
                        searchTable.draw(false);
                    }else{
                        searchTable.ajax.reload();
                    }


                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}


function deleteData(id) {
    ajax({
        url: APP.ApiUrl("customer_portal/organicProduct/delete"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete");
            searchTable.ajax.reload();
        }
    }, true, false);
}

function renderImage(data) {

    var picture  = data.picture;
    var picture_path = PUBLIC_PATH + "/images/no_img.png";
    if(picture !== "" && picture != null){
        picture_path = PUBLIC_PATH + "/uploads/CustomerPortal/OrganicProduct/"+ data.id + "/" + picture;
    }

    return "<img class='image-gird' src='"+picture_path+"' />";
}
