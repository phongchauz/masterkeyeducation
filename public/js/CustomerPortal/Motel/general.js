$(document).ready(function () {
    saveGeneral();

});



function saveGeneral() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('customer_portal/motel/saveGeneral'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    $("#motel-code").text('@'+response.data.code);
                    $("#motel-name").text(response.data.name);
                    var picturePath = preMotelPicturePath+'/'+response.data.picture;
                    $("#motel-picture").css('background-image', 'url(' + picturePath + ')');
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}
