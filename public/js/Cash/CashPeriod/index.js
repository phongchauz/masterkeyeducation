var searchTable;

$(document).ready(function () {

    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "name",
            defaultContent: "",
            data: "name"
        },{
            class: "td-no-wrap text-right",
            orderable: true,
            name: "income",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return formatNumber(row.income, ',');
            }
        },{
            class: "td-no-wrap text-right",
            orderable: true,
            name: "outcome",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return formatNumber(row.outcome, ',');
            }
        },{
            class: "td-no-wrap text-right",
            orderable: true,
            name: "revenue",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return formatNumber(row.revenue, ',');
            }
        },
      {
            class: "text-center td-no-wrap",
            orderable: true,
            name: "note",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return row.note;
            }
        }, {
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a href="javascript:;" class="btn btn-sm btn-round btn-primary btn-calculate tooltips mr-1" title="' + TRANSLATED_LABELS.lblCalculator + '"><i class="fas fa-calculator"></i></a>';
                html += '<a href="javascript:;" class="btn btn-sm btn-round btn-primary btn-export tooltips " title="' + TRANSLATED_LABELS.lblExport + '"><i class="fas fa-download"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [1, "asc"]
        ],
        ajax: {
            url: APP.ApiUrl('cash/cashPeriod/search'),
            type: "post",
            data: function (d) {
                d.year = $("#s_year").val();
                return d;
            }
        },
        columns: column,
        fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            var row_cls = "";
            if(aData.revenue <= 0 && (aData.income > 0 || aData.outcome > 0)) {
                row_cls = "row-danger";
            }

            nRow.className = row_cls;

        }
    }, true);



    $(document).on("click", ".btn-refresh", function (e) {
        searchTable.ajax.reload();
    });

    $(document).on("click", ".btn-add", function (e) {
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h5").text(titleAdd);
        showModal('#modalEdit');
    });

    saveData();



    $(document).on("click", ".btn-calculate", function(e){
        e.preventDefault();

        var data = getRowDataMaster(this, searchTable);
        var modal = $("#modalConfirm");
        modal.find("h5").text(TRANSLATED_LABELS.lblConfirm);
        modal.find("#content-confirm").text("Are you sure you want to calculator data for this month: "+data.name+"?");


        if(data) {
            $("#btn-calculator-object").attr("data-id", data.id);
            showModal("#modalConfirm");
        }
    });

    $(document).on("click", "#btn-calculator-object", function (e) {

        var modal = $("#modalConfirm");
        modal.find("h5").text(TRANSLATED_LABELS.lblCalculating);
        modal.find("#content-confirm").text("Please waiting a few seconds when system calculating data...");

        var id = $(this).attr("data-id");
        calculatorData(id);
    });
});


function renderStatus(status) {
    if (status == 0) {

        return '<span class="badge badge-danger">' + TRANSLATED_LABELS.lblLock + '</span>';

    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblActive + '</span>';
}

function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('cash/cashPeriod/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();
                    searchTable.ajax.reload();
                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}


function calculatorData(id) {
    ajax({
        url: APP.ApiUrl("cash/cashPeriod/calculator"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modalConfirm");
            searchTable.ajax.reload();
        }
    }, true, false);
}

