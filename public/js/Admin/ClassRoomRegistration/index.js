var searchTable;
$(document).ready(function () {


    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },
        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "code",
            defaultContent: "",
            data: "code"
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "course_name",
            defaultContent: "",
            data: "course_name"
        },{
            class: "",
            orderable: true,
            name: "fullname",
            defaultContent: "",
            data: "fullname"
        },{
            class: "",
            orderable: true,
            name: "phone",
            defaultContent: "",
            data: "phone"
        },{
            class: "",
            orderable: true,
            name: "contact_email",
            defaultContent: "",
            data: "contact_email"
        },{
            class: "",
            orderable: true,
            name: "message",
            defaultContent: "",
            data: "message"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a class="btn btn-sm btn-round btn-primary btn-approval  tooltips mr-1" title="' + TRANSLATED_LABELS.lblApproval + '"><i class="fas fa-file-search"></i></a>';

                if(row.status == 2){
                    return html;
                }

                html += '<a class="btn btn-sm btn-round btn-danger btn-delete tooltips mr-1" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="far fa-trash-alt"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('admin/classRoomRegistration/search'),
            type: "post",
            data: function (d) {
                d._token = _token;
                d.course_id = $("#s_course_id").val();
                d.class_room_id = $("#s_class_room_id").val();
                d.key_search = $("#s_key_search").val();
                d.status = $("#s_status").val();
            }
        },
        columns: column
    }, true);


    $(document).on("change", "select.change-data", function (e) {
        e.preventDefault();
        var type = $(this).attr('name');
        var value_change = $(this).val();
        $.ajax({
            url: APP.ApiUrl('admin/classRoomRegistration/changeData'),
            type: 'POST',
            data: {
                _token: _token,
                type: type,
                value_change: value_change
            },
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {

                    switch (response.type){
                        case 's_course_id':
                            $('#s-classroom-area').html(response.contentClassroomHtml);
                            break;

                    }

                    $('.combobox-search').selectpicker('refresh');
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false
        });

    });

    $("form#form-search").submit(function(e) {
        e.preventDefault();
        searchTable.ajax.reload();
    });

    $(document).on("click", ".btn-approval", function(e){
        e.preventDefault();
        $("#modalEdit").find("h5").text(titleApproval);

        var data = getRowDataMaster(this,searchTable);
        $("input[name='id']").val(data.id);
        $("#show_course").find(".col-code").text(": "+data.code);
        $("#show_course").find(".col-course").text(": "+data.course_name);


        $("#show_fullname").find(".col-fullname").text(": "+data.fullname);
        $("#show_fullname").find(".col-phone").text(": "+data.phone);
        $("#show_fullname").find(".col-email").text(": "+data.contact_email);

        $("#show_message").html(data.message);

        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-submit-approval", function(e){
        e.preventDefault();
        var status = $(this).attr('data-status');
        var form = $("#form-data");
        form.find("input[name='status']").val(status);
        form.find("input[name='_token']").val(_token);

        var formData = new FormData(form[0]);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/classRoomRegistration/saveData'),
            type: 'POST',
            dataType:"json",
            data: formData,
            success: function (response) {

                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();
                    searchTable.draw(false);
                    hideModal("#modalEdit");

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        var id = $(this).attr("data-id");
        deleteData(id);
    });


});



function renderStatus(status) {
    if (status == 0) {
        return '<span class="badge badge-danger">' + TRANSLATED_LABELS.lblCanceled + '</span>';
    }
    if (status == 1) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblNew + '</span>';
    }
    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblApproved + '</span>';
}





function deleteData(id) {
    ajax({
        url: APP.ApiUrl("admin/classRoomRegistration/delete"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete");
            searchTable.ajax.reload();
        }
    }, true, false);
}


