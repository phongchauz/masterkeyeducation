var searchTable;

$(document).ready(function () {

    var columns = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "image",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderImageGallery(row);
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "picture_url",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var picturePath = PUBLIC_PATH + "/uploads/"+SYSTEM_CONSTANT.COMPANY_ID+"/Galleries/"+ row.id + "/" + row.picture;
                return "<a target='_blank' href='"+picturePath+"'>"+picturePath+"</a>";
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a class="btn btn-sm btn-round btn-danger btn-delete-gallery tooltips mr-1" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="fas fa-trash"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('admin/gallery/search'),
            type: "post",
            data: function (d) {
                d._token = _token;
            }
        },
        columns: columns
    }, true);


    $(document).on("click", ".btn-add", function (e) {
        $(".continue-add").removeClass("hide");
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h5").text(titleAdd);
        $('.combobox-search').selectpicker('refresh');
        $(".chk-continue").removeClass("hide");
        showModal('#modalEdit');
    });

    

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm-delete").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm-delete").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        deleteData();
    });

    $(document).on("click", ".btn-delete-all", function(e){
        e.preventDefault();
        $("#header-confirm-delete-all").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm-delete-all").text(TRANSLATED_LABELS.lblConfirmDeleteAll+'?');
        $("#modal-confirm-delete-all").modal("show");

    });

    $(document).on("click", "#btn-delete-object-all", function (e) {
        deleteDataAll();
    });

    saveData();

});



function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/gallery/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();
                    searchTable.ajax.reload();
                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }

                    $("input[name='code']").focus();

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}

function deleteData() {
    ajax({
        url: APP.ApiUrl("admin/gallery/delete"),
        type: "POST",
        data: {},
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete-gallery");
            searchTable.ajax.reload();
        }
    }, true, false);
}

function deleteDataAll() {
    ajax({
        url: APP.ApiUrl("admin/gallery/deleteAll"),
        type: "POST",
        data: {},
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete-all");
            searchTable.ajax.reload();
        }
    }, true, false);
}

function renderImageGallery(data) {

    var picture  = data.picture;
    var picture_path = PUBLIC_PATH + "/images/no_img.png";
    if(picture !== "" && picture != null){
        picture_path = PUBLIC_PATH + "/uploads/"+SYSTEM_CONSTANT.COMPANY_ID + "/Galleries/"+ data.id + "/" + picture;
    }

    return "<img class='image-gird' src='"+picture_path+"' />";
}
