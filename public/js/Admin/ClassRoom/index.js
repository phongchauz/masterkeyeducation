var searchTable;

$(document).ready(function () {

    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "",
            orderable: true,
            name: "code",
            defaultContent: "",
            data: "code"
        },{
            class: "td-no-wrap ",
            orderable: true,
            name: "class_time",
            defaultContent: "",
            data: function (row, type, set, meta) {

                var monday = row.monday;
                var tuesday = row.tuesday;
                var wednesday = row.wednesday;
                var thursday = row.thursday;
                var friday = row.friday;
                var saturday = row.saturday;
                var sunday = row.sunday;

                var html = "<table class='table-class-time' style='border: none !important;'>";

                if(monday != "" && monday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblMonday+"</td><td class='w-80px'>: "+monday+"</td></tr>";}
                if(tuesday != "" && tuesday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblTuesday+"</td><td class='w-80px'>: "+tuesday+"</td></tr>";}
                if(wednesday != "" && wednesday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblWednesday+"</td><td class='w-80px'>: "+wednesday+"</td></tr>";}
                if(thursday != "" && thursday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblThursday+"</td><td class='w-80px'>: "+thursday+"</td></tr>";}
                if(friday != "" && friday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblFriday+"</td><td class='w-80px'>: "+friday+"</td></tr>";}
                if(saturday != "" && saturday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblSaturday+"</td><td class='w-80px'>: "+saturday+"</td></tr>";}
                if(sunday != "" && sunday != null){html += "<tr><td class='w-60px'> * "+ TRANSLATED_LABELS.lblSunday+"</td><td class='w-80px'>: "+sunday+"</td></tr>";}

                html += "</table>";

                return html;
            }
        },{
            class: "",
            orderable: true,
            name: "course_name",
            defaultContent: "",
            data: "course_name"
        },{
            class: "",
            orderable: true,
            name: "teacher_name",
            defaultContent: "",
            data: "teacher_name"
        },{
            class: "text-center",
            orderable: true,
            name: "slot",
            defaultContent: "",
            data: "slot"
        },{
            class: "text-center",
            orderable: true,
            name: "slot_registered",
            defaultContent: "",
            data: "slot_registered"
        },{
            class: "text-center",
            orderable: true,
            name: "free_slot",
            defaultContent: "",
            data: "free_slot"
        },{
            class: "",
            orderable: true,
            name: "note",
            defaultContent: "",
            data: "note"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a class="btn btn-sm btn-round btn-primary btn-edit  tooltips mr-1" title="' + TRANSLATED_LABELS.lblEdit + '"><i class="fas fa-pencil"></i></a>';
                html += '<a class="btn btn-sm btn-round btn-danger btn-delete tooltips mr-1" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="far fa-trash-alt"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('admin/classRoom/search'),
            type: "post"
        },
        columns: column
    }, true);


    $(document).on("click", ".btn-add", function (e) {
        $(".continue-add").removeClass("hide");
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h5").text(titleAdd);

        $('.combobox-search').selectpicker('refresh');
        $(".chk-continue").removeClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("change keyup pasted", "#name", function (e) {
        clearTimeout(lastChange);
        lastChange = setTimeout(function(){
            $.ajax({
                url: APP.ApiUrl('admin/coreData/renderSlug'),
                type: 'POST',
                dataType: 'json',
                data: {
                    str: $("#name").val()
                },
                success: function(response) {
                    if (response.success) {
                        $("input[name='slug']").val(response.slug);
                    }
                }
            });
        }, 500);
    });

    $(document).on("click", ".btn-edit", function(e){
        e.preventDefault();
        $(".continue-add").addClass("hide");
        $("#modalEdit").find("h5").text(titleEdit);
        $("input[name='status']").prop("checked", false);

        var data = getRowDataMaster(this,searchTable);

        $("input[name='id']").val(data.id);
        $("input[name='code']").val(data.code);
        $("input[name='monday']").val(data.monday);
        $("input[name='tuesday']").val(data.tuesday);
        $("input[name='wednesday']").val(data.wednesday);
        $("input[name='thursday']").val(data.thursday);
        $("input[name='friday']").val(data.friday);
        $("input[name='saturday']").val(data.saturday);
        $("input[name='sunday']").val(data.sunday);
        $("select[name='course_id']").val(data.course_id);
        $("select[name='teacher_id']").val(data.teacher_id);
        $("input[name='slot']").val(data.slot);
        $("textarea[name='note']").val(data.note);

        if(data.status){
            $("input[name='status']").prop("checked", true);
        }



        $('.combobox-search').selectpicker('refresh');
        $("input[name='continue']").prop("checked", false);
        $(".chk-continue").addClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        var id = $(this).attr("data-id");
        deleteData(id);
    });

    saveData();

});

function renderStatus(status) {
    if (status == 0) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblLock + '</span>';
    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblActive + '</span>';
}

function renderMain(is_main) {
    if (is_main == 0) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblNo + '</span>';
    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblYes + '</span>';
}

function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/classRoom/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();
                    searchTable.ajax.reload();
                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }

                    $("input[name='code']").focus();

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}


function deleteData(id) {
    ajax({
        url: APP.ApiUrl("admin/classRoom/delete"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete");
            searchTable.ajax.reload();
        }
    }, true, false);
}

