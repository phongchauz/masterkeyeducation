$(document).ready(function () {

    $(document).on("click", ".access-module", function (e) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-href');
        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/module/writeSessionModule'),
            type: 'POST',
            data: {module_id: id, url: url},
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    window.location.href = response.url;
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false
        });
    });


});

