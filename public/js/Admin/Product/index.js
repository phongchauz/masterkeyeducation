var searchTable;

$(document).ready(function () {

    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "picture",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderImage(row);
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "code",
            defaultContent: "",
            data: "code"
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "name",
            defaultContent: "",
            data: "name"
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "unit_name",
            defaultContent: "",
            data: "unit_name"
        },{
            class: "td-no-wrap text-right",
            orderable: true,
            name: "price",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return formatNumber(row.price);
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "product_type_name",
            defaultContent: "",
            data: "product_type_name"
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "description",
            defaultContent: "",
            data: "description"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a class="btn btn-sm btn-round btn-primary btn-edit  tooltips mr-1" title="' + TRANSLATED_LABELS.lblEdit + '"><i class="fas fa-pencil"></i></a>';
                html += '<a class="btn btn-sm btn-round btn-danger btn-delete tooltips mr-1" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="far fa-trash-alt"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('admin/product/search'),
            type: "post",
            data: function (d) {
                d._token = _token;
                d.product_type_id = $("#s_product_type_id").val();
                d.search_text = $("#s_key_search").val();
                return d;
            }
        },
        columns: column
    }, true);


    $(document).on("click", ".btn-add", function (e) {
        $(".continue-add").removeClass("hide");
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h4").text(titleAdd);

        $('.combobox-search').selectpicker('refresh');
        $(".chk-continue").removeClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-edit", function(e){
        e.preventDefault();
        $(".continue-add").addClass("hide");
        $("#modalEdit").find("h4").text(titleEdit);
        $("input[name='status']").prop("checked", false);

        var data = getRowDataMaster(this,searchTable);

        $("input[name='id']").val(data.id);
        $("input[name='code']").val(data.code);
        $("input[name='name']").val(data.name);
        $("input[name='slug']").val(data.slug);
        $("input[name='price']").val(data.price);
        $("select[name='unit_id']").val(data.unit_id);
        $("select[name='product_type_id']").val(data.product_type_id);
        $("textarea[name='description']").val(data.description);



        if(data.picture != "" && data.picture != null) {
            var pathFile = APP.ApiUrl("public/uploads/"+data.company_id+"/Product/" + data.id + "/" + data.picture);
            var listPathFile = [pathFile];

            var initialPreviewConfig = [
                {"caption": data.icon, "downloadUrl": pathFile, "key": 0}
            ];

            reInitFileInput($("#picture"), listPathFile, initialPreviewConfig);
        } else {
            reInitFileInput($("#picture"));
        }


        if(data.status){
            $("input[name='status']").prop("checked", true);
        }

        $('.combobox-search').selectpicker('refresh');
        $("input[name='continue']").prop("checked", false);
        $(".chk-continue").addClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        var id = $(this).attr("data-id");
        deleteData(id);
    });

    $(document).on("submit", "#form-search", function (e) {
        e.preventDefault();
        searchTable.ajax.reload();
    });

    $(document).on("click", ".btn-import", function(e){
        e.preventDefault();
        $("#modalImport").find("h4").text(TRANSLATED_LABELS.lblImport);
        $("form#form-data-import")[0].reset();
        $("#file").fileinput('refresh', {
            initialPreview: ''
        });
        showModal("#modalImport")
    });

    saveData();
    importData();

});

function importData() {
    $("form#form-data-import").submit(function(e) {
        var formData = new FormData(this);
        e.preventDefault();
        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/product/saveImportData'),
            type: 'POST',
            data: formData,
            success: function (data) {

                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    $("form#form-data-import")[0].reset();
                    searchTable.ajax.reload();
                    hideModal("modalImport");
                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}

function renderStatus(status) {
    if (status == 0) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblLock + '</span>';
    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblActive + '</span>';
}


function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/product/saveData'),
            type: 'POST',
            data: formData,
            dataType:"json",
            success: function (response) {
                // var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();

                    if(response.is_new){
                        searchTable.ajax.reload();
                    }else{
                        searchTable.draw(false);
                    }

                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }

                    $("input[name='code']").focus();

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}


function deleteData(id) {
    ajax({
        url: APP.ApiUrl("admin/product/delete"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete");
            searchTable.ajax.reload();
        }
    }, true, false);
}

function renderImage(data) {

    var picture  = data.picture;
    var picture_path = PUBLIC_PATH + "/images/no_img.png";
    if(picture !== "" && picture != null){
        picture_path = PUBLIC_PATH + "/uploads/"+data.company_id+"/Product/"+ data.id + "/" + picture;
    }

    return "<img class='image-gird' src='"+picture_path+"' />";
}
