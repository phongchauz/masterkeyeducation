var searchTable;

$(document).ready(function () {

    var column = [{
        class: "text-center",
        name: "id",
        orderable: false,
        data: function (row, type, set, meta) {
            return searchTable.page.info().page * searchTable.page.info().length + 1 + meta.row;
        }
    },

        {
            class: "td-no-wrap text-center",
            orderable: true,
            name: "picture",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderImage(row);
            }
        },{
            class: "td-no-wrap",
            orderable: true,
            name: "fullname",
            defaultContent: "",
            data: "fullname"
        },{
            class: "text-textarea",
            orderable: true,
            name: "position",
            defaultContent: "",
            data: "position"
        },{
            class: "text-textarea",
            orderable: true,
            name: "description",
            defaultContent: "",
            data: "description"
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "is_default",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderMain(row.is_default);
            }
        },{
            class: "text-center td-no-wrap",
            orderable: true,
            name: "status",
            defaultContent: "",
            data: function (row, type, set, meta) {
                return renderStatus(row.status);
            }
        },
        {
            class: "text-center td-no-wrap",
            orderable: false,
            name: "action",
            defaultContent: "",
            data: function (row, type, set, meta) {
                var html = '';
                html += '<a class="btn btn-sm btn-round btn-primary btn-edit  tooltips mr-1" title="' + TRANSLATED_LABELS.lblEdit + '"><i class="fas fa-pencil"></i></a>';
                html += '<a class="btn btn-sm btn-round btn-danger btn-delete tooltips mr-1" title="' + TRANSLATED_LABELS.lblDelete + '"><i class="far fa-trash-alt"></i></a>';

                return html;
            }
        }
    ];

    searchTable = $("#searchTable").createDataTable({
        paging: true,
        searching: false,
        order: [
            [0, "desc"]
        ],
        ajax: {
            url: APP.ApiUrl('admin/ourTeam/search'),
            type: "post"
        },
        columns: column
    }, true);


    $(document).on("click", ".btn-add", function (e) {
        $(".continue-add").removeClass("hide");
        $("form#form-data")[0].reset();
        $("#modalEdit").find("h4").text(titleAdd);

        $('.combobox-search').selectpicker('refresh');
        $(".chk-continue").removeClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-edit", function(e){
        e.preventDefault();
        $(".continue-add").addClass("hide");
        $("#modalEdit").find("h4").text(titleEdit);
        $("input[name='status']").prop("checked", false);
        $("input[name='is_default']").prop("checked", false);

        var data = getRowDataMaster(this,searchTable);

        $("input[name='id']").val(data.id);
        $("input[name='fullname']").val(data.fullname);
        $("input[name='position']").val(data.fullname);
        $("input[name='facebook_url']").val(data.facebook_url);
        $("input[name='twitter_url']").val(data.twitter_url);
        $("input[name='linkedin_url']").val(data.linkedin_url);
        $("input[name='instagram_url']").val(data.instagram_url);
        $("textarea[name='description']").val(data.description);


        if(data.picture != "" && data.picture != null) {
            var pathFile = APP.ApiUrl("public/uploads/OurTeam/" + data.id + "/" + data.picture);
            var listPathFile = [pathFile];

            var initialPreviewConfig = [
                {"caption": data.icon, "downloadUrl": pathFile, "key": 0}
            ];

            reInitFileInput($("#picture"), listPathFile, initialPreviewConfig);
        } else {
            reInitFileInput($("#picture"));
        }


        if(data.status){
            $("input[name='status']").prop("checked", true);
        }

        if(data.is_default){
            $("input[name='is_default']").prop("checked", true);
        }

        $('.combobox-search').selectpicker('refresh');
        $("input[name='continue']").prop("checked", false);
        $(".chk-continue").addClass("hide");
        showModal('#modalEdit');
    });

    $(document).on("click", ".btn-delete", function(e){
        e.preventDefault();
        $("#header-confirm").text(TRANSLATED_LABELS.lblHeaderDelete);
        $("#content-confirm").text(TRANSLATED_LABELS.lblConfirmDelete+'?');
        $("#modal-confirm-delete").modal("show");
        var data = getRowDataMaster(this, searchTable);
        if(data) {
            $("#btn-delete-object").attr("data-id", data.id);
        }
    });

    $(document).on("click", "#btn-delete-object", function (e) {
        var id = $(this).attr("data-id");
        deleteData(id);
    });

    saveData();

});

function renderStatus(status) {
    if (status == 0) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblLock + '</span>';
    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblActive + '</span>';
}

function renderMain(is_main) {
    if (is_main == 0) {
        return '<span class="badge badge-info">' + TRANSLATED_LABELS.lblNo + '</span>';
    }

    return '<span class="badge badge-success">' + TRANSLATED_LABELS.lblYes + '</span>';
}

function saveData() {

    $("form#form-data").submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.loadingStart();
        $.ajax({
            url: APP.ApiUrl('admin/ourTeam/saveData'),
            type: 'POST',
            data: formData,
            success: function (data) {
                var response = $.parseJSON(data);
                $.loadingEnd();
                if (response.success) {
                    slideMessage(TRANSLATED_LABELS.lblSuccess, response.message, 'success');
                    // Reset form
                    $("form#form-data")[0].reset();
                    searchTable.ajax.reload();
                    if(response.is_continue == 0) {
                        hideModal('#modalEdit');
                    } else {
                        $("input[name='continue']").prop("checked", true);
                    }

                    $("input[name='code']").focus();

                }else{
                    slideMessage(TRANSLATED_LABELS.lblError, response.message, 'danger');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
}


function deleteData(id) {
    ajax({
        url: APP.ApiUrl("admin/ourTeam/delete"),
        type: "POST",
        data: {
            id: id
        },
        success: function(resp){
            var type = "success",
                title = TRANSLATED_LABELS.lblDeleteSuccess;
            if(!resp.success) {
                type = "danger";
                title = TRANSLATED_LABELS.lblDeleteError;
            }
            slideMessage(title, resp.message, type);
            hideModal("#modal-confirm-delete");
            searchTable.ajax.reload();
        }
    }, true, false);
}

function renderImage(data) {

    var picture  = data.picture;
    var picture_path = PUBLIC_PATH + "/pictures/no_img.png";
    if(picture !== "" && picture != null){
        picture_path = PUBLIC_PATH + "/uploads/OurTeam/"+ data.id + "/" + picture;
    }

    return "<img class='image-gird' src='"+picture_path+"' />";
}
