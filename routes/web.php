<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'HomeController@index');
Route::get('/login', 'AuthController@index');
Route::post('/logout',[
    'uses' => 'AuthController@logout',
    'as'   => 'logout'
]);

Route::get('login', [ 'as' => 'login', 'uses' => 'AuthController@index']);

Route::post('post-login', 'AuthController@postLogin');
Route::get('registration', 'AuthController@registration');

Route::post('post-registration', 'AuthController@postRegistration');
Route::get('logout', 'AuthController@logout');

/**
 * Home page url config
 */
Route::get('/about-us', 'HomeController@about');

Route::group(['prefix' => 'course'], function() {

    Route::get('/', 'CourseController@index');
    Route::get('/{slug}', 'CourseController@course');
    Route::post('/saveRegistration', 'CourseController@saveRegistration');
});

Route::group(['prefix' => 'course-group'], function() {

    Route::get('/', 'CourseGroupController@index');
    Route::get('/{slug}', 'CourseGroupController@course');
    Route::post('/saveRegistration', 'CourseGroupController@saveRegistration');
});

Route::get('course-registration/{slug}', 'CourseController@registration');

Route::group(['prefix' => 'teachers'], function() {

    Route::get('/', 'TeacherController@index');
});

Route::group(['prefix' => 'pricing'], function() {

    Route::get('/', 'PricingController@index');
});

Route::group(['prefix' => 'blog'], function() {

    Route::get('/', 'BlogController@index');
});

Route::group(['prefix' => 'contact'], function() {

    Route::get('/', 'ContactController@index');
});

Route::group(['prefix' => 'home'], function() {

    Route::post('/saveSubscribe', 'HomeController@saveSubscribe');
    Route::post('/saveCustomerRequest', 'HomeController@saveCustomerRequest');
});

/**
 * Admin
 */
Route::group(['prefix' => 'admin'], function() {

    /**
     * Dashboard
     */
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('index', 'Admin\DashboardController@index');

    });

    /**
     * Dashboard
     */
    Route::group(['prefix' => 'company'], function () {
        Route::get('profile', 'Admin\CompanyController@profile');

    });


    /**
     * Core Data
     */
    Route::group(['prefix' => 'coreData'], function () {
        Route::post('renderSlug', 'Admin\CoreDataController@renderSlug');

    });
    /**
     * Company
     */
    Route::group(['prefix' => 'company'], function () {
        Route::get('index', 'Admin\CompanyController@index');
        Route::get('profile/{page?}', 'Admin\CompanyController@profile');
        Route::post('saveGeneral', 'Admin\CompanyController@saveGeneral');
        Route::post('search', 'Admin\CompanyController@search');
        Route::post('saveData', 'Admin\CompanyController@saveData');
        Route::get('companyProfile', 'Admin\CompanyController@companyProfile');
        Route::post('updateMailerSetting', 'Admin\CompanyController@updateMailerSetting');
        Route::post('updateSNSSetting', 'Admin\CompanyController@updateSNSSetting');
        Route::post('updateSocial', 'Admin\CompanyController@updateSocial');
        Route::post('sendTestMail', 'Admin\CompanyController@sendTestMail');

    });

    /**
     * Company
     */
    Route::group(['prefix' => 'siteSetting'], function () {
        Route::get('index', 'Admin\SiteSettingController@index');
        Route::post('saveData', 'Admin\SiteSettingController@saveData');
    });

    /**
     * User
     */
    Route::group(['prefix' => 'user'], function() {
        Route::get('index', 'Admin\UserController@index');
        Route::get('search', 'Admin\UserController@search');
        Route::get('profile/{page?}/{id?}', 'Admin\UserController@profile');
        Route::post('saveData', 'Admin\UserController@saveData');
        Route::post('delete', 'Admin\UserController@delete');
        Route::post('changeAvatar', 'Admin\UserController@changeAvatar');
        Route::post('removeAvatar', 'Admin\UserController@removeAvatar');
        Route::post('saveGeneral', 'Admin\UserController@saveGeneral');
        Route::post('changePassword', 'Admin\UserController@changePassword');
        Route::get('import', 'Admin\UserController@import');
        Route::post('saveImportData', 'Admin\UserController@saveImportData');
        Route::post('export', 'Admin\UserController@export');
        Route::post('saveAccessMenu', 'Admin\UserController@saveAccessMenu');
    });

    Route::group(['prefix' => 'role'], function() {
        Route::get('index', 'Admin\RoleController@index');
        Route::get('search', 'Admin\RoleController@search');
        Route::post('saveData', 'Admin\RoleController@saveData');
        Route::post('delete', 'Admin\RoleController@delete');
    });

    Route::group(['prefix' => 'customer'], function() {
        Route::get('index', 'Admin\CustomerController@index');
        Route::get('search', 'Admin\CustomerController@search');
        Route::post('saveData', 'Admin\CustomerController@saveData');
        Route::post('delete', 'Admin\CustomerController@delete');
    });

    Route::group(['prefix' => 'module'], function() {
        Route::get('index', 'Admin\ModuleController@index');
        Route::get('access', 'Admin\ModuleController@access');
        Route::post('search', 'Admin\ModuleController@search');
        Route::post('saveData', 'Admin\ModuleController@saveData');
        Route::post('delete', 'Admin\ModuleController@delete');
        Route::post('writeSessionModule', 'Admin\ModuleController@writeSessionModule');
    });

    Route::group(['prefix' => 'banner'], function() {
        Route::get('index', 'Admin\BannerController@index');
        Route::get('access', 'Admin\BannerController@access');
        Route::post('search', 'Admin\BannerController@search');
        Route::post('saveData', 'Admin\BannerController@saveData');
        Route::post('delete', 'Admin\BannerController@delete');
        Route::post('changeCheckData', 'Admin\BannerController@changeCheckData');
    });

    Route::group(['prefix' => 'feature'], function() {
        Route::get('index', 'Admin\FeatureController@index');
        Route::post('search', 'Admin\FeatureController@search');
        Route::post('saveData', 'Admin\FeatureController@saveData');
        Route::post('delete', 'Admin\FeatureController@delete');
    });

    Route::group(['prefix' => 'site'], function() {
        Route::get('introduction', 'Admin\SiteController@introduction');
        Route::post('saveIntroduction', 'Admin\SiteController@saveIntroduction');
    });

    Route::group(['prefix' => 'course'], function() {
        Route::get('index', 'Admin\CourseController@index');
        Route::post('search', 'Admin\CourseController@search');
        Route::post('saveData', 'Admin\CourseController@saveData');
        Route::post('delete', 'Admin\CourseController@delete');
    });

    Route::group(['prefix' => 'appIndex'], function() {
        Route::get('index', 'Admin\AppIndexController@index');
        Route::post('search', 'Admin\AppIndexController@search');
        Route::post('saveData', 'Admin\AppIndexController@saveData');
        Route::post('delete', 'Admin\AppIndexController@delete');
    });

    Route::group(['prefix' => 'ourTeam'], function() {
        Route::get('index', 'Admin\OurTeamController@index');
        Route::post('search', 'Admin\OurTeamController@search');
        Route::post('saveData', 'Admin\OurTeamController@saveData');
        Route::post('delete', 'Admin\OurTeamController@delete');
    });

    Route::group(['prefix' => 'article'], function() {
        Route::get('index', 'Admin\ArticleController@index');
        Route::post('search', 'Admin\ArticleController@search');
        Route::post('saveData', 'Admin\ArticleController@saveData');
        Route::post('delete', 'Admin\ArticleController@delete');
    });

    Route::group(['prefix' => 'productType'], function() {
        Route::get('index', 'Admin\ProductTypeController@index');
        Route::post('search', 'Admin\ProductTypeController@search');
        Route::post('saveData', 'Admin\ProductTypeController@saveData');
        Route::post('delete', 'Admin\ProductTypeController@delete');
    });

    Route::group(['prefix' => 'gallery'], function() {
        Route::get('index', 'Admin\GalleryController@index');
        Route::post('search', 'Admin\GalleryController@search');
        Route::post('saveData', 'Admin\GalleryController@saveData');
        Route::post('delete', 'Admin\GalleryController@delete');
        Route::post('deleteAll', 'Admin\GalleryController@deleteAll');
    });

    Route::group(['prefix' => 'product'], function() {
        Route::get('index', 'Admin\ProductController@index');
        Route::post('search', 'Admin\ProductController@search');
        Route::post('saveData', 'Admin\ProductController@saveData');
        Route::post('delete', 'Admin\ProductController@delete');
        Route::post('saveImportData', 'Admin\ProductController@saveImportData');
    });

    Route::group(['prefix' => 'unit'], function() {
        Route::get('index', 'Admin\UnitController@index');
        Route::post('search', 'Admin\UnitController@search');
        Route::post('saveData', 'Admin\UnitController@saveData');
        Route::post('delete', 'Admin\UnitController@delete');
    });

    Route::group(['prefix' => 'coreValue'], function() {
        Route::get('index', 'Admin\CoreValueController@index');
        Route::post('search', 'Admin\CoreValueController@search');
        Route::post('saveData', 'Admin\CoreValueController@saveData');
        Route::post('delete', 'Admin\CoreValueController@delete');
    });

    Route::group(['prefix' => 'teacher'], function() {
        Route::get('index', 'Admin\TeacherController@index');
        Route::post('search', 'Admin\TeacherController@search');
        Route::post('saveData', 'Admin\TeacherController@saveData');
        Route::post('delete', 'Admin\TeacherController@delete');
    });

    Route::group(['prefix' => 'comment'], function() {
        Route::get('index', 'Admin\CommentController@index');
        Route::post('search', 'Admin\CommentController@search');
        Route::post('saveData', 'Admin\CommentController@saveData');
        Route::post('delete', 'Admin\CommentController@delete');
    });

    Route::group(['prefix' => 'mission'], function() {
        Route::get('index', 'Admin\MissionController@index');
        Route::post('search', 'Admin\MissionController@search');
        Route::post('saveData', 'Admin\MissionController@saveData');
        Route::post('delete', 'Admin\MissionController@delete');
    });

    Route::group(['prefix' => 'subscribe'], function() {
        Route::get('index', 'Admin\SubscribeController@index');
        Route::post('search', 'Admin\SubscribeController@search');
        Route::post('saveData', 'Admin\SubscribeController@saveData');
        Route::post('delete', 'Admin\SubscribeController@delete');
    });

    Route::group(['prefix' => 'customerRequest'], function() {
        Route::get('index', 'Admin\CustomerRequestController@index');
        Route::post('search', 'Admin\CustomerRequestController@search');
        Route::post('saveData', 'Admin\CustomerRequestController@saveData');
        Route::post('delete', 'Admin\CustomerRequestController@delete');
    });

    Route::group(['prefix' => 'classRoom'], function() {
        Route::get('index', 'Admin\ClassRoomController@index');
        Route::post('search', 'Admin\ClassRoomController@search');
        Route::post('saveData', 'Admin\ClassRoomController@saveData');
        Route::post('delete', 'Admin\ClassRoomController@delete');
    });

    Route::group(['prefix' => 'courseGroup'], function() {
        Route::get('index', 'Admin\CourseGroupController@index');
        Route::post('search', 'Admin\CourseGroupController@search');
        Route::post('saveData', 'Admin\CourseGroupController@saveData');
        Route::post('delete', 'Admin\CourseGroupController@delete');
    });

    Route::group(['prefix' => 'position'], function() {
        Route::get('index', 'Admin\PositionController@index');
        Route::post('search', 'Admin\PositionController@search');
        Route::post('saveData', 'Admin\PositionController@saveData');
        Route::post('delete', 'Admin\PositionController@delete');
        Route::post('saveAccess', 'Admin\PositionController@saveAccess');
    });

    Route::group(['prefix' => 'classRoomRegistration'], function() {
        Route::get('index', 'Admin\ClassRoomRegistrationController@index');
        Route::post('search', 'Admin\ClassRoomRegistrationController@search');
        Route::post('saveData', 'Admin\ClassRoomRegistrationController@saveData');
        Route::post('delete', 'Admin\ClassRoomRegistrationController@delete');
        Route::post('changeData', 'Admin\ClassRoomRegistrationController@changeData');
    });

});
