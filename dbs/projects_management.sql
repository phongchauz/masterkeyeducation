-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2020 at 04:02 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_lat` double DEFAULT NULL,
  `address_lng` double DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `subject`, `address`, `address_lat`, `address_lng`, `province`, `email`, `phone_number`, `website`, `favicon`, `logo`, `logo_alt`, `logo_title`, `copyright`, `description`, `title`, `keyword`, `slogan`, `hotline`, `banner`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'VTM Vietnam', '364 Cong Hoa Str, Ward 13, Tab Binh District, Ho Chi Minh City', 0, 0, '', 'info@vtm.co.jp', '0939.125.621', '', 'favicon.ico', 'project_mgt_full_logo.png', '', '', 'Bản quyền © #YYYY# VTMVN. Toàn quyền', '', 'Project Management', '', 'Project Management', '', '', '2020-02-25 16:38:43', NULL, '2020-10-14 03:45:57', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã dự án',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên dự án',
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Điện thoại',
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Thư liên hệ',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `code`, `name`, `phone`, `contact_email`, `address`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Z756', 'Nhà máy Z756', '0939125621', 'z756@gmail.com', 'Số 6, đường số 1, khu công nghiệp quốc phòng Long Bình, Đồng Nai', NULL, 1, '2020-10-12 10:05:06', NULL, '2020-10-12 10:05:19', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dev_languages`
--

CREATE TABLE `dev_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã dự án',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên dự án',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dev_languages`
--

INSERT INTO `dev_languages` (`id`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'php', 'PHP', NULL, 1, '2020-10-12 11:02:36', NULL, '2020-10-12 11:02:36', NULL, NULL, NULL),
(2, 'java', 'Java', NULL, 1, '2020-10-12 11:02:58', NULL, '2020-10-12 11:02:58', NULL, NULL, NULL),
(3, 'python', 'Python', NULL, 1, '2020-10-12 11:03:17', NULL, '2020-10-12 11:03:17', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frameworks`
--

CREATE TABLE `frameworks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã dự án',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên dự án',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frameworks`
--

INSERT INTO `frameworks` (`id`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'cakephp', 'Cake PHP', NULL, 1, '2020-10-12 11:03:31', NULL, '2020-10-12 11:03:31', NULL, NULL, NULL),
(2, 'laravel', 'Laravel', NULL, 1, '2020-10-12 11:03:57', NULL, '2020-10-12 11:03:57', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `function_menus`
--

CREATE TABLE `function_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `semi_parent_id` int(11) DEFAULT 0,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cls_icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_link` tinyint(4) DEFAULT 0 COMMENT '0: not link | 1: link',
  `is_fullscreen` tinyint(4) DEFAULT 0 COMMENT '0: default screen | 1: fullscreen',
  `is_ajax` tinyint(4) DEFAULT 0 COMMENT '0: default function | 1: ajax function',
  `open_new_tab` tinyint(4) DEFAULT 0 COMMENT '0: current screen | 1: open in new tab',
  `display` tinyint(4) DEFAULT 0 COMMENT '0: hide | 1: display in left menu',
  `ordinal` tinyint(4) DEFAULT 1 COMMENT 'Ordinal to show in left menu',
  `status` tinyint(4) DEFAULT 1 COMMENT '0: Inactive | 1: Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `function_menus`
--

INSERT INTO `function_menus` (`id`, `module_id`, `parent_id`, `semi_parent_id`, `name`, `plugin`, `controller`, `action`, `params`, `cls_icon`, `link`, `is_link`, `is_fullscreen`, `is_ajax`, `open_new_tab`, `display`, `ordinal`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 0, 0, 'Hệ Thống', NULL, NULL, NULL, NULL, 'icon-system-left-menu', NULL, 0, 0, 0, 0, 0, 2, 1, '2020-03-04 19:25:02', 1, '2020-07-06 15:49:33', 1, NULL, NULL),
(2, 1, 0, 0, 'Cài đặt', '', '', '', '', 'icon-setting-left-menu', '', 0, 0, 0, 0, 1, 1, 1, '2020-03-04 19:25:02', 1, '2020-09-09 18:37:08', 1, NULL, NULL),
(3, 1, 2, 0, 'Hồ sơ công ty', 'admin', 'company', 'companyProfile', '', 'icon-company-left-menu', 'admin/company/companyProfile', 1, 0, 0, 0, 1, 1, 1, '2020-03-04 19:27:14', 1, '2020-09-09 18:35:07', 1, NULL, NULL),
(4, 1, 2, 0, 'Danh sách người dùng', 'admin', 'user', 'index', '', 'icon-user-left-menu', 'admin/user/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(5, 1, 2, 0, 'Danh sách dự án', 'admin', 'project', 'index', '', 'icon-user-left-menu', 'admin/project/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(6, 1, 2, 0, 'Danh sách khách hàng', 'admin', 'customer', 'index', '', 'icon-user-left-menu', 'admin/customer/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(7, 1, 2, 0, 'Phân quyền dự án', 'admin', 'projectRole', 'index', '', 'icon-user-left-menu', 'admin/projectRole/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(8, 1, 2, 0, 'Danh sách mức ưu tiên', 'admin', 'projectPriority', 'index', '', 'icon-user-left-menu', 'admin/projectPriority/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(9, 1, 2, 0, 'Danh sách loại tập tin', 'admin', 'projectFileType', 'index', '', 'icon-user-left-menu', 'admin/projectFileType/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(10, 1, 2, 0, 'Truy cập dự án', 'admin', 'project', 'access', '', 'icon-user-left-menu', 'admin/project/access', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(11, 2, 0, 0, 'Dashboard', 'projectPortal', 'dashboard', 'index', '', 'icon-dashboard-left-menu', 'projectPortal/dashboard/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-13 09:08:24', 1, NULL, NULL),
(12, 2, 0, 0, 'Setting', 'projectPortal', '', '', '', 'icon-setting-left-menu', '', 0, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-13 09:08:07', 1, NULL, NULL),
(13, 2, 12, 0, 'Project Members', 'projectPortal', 'projectMember', 'index', '', 'icon-user-left-menu', 'projectProtal/projectMember/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(14, 2, 12, 0, 'Project Files', 'projectPortal', 'projectFile', 'index', '', 'icon-user-left-menu', 'projectProtal/projectFile/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(15, 2, 12, 0, 'Project Accounts', 'projectPortal', 'projectAccount', 'index', '', 'icon-profile-left-menu', 'projectProtal/projectAccount/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-13 09:09:00', 1, NULL, NULL),
(16, 2, 0, 0, 'Project MGT', 'projectPortal', '', '', '', 'icon-document-type-left-menu', '', 0, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-13 09:08:07', 1, NULL, NULL),
(17, 2, 16, 0, 'Tasks', 'projectPortal', 'task', 'index', '', 'icon-task-left-menu', 'projectProtal/task/index', 1, 1, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-14 07:59:39', 1, NULL, NULL),
(18, 2, 16, 0, 'Task Histories', 'projectPortal', 'taskHistory', 'index', '', 'icon-task-left-menu', 'projectProtal/taskHistory/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-10-14 07:59:18', 1, NULL, NULL),
(19, 2, 16, 0, 'Task Assigned', 'projectPortal', 'task', 'assigned', '', 'icon-task-assigned-left-menu', 'projectProtal/task/assigned', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_09_171802_create_companies_table', 1),
(10, '2020_09_09_174444_create_function_menus_table', 1),
(11, '2020_09_09_174517_create_role_accesses_table', 1),
(12, '2020_09_09_175440_add_fields_for_users_table', 1),
(13, '2020_09_09_184204_create_roles_table', 1),
(14, '2020_09_09_190523_add_fields_1_for_users_table', 1),
(15, '2020_10_12_163358_create_projects_table', 2),
(16, '2020_10_12_163856_create_customers_table', 3),
(17, '2020_10_12_175803_create_frameworks_table', 4),
(18, '2020_10_12_175842_create_dev_languages_table', 4),
(19, '2020_10_13_104426_create_project_file_types_table', 5),
(20, '2020_10_13_104953_create_project_roles_table', 5),
(21, '2020_10_13_105008_create_project_priorities_table', 5),
(22, '2020_10_13_142153_create_project_members_table', 6),
(23, '2020_10_13_142204_create_project_files_table', 6),
(24, '2020_10_13_142216_create_project_accounts_table', 6),
(25, '2020_10_13_142228_create_tasks_table', 6),
(26, '2020_10_13_142237_create_task_histories_table', 6),
(27, '2020_10_15_144825_chaunnp_add_fields_1_for_users', 7);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã dự án',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên dự án',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL COMMENT 'Khách hàng',
  `dev_language_id` int(11) DEFAULT NULL COMMENT 'Ngôn ngữ lập trình',
  `framework_id` int(11) DEFAULT NULL COMMENT 'Framework',
  `checkout_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL COMMENT 'Ngày hoàn thành',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `code`, `name`, `image`, `customer_id`, `dev_language_id`, `framework_id`, `checkout_url`, `description`, `start_date`, `finish_date`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'z756', 'Project Z756', NULL, 1, 1, 2, NULL, NULL, '1970-01-01', '1970-01-01', 1, '2020-10-12 10:16:37', NULL, '2020-10-13 03:39:50', NULL, NULL, NULL),
(2, 'skilltopia', 'Skilltopia', NULL, 1, 1, 1, 'svn://118.69.55.244/skilltopia', 'SKILLTOPIA', '2020-10-13', '2020-10-13', 1, '2020-10-13 03:36:27', NULL, '2020-10-14 03:51:26', NULL, NULL, NULL),
(3, 'Solar', 'Solar Market', NULL, 1, 1, 2, 'svn://118.69.55.244/solarmarket', NULL, '2020-10-14', '2020-10-14', 1, '2020-10-14 09:17:54', NULL, '2020-10-14 09:17:54', NULL, NULL, NULL),
(4, 'Chocolates', 'Chocolates', 'avatar_chocolates.jpg', 1, 0, 0, 'svn://118.69.55.244/chocolates', NULL, '2020-10-14', '2020-10-14', 1, '2020-10-14 11:31:30', NULL, '2020-10-14 11:31:30', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_accounts`
--

CREATE TABLE `project_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'Mã dự án',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên tài khoản',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mật khẩu',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_accounts`
--

INSERT INTO `project_accounts` (`id`, `project_id`, `email`, `password`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 'chaunp@gmail.com', 'vtm4test', 'Master Admin', 1, '2020-10-13 11:53:38', NULL, '2020-10-13 11:53:50', NULL, NULL, NULL),
(2, 1, 'thand@vtm.co.jp', 'vtm4test', 'Account test client', 1, '2020-10-13 11:55:01', NULL, '2020-10-13 11:55:01', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_files`
--

CREATE TABLE `project_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'Mã dự án',
  `project_file_type_id` int(11) DEFAULT NULL COMMENT 'Loại file, ID trong bảng project_file_types',
  `user_id` int(11) DEFAULT NULL COMMENT 'Uploader',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên file',
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Loại file',
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mở rộng',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_file_types`
--

CREATE TABLE `project_file_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã loại file',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên loại file',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_file_types`
--

INSERT INTO `project_file_types` (`id`, `code`, `name`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'dbrd', 'Database Relationship Diagram', NULL, 1, '2020-10-13 04:25:07', NULL, '2020-10-13 04:25:07', NULL, NULL, NULL),
(2, 'um', 'User Manual', NULL, 1, '2020-10-13 04:25:28', NULL, '2020-10-13 04:25:28', NULL, NULL, NULL),
(3, 'pd', 'Projetc Description', NULL, 1, '2020-10-13 04:26:05', NULL, '2020-10-13 04:26:05', NULL, NULL, NULL),
(4, 'tr', 'Template Report', NULL, 1, '2020-10-13 04:26:38', NULL, '2020-10-13 04:26:38', NULL, NULL, NULL),
(5, 'tc', 'Test Case', NULL, 1, '2020-10-13 04:26:51', NULL, '2020-10-13 04:26:51', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_members`
--

CREATE TABLE `project_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'Mã dự án',
  `user_id` int(11) DEFAULT NULL COMMENT 'Mã thành viên',
  `role_id` int(11) DEFAULT NULL COMMENT 'Quyền dự án',
  `join_date` date DEFAULT NULL COMMENT 'Ngày tham gia',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_members`
--

INSERT INTO `project_members` (`id`, `project_id`, `user_id`, `role_id`, `join_date`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 3, 1, '2020-10-13', 'test', 1, '2020-10-13 08:57:55', NULL, '2020-10-13 08:57:55', NULL, NULL, NULL),
(2, 1, 2, 4, '2020-10-13', NULL, 1, '2020-10-13 09:02:48', NULL, '2020-10-13 09:02:48', NULL, NULL, NULL),
(4, 3, 7, 1, '2020-10-14', NULL, 1, '2020-10-14 09:18:21', NULL, '2020-10-14 09:18:21', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_priorities`
--

CREATE TABLE `project_priorities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã ưu tiên',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên ưu tiên',
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Màu sắc',
  `background_color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Màu nền',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_priorities`
--

INSERT INTO `project_priorities` (`id`, `code`, `name`, `color`, `background_color`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Important', 'Important', '#f72020', '#ffffff', NULL, 1, '2020-10-13 06:31:48', NULL, '2020-10-13 06:33:34', NULL, NULL, NULL),
(2, 'Normal', 'Normal', '#171515', '#ffffff', NULL, 1, '2020-10-13 06:32:02', NULL, '2020-10-13 06:34:06', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_roles`
--

CREATE TABLE `project_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã quyền',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên quyền',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_roles`
--

INSERT INTO `project_roles` (`id`, `code`, `name`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'leader', 'Leader', NULL, 1, '2020-10-13 04:21:22', NULL, '2020-10-13 04:21:22', NULL, NULL, NULL),
(2, 'member', 'Member', NULL, 1, '2020-10-13 04:21:36', NULL, '2020-10-13 04:21:36', NULL, NULL, NULL),
(3, 'tester', 'Tester', NULL, 1, '2020-10-13 04:23:22', NULL, '2020-10-13 04:23:22', NULL, NULL, NULL),
(4, 'DEVF', 'Developer Fullstack', 'Dev + Document + Test', 1, '2020-10-13 08:40:37', NULL, '2020-10-13 08:40:37', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_admin` int(11) DEFAULT NULL COMMENT 'Số phân quyền',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã phân quyền',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `is_admin`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 'MA', 'Master Admin', NULL, 1, '2020-10-13 07:53:56', NULL, '2020-10-13 07:53:56', NULL, NULL, NULL),
(2, 2, 'DEV', 'Developer', NULL, 1, '2020-10-13 07:54:18', NULL, '2020-10-13 07:54:18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_accesses`
--

CREATE TABLE `role_accesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT 'Mã phân quyền',
  `is_admin` int(11) DEFAULT NULL COMMENT 'Giá trị phân quyền = field is_admin trong bảng user',
  `module_id` int(11) DEFAULT NULL COMMENT 'Id của bảng module',
  `functions_access` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Định nghĩa trong nhóm quyền này sẽ được truy cập những chức năng nào',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'Mã dự án',
  `redmine_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã của task lưu trên redmine',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên task',
  `task_master` int(11) DEFAULT NULL COMMENT 'Người giao',
  `assignee` int(11) DEFAULT NULL COMMENT 'Người được phân công',
  `priority_id` int(11) DEFAULT NULL COMMENT 'Độ ưu tiên',
  `start_date` date DEFAULT NULL COMMENT 'Ngày giao',
  `expired_date` date DEFAULT NULL COMMENT 'Ngày hết hạn',
  `finish_date` date DEFAULT NULL COMMENT 'Ngày hoàn thành',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `project_id`, `redmine_code`, `name`, `task_master`, `assignee`, `priority_id`, `start_date`, `expired_date`, `finish_date`, `note`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, '22190', 'BĐS + Solar: Cải tiến tab xem thuộc tính Customer', NULL, 2, 1, '2020-10-14', '2020-10-14', '2020-10-14', NULL, 2, '2020-10-14 07:49:43', NULL, '2020-10-14 09:02:07', NULL, NULL, NULL),
(2, 1, '22188', 'Solar + BĐS: Xóa Domain thành công nhưng có báo lỗi \"undefined\"', NULL, 2, 2, '2020-10-14', '2020-10-14', '2020-10-14', NULL, 3, '2020-10-14 09:15:31', NULL, '2020-10-14 09:15:57', NULL, NULL, NULL),
(3, 1, '344543', '34534543', NULL, 3, 1, '2020-10-15', '2020-10-15', NULL, NULL, 1, '2020-10-15 08:45:46', NULL, '2020-10-15 08:45:46', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_histories`
--

CREATE TABLE `task_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'Mã dự án',
  `redmine_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã của task lưu trên redmine',
  `task_master` int(11) DEFAULT NULL COMMENT 'Người giao',
  `assignee` int(11) DEFAULT NULL COMMENT 'Người được phân công',
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL COMMENT 'Ngày sinh',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `contact_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `self_introduction` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giới thiệu',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `functions_access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Quyền truy cập chức năng',
  `modules_access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Quyền truy cập module',
  `is_admin` int(11) DEFAULT NULL COMMENT 'Mã phân quyền',
  `current_project_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '1: Hoạt động | 0: Đã khóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `name`, `birthday`, `avatar`, `phone`, `address`, `contact_email`, `self_introduction`, `email`, `email_verified_at`, `password`, `remember_token`, `functions_access`, `modules_access`, `is_admin`, `current_project_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 'Master Admin', NULL, NULL, '', NULL, '', '', 'admin@gmail.com', NULL, '$2y$10$4zAFrcSyiM2pH/6LrZlUiOmjpcYeQDu2dGuItFAPQz2H00zw6TOnC', NULL, NULL, '', 1, 1, 0, '2020-09-08 23:02:21', '2020-10-15 03:33:55'),
(2, 'chaunp', 'Nguyễn Phong Châu', '1991-06-24', NULL, '0939125621', NULL, 'chaunp@vtm.co.jp', '', 'chaunp@vtm.co.jp', NULL, '$2y$10$4EVhsUkMQXsnZ.35BuXnO.IJd4qC/SInVKm.O/tVbDa8n5RUgVz.W', NULL, NULL, '', 2, 1, 1, '2020-10-13 07:55:03', '2020-10-15 07:50:08'),
(3, 'thamnd', 'Nguyễn Đình Thâm', NULL, NULL, '012365487989', NULL, 'thamnd@vtm.co.jp', NULL, 'thamnd@vtm.co.jp', NULL, '$2y$10$Ugslu4vbuC0s3c1RD73cOuf9ZUgr81lAgBJvxQvzny90dpSB2uf4C', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:55:36', '2020-10-13 07:55:36'),
(4, 'uyenttt', 'Tống Thị Thúy Uyển', NULL, NULL, '0123654789', NULL, 'uyenttt@vtm.co.jp', NULL, 'uyenttt@vtm.co.jp', NULL, '$2y$10$Heuz7HTpVt1WJA.VJB4IXOO4Kdn3nUrzBhZIp58e22uUnuZDCkJh2', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:56:16', '2020-10-13 07:56:16'),
(5, 'phucpd', 'Phan Đức Phúc', NULL, NULL, '0123654789', NULL, 'phucpd@vtm.co.jp', NULL, 'phucpd@vtm.co.jp', NULL, '$2y$10$Zvm8nGgP36T6Wyn9VSZmIuXr/HoQwRsBntu0Sa0m3izdelB82LTsO', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:56:52', '2020-10-13 07:56:52'),
(6, 'minhlv', 'Lưu Văn Minh', NULL, NULL, '0939125621', NULL, 'minhlv@vtm.co.jp', NULL, 'minhlv@vtm.co.jp', NULL, '$2y$10$HzRJkv8nMdR/u4qhazuXx.gJ6xuLZJERSMc35iyFIvzOjELGaHV76', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:57:17', '2020-10-13 07:57:17'),
(7, 'ngatp', 'Trần Phương Nga', NULL, NULL, '0123654789', NULL, 'ngatp@vtm.co.jp', NULL, 'ngatp@vtm.co.jp', NULL, '$2y$10$.n0ACacF319MWDzsKNskrOHaLd9OV9fgRnOiiIbyMccNsADIFTdC.', NULL, NULL, NULL, 2, 1, 1, '2020-10-13 07:57:41', '2020-10-14 09:53:38'),
(8, 'khaich', 'Chu Hoàng Khải', NULL, NULL, '0123654789', NULL, 'khaich@vtm.co.jp', NULL, 'khaich@vtm.co.jp', NULL, '$2y$10$xniNm7T.cyWxvOhbxkUGaeXkyaGytwidy6oAHFfoof8g7G87u1YY2', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:58:09', '2020-10-13 07:58:09'),
(9, 'nampt', 'Phạm Thanh Nam', NULL, NULL, '0123654789', NULL, 'nampt@vtm.co.jp', NULL, 'nampt@vtm.co.jp', NULL, '$2y$10$QZj5PPJXd4XJdTivPhRvZuEq4bI77RkvJL3RlMtSs3cW.JRJGupky', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:58:34', '2020-10-13 07:58:34'),
(10, 'hoangct', 'Cao Thanh Hoàng', NULL, NULL, '0123654789', NULL, 'hoangct@vtm.co.jp', NULL, 'hoangct@vtm.co.jp', NULL, '$2y$10$cD9h2f95GC3dZ8MvftuoluusMANptofLm0Nr8uWGcVrQWFsP37jBi', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:59:00', '2020-10-13 07:59:00'),
(11, 'doannd', 'Nguyễn Duy Đoan', NULL, NULL, '0123654789', NULL, 'doannd@vtm.co.jp', NULL, 'doannd@vtm.co.jp', NULL, '$2y$10$V2syq2/Ov9qUOsCskRUejO3xDu8Vv5PSwzVcJziRgPcWFCDuWBCdi', NULL, NULL, NULL, 2, NULL, 1, '2020-10-13 07:59:29', '2020-10-13 07:59:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dev_languages`
--
ALTER TABLE `dev_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frameworks`
--
ALTER TABLE `frameworks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `function_menus`
--
ALTER TABLE `function_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `function_menus_name_index` (`name`),
  ADD KEY `function_menus_plugin_index` (`plugin`),
  ADD KEY `function_menus_controller_index` (`controller`),
  ADD KEY `function_menus_action_index` (`action`),
  ADD KEY `function_menus_params_index` (`params`),
  ADD KEY `function_menus_cls_icon_index` (`cls_icon`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_accounts`
--
ALTER TABLE `project_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_files`
--
ALTER TABLE `project_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_file_types`
--
ALTER TABLE `project_file_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_members`
--
ALTER TABLE `project_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_priorities`
--
ALTER TABLE `project_priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_roles`
--
ALTER TABLE `project_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_accesses`
--
ALTER TABLE `role_accesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_project_id_index` (`project_id`),
  ADD KEY `tasks_redmine_code_index` (`redmine_code`),
  ADD KEY `tasks_name_index` (`name`),
  ADD KEY `tasks_task_master_index` (`task_master`),
  ADD KEY `tasks_assignee_index` (`assignee`);

--
-- Indexes for table `task_histories`
--
ALTER TABLE `task_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_histories_project_id_index` (`project_id`),
  ADD KEY `task_histories_redmine_code_index` (`redmine_code`),
  ADD KEY `task_histories_task_master_index` (`task_master`),
  ADD KEY `task_histories_assignee_index` (`assignee`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_is_admin_index` (`is_admin`),
  ADD KEY `users_code_index` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dev_languages`
--
ALTER TABLE `dev_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frameworks`
--
ALTER TABLE `frameworks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `function_menus`
--
ALTER TABLE `function_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_accounts`
--
ALTER TABLE `project_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_files`
--
ALTER TABLE `project_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_file_types`
--
ALTER TABLE `project_file_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `project_members`
--
ALTER TABLE `project_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_priorities`
--
ALTER TABLE `project_priorities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_roles`
--
ALTER TABLE `project_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_accesses`
--
ALTER TABLE `role_accesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `task_histories`
--
ALTER TABLE `task_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
