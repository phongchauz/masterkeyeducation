-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2020 at 06:49 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timetable`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_block_id` int(11) DEFAULT NULL COMMENT 'Mã khối môn học',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên lớp',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `classrooms`
--

CREATE TABLE `classrooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phòng',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classrooms`
--

INSERT INTO `classrooms` (`id`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, '301', '301', NULL, 1, '2020-09-12 16:26:55', NULL, '2020-09-12 16:26:55', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_hours`
--

CREATE TABLE `class_hours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` int(11) DEFAULT NULL COMMENT 'id of table class_hour_types',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã phân quyền',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `from_hour` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giờ bắt đầu',
  `to_hour` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giờ kết thúc',
  `from_hour_912` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giờ bắt đầu',
  `to_hour_912` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giờ kết thúc',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_hours`
--

INSERT INTO `class_hours` (`id`, `type_id`, `code`, `name`, `from_hour`, `to_hour`, `from_hour_912`, `to_hour_912`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 'TDG', '20p đầu giờ', '07:15', '07:35', '07:15', '07:35', NULL, 1, '2020-09-12 12:31:47', NULL, '2020-09-12 12:31:47', NULL, NULL, NULL),
(2, 1, 'T1S', 'Tiết 1', '07:40', '08:25', '07:40', '08:25', NULL, 1, '2020-09-12 12:31:47', NULL, '2020-09-12 12:31:47', NULL, NULL, NULL),
(3, 1, 'T2S', 'Tiết 2', '08:30', '09:15', '08:30', '09:15', NULL, 1, '2020-09-12 12:31:47', NULL, '2020-09-12 12:31:47', NULL, NULL, NULL),
(4, 2, 'TGLS', 'Nghĩ giữa tiết', '09:15', '09:30', '09:15', '09:30', NULL, 1, '2020-09-12 12:31:47', NULL, '2020-09-12 12:31:47', NULL, NULL, NULL),
(5, 1, 'T3S', 'Tiết 3', '09:35', '10:20', '09:35', '10:20', NULL, 1, '2020-09-12 12:31:47', NULL, '2020-09-12 12:31:47', NULL, NULL, NULL),
(6, 1, 'T4S', 'Tiết 4', '10:25', '11:10', '10:25', '11:10', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(7, 1, 'T5S', 'Tiết 5', '11:15', '11:55', '11:15', '11:55', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(8, 0, 'TT', 'Giữa buổi', '12:10', '13:40', '12:10', '13:40', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:44:41', NULL, NULL, NULL),
(9, 1, 'T1C', 'Tiết 1', '13:30', '14:15', '13:30', '14:15', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(10, 1, 'T2C', 'Tiết 2', '14:20', '15:00', '14:20', '15:00', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(11, 2, 'TGLC', 'Nghĩ giữa tiết', '15:05', '15:20', '15:05', '15:20', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(12, 1, 'T4C', 'Tiết 3', '15:25', '15:50', '15:25', '15:50', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL),
(13, 1, 'T5C', 'Tiết 4', '16:15', '16:30', '16:15', '16:45', NULL, 1, '2020-09-12 12:31:48', NULL, '2020-09-12 12:31:48', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_hour_types`
--

CREATE TABLE `class_hour_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_hour_types`
--

INSERT INTO `class_hour_types` (`id`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Tiết học chính thức', NULL, 1, '2020-09-12 02:04:00', NULL, '2020-09-12 02:04:00', NULL, NULL, NULL),
(2, 'Giải lao', NULL, 1, '2020-09-12 02:04:09', NULL, '2020-09-12 02:04:09', NULL, NULL, NULL),
(3, 'Nghỉ giữa tiết', 'test', 1, '2020-09-12 02:04:29', NULL, '2020-09-12 02:04:48', NULL, NULL, NULL),
(4, 'Đầu giờ', NULL, 1, '2020-09-12 09:28:58', NULL, '2020-09-12 09:28:58', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_lat` double DEFAULT NULL,
  `address_lng` double DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `subject`, `address`, `address_lat`, `address_lng`, `province`, `email`, `phone_number`, `website`, `favicon`, `logo`, `logo_alt`, `logo_title`, `copyright`, `description`, `title`, `keyword`, `slogan`, `hotline`, `banner`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Oganic Green Code', '24/5/4 Truong Phuoc Phan, Binh Tri Dong, Binh Tan, HCM', 0, 0, '', 'info@nongsantugoc.com', '0939.125.621', '', 'favicon.ico', 'favicon.ico', '', '', 'Bản quyền © #YYYY# NSTG. Toàn quyền', '', 'NSTG', '', 'NSTG', '', '', '2020-02-25 16:38:43', NULL, '2020-09-09 18:24:57', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `education_years`
--

CREATE TABLE `education_years` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã phân quyền',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `from_date` date DEFAULT NULL COMMENT 'Ngày bắt đầu',
  `to_date` date DEFAULT NULL COMMENT 'Ngày kết thúc',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `is_current` tinyint(4) DEFAULT '0' COMMENT 'Năm đang đào tạo, 0: No | 1: Yes',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education_years`
--

INSERT INTO `education_years` (`id`, `code`, `name`, `from_date`, `to_date`, `description`, `is_current`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'EY2020', '2020', '2020-09-12', '2021-09-12', 'Năm học 2020 2021', 0, 1, '2020-09-12 00:29:12', NULL, '2020-09-12 01:48:51', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `function_menus`
--

CREATE TABLE `function_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `semi_parent_id` int(11) DEFAULT '0',
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cls_icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_link` tinyint(4) DEFAULT '0' COMMENT '0: not link | 1: link',
  `is_fullscreen` tinyint(4) DEFAULT '0' COMMENT '0: default screen | 1: fullscreen',
  `is_ajax` tinyint(4) DEFAULT '0' COMMENT '0: default function | 1: ajax function',
  `open_new_tab` tinyint(4) DEFAULT '0' COMMENT '0: current screen | 1: open in new tab',
  `display` tinyint(4) DEFAULT '0' COMMENT '0: hide | 1: display in left menu',
  `ordinal` tinyint(4) DEFAULT '1' COMMENT 'Ordinal to show in left menu',
  `status` tinyint(4) DEFAULT '1' COMMENT '0: Inactive | 1: Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `function_menus`
--

INSERT INTO `function_menus` (`id`, `module_id`, `parent_id`, `semi_parent_id`, `name`, `plugin`, `controller`, `action`, `params`, `cls_icon`, `link`, `is_link`, `is_fullscreen`, `is_ajax`, `open_new_tab`, `display`, `ordinal`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 0, 0, 'Hệ Thống', NULL, NULL, NULL, NULL, 'icon-system-left-menu', NULL, 0, 0, 0, 0, 0, 2, 1, '2020-03-04 19:25:02', 1, '2020-07-06 15:49:33', 1, NULL, NULL),
(2, 1, 0, 0, 'Settings', '', '', '', '', 'icon-setting-left-menu', '', 0, 0, 0, 0, 1, 1, 1, '2020-03-04 19:25:02', 1, '2020-09-12 15:17:44', 1, NULL, NULL),
(3, 1, 2, 0, 'Company Profile', 'admin', 'company', 'companyProfile', '', 'icon-company-left-menu', 'admin/company/companyProfile', 1, 0, 0, 0, 1, 1, 1, '2020-03-04 19:27:14', 1, '2020-09-12 15:17:53', 1, NULL, NULL),
(4, 1, 2, 0, 'Users', 'admin', 'user', 'index', '', 'icon-user-left-menu', 'admin/user/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-12 15:18:00', 1, NULL, NULL),
(5, 1, 2, 0, 'Education Year', 'admin', 'educationYear', 'index', '', 'icon-user-left-menu', 'admin/educationYear/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(6, 1, 2, 0, 'Class Hour type', 'admin', 'classHourType', 'index', '', 'icon-user-left-menu', 'admin/classHourType/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(7, 1, 2, 0, 'Class Hour', 'admin', 'classHour', 'index', '', 'icon-user-left-menu', 'admin/classHour/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(8, 1, 2, 0, 'Grades', 'admin', 'grade', 'index', '', 'icon-user-left-menu', 'admin/grade/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-12 15:02:09', 1, NULL, NULL),
(9, 1, 2, 0, 'Subjects', 'admin', 'subject', 'index', '', 'icon-user-left-menu', 'admin/subject/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(10, 1, 2, 0, 'Subject Blocks', 'admin', 'subjectBlock', 'index', '', 'icon-user-left-menu', 'admin/subjectBlock/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(11, 1, 2, 0, 'Classes', 'admin', 'classes', 'index', '', 'icon-user-left-menu', 'admin/classes/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(12, 1, 2, 0, 'Subject Departments', 'admin', 'subjectDepartment', 'index', '', 'icon-user-left-menu', 'admin/subjectDepartment/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-09 18:35:15', 1, NULL, NULL),
(13, 1, 2, 0, 'Classrooms', 'admin', 'classroom', 'index', '', 'icon-user-left-menu', 'admin/classroom/index', 1, 0, 0, 0, 1, 1, 1, '2020-03-13 16:24:06', 1, '2020-09-12 16:26:45', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `amount` int(11) DEFAULT NULL COMMENT 'Giá trị',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `amount`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Khối 6', 6, 'test', 1, '2020-09-12 15:08:56', NULL, '2020-09-12 15:09:04', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_09_075238_chaunnp_add_fields_1_for_warehouse_commodities', 2),
(10, '2020_09_09_171802_create_companies_table', 2),
(13, '2020_09_09_174444_create_function_menus_table', 3),
(14, '2020_09_09_174517_create_role_accesses_table', 3),
(16, '2020_09_09_175440_add_fields_for_users_table', 4),
(18, '2020_09_09_184204_create_roles_table', 5),
(19, '2020_09_09_185809_create_modules_table', 6),
(24, '2020_09_09_190523_add_fields_1_for_users_table', 7),
(26, '2020_09_12_045224_create_education_years_table', 8),
(28, '2020_09_12_083303_create_class_hour_types_table', 9),
(29, '2020_09_12_082913_create_class_hours_table', 10),
(30, '2020_09_12_214835_create_subjects_table', 11),
(32, '2020_09_12_214956_create_grades_table', 12),
(33, '2020_09_12_223937_create_class_rooms_table', 13),
(34, '2020_09_12_224849_create_subject_departments_table', 13),
(35, '2020_09_12_225830_create_classes_table', 14),
(36, '2020_09_12_230316_create_subject_blocks_table', 15),
(37, '2020_09_12_232315_create_classrooms_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0: Locked | 1: Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'Admin', NULL, '1', '2020-09-09 19:02:47', NULL, '2020-09-09 19:02:47', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_admin` int(11) DEFAULT NULL COMMENT 'Số phân quyền',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã phân quyền',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phân quyền',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `is_admin`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 1, 'adm', 'Master Admin', NULL, 1, '2020-09-09 18:45:33', NULL, '2020-09-09 18:45:33', NULL, NULL, NULL),
(2, 2, 'cus', 'Customer Admin', NULL, 1, '2020-09-09 18:45:33', NULL, '2020-09-09 18:45:33', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_accesses`
--

CREATE TABLE `role_accesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT 'Mã phân quyền',
  `is_admin` int(11) DEFAULT NULL COMMENT 'Giá trị phân quyền = field is_admin trong bảng user',
  `module_id` int(11) DEFAULT NULL COMMENT 'Id của bảng module',
  `functions_access` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Định nghĩa trong nhóm quyền này sẽ được truy cập những chức năng nào',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã môn học',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên môn học',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Diễn giải',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `code`, `name`, `name_en`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'TOAN', 'Toán', 'Maths', NULL, 1, '2020-09-12 15:18:49', NULL, '2020-09-12 15:32:07', NULL, NULL, NULL),
(2, 'VAN', 'Văn', 'Literature', NULL, 1, '2020-09-12 15:19:25', NULL, '2020-09-12 15:31:26', NULL, NULL, NULL),
(3, 'LY', 'Vật lý', 'Physic', NULL, 1, '2020-09-12 15:20:47', NULL, '2020-09-12 15:32:47', NULL, NULL, NULL),
(4, 'HOA', 'Hóa học', 'Chemistry', NULL, 1, '2020-09-12 15:21:06', NULL, '2020-09-12 15:33:04', NULL, NULL, NULL),
(5, 'SINH', 'Sinh học', 'Biology', NULL, 1, '2020-09-12 15:21:12', NULL, '2020-09-12 15:33:28', NULL, NULL, NULL),
(6, 'SU', 'Lịch sử', 'History', NULL, 1, '2020-09-12 15:21:24', NULL, '2020-09-12 15:36:53', NULL, NULL, NULL),
(7, 'DIA', 'Địa lý', 'Geography', NULL, 1, '2020-09-12 15:21:34', NULL, '2020-09-12 15:37:04', NULL, NULL, NULL),
(8, 'ANHVAN', 'Anh văn', 'English', NULL, 1, '2020-09-12 15:21:40', NULL, '2020-09-12 15:37:22', NULL, NULL, NULL),
(9, 'THEDUC', 'Thể dục', 'PE', NULL, 1, '2020-09-12 15:21:51', NULL, '2020-09-12 15:34:12', NULL, NULL, NULL),
(10, 'NGHE', 'Nghề', 'Trade', NULL, 1, '2020-09-12 15:23:07', NULL, '2020-09-12 15:36:23', NULL, NULL, NULL),
(11, 'AVQT', 'Anh văn quốc tế', 'International English', NULL, 1, '2020-09-12 15:23:14', NULL, '2020-09-12 15:30:50', NULL, NULL, NULL),
(12, 'TIN', 'Tin học', 'IT', NULL, 1, '2020-09-12 15:23:29', NULL, '2020-09-12 15:33:52', NULL, NULL, NULL),
(13, 'GDCD', 'Giáo dục công dân', 'Citizenship', NULL, 1, '2020-09-12 15:23:45', NULL, '2020-09-12 15:34:35', NULL, NULL, NULL),
(14, 'GDQP', 'GIÁO DỤC QUỐC PHÒNG', NULL, NULL, 1, '2020-09-12 15:24:13', NULL, '2020-09-12 15:24:37', NULL, NULL, NULL),
(15, 'MT', 'MT', NULL, NULL, 1, '2020-09-12 15:25:15', NULL, '2020-09-12 15:25:15', NULL, NULL, NULL),
(16, 'AMNHAC', 'Âm nhạc', 'Music', NULL, 1, '2020-09-12 15:34:57', NULL, '2020-09-12 15:34:57', NULL, NULL, NULL),
(17, 'MYTHUAT', 'Mỹ thuật', 'Art', NULL, 1, '2020-09-12 15:35:28', NULL, '2020-09-12 15:35:28', NULL, NULL, NULL),
(18, 'SHCN', 'SHCN', 'SHCN', NULL, 1, '2020-09-12 15:35:39', NULL, '2020-09-12 15:35:39', NULL, NULL, NULL),
(19, 'HĐTT', 'HĐTT', 'Class activity', NULL, 1, '2020-09-12 15:36:05', NULL, '2020-09-12 15:36:05', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_blocks`
--

CREATE TABLE `subject_blocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên bộ môn',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_blocks`
--

INSERT INTO `subject_blocks` (`id`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'TN', 'Tự nhiên', NULL, 1, '2020-09-12 16:15:22', NULL, '2020-09-12 16:15:22', NULL, NULL, NULL),
(2, 'XH', 'Xã hội', NULL, 1, '2020-09-12 16:15:29', NULL, '2020-09-12 16:15:29', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_departments`
--

CREATE TABLE `subject_departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên bộ môn',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Trạng thái',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_departments`
--

INSERT INTO `subject_departments` (`id`, `code`, `name`, `description`, `status`, `created_at`, `created_user`, `updated_at`, `updated_user`, `deleted_at`, `deleted_user`) VALUES
(1, 'TOAN', 'Toán', NULL, 1, '2020-09-12 16:20:30', NULL, '2020-09-12 16:20:30', NULL, NULL, NULL),
(2, 'ANH', 'Anh', NULL, 1, '2020-09-12 16:20:43', NULL, '2020-09-12 16:20:43', NULL, NULL, NULL),
(3, 'XAHOI', 'Xã hội', NULL, 1, '2020-09-12 16:20:52', NULL, '2020-09-12 16:20:52', NULL, NULL, NULL),
(4, 'TUNHIEN', 'Tự nhiên', NULL, 1, '2020-09-12 16:20:59', NULL, '2020-09-12 16:20:59', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `contact_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ảnh đại diện',
  `self_introduction` text COLLATE utf8mb4_unicode_ci COMMENT 'Giới thiệu',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `functions_access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Quyền truy cập chức năng',
  `modules_access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Quyền truy cập module',
  `is_admin` int(11) DEFAULT NULL COMMENT 'Mã phân quyền',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1: Hoạt động | 0: Đã khóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `name`, `avatar`, `phone`, `address`, `contact_email`, `self_introduction`, `email`, `email_verified_at`, `password`, `remember_token`, `functions_access`, `modules_access`, `is_admin`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 'Nguyen Phong Chau', NULL, '', NULL, '', '', 'chaunp1991@gmail.com', NULL, '$2y$10$4zAFrcSyiM2pH/6LrZlUiOmjpcYeQDu2dGuItFAPQz2H00zw6TOnC', NULL, NULL, '', 1, 0, '2020-09-08 23:02:21', '2020-09-09 12:19:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classrooms`
--
ALTER TABLE `classrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_hours`
--
ALTER TABLE `class_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_hour_types`
--
ALTER TABLE `class_hour_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education_years`
--
ALTER TABLE `education_years`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `function_menus`
--
ALTER TABLE `function_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `function_menus_name_index` (`name`),
  ADD KEY `function_menus_plugin_index` (`plugin`),
  ADD KEY `function_menus_controller_index` (`controller`),
  ADD KEY `function_menus_action_index` (`action`),
  ADD KEY `function_menus_params_index` (`params`),
  ADD KEY `function_menus_cls_icon_index` (`cls_icon`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_accesses`
--
ALTER TABLE `role_accesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_blocks`
--
ALTER TABLE `subject_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_departments`
--
ALTER TABLE `subject_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_is_admin_index` (`is_admin`),
  ADD KEY `users_code_index` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classrooms`
--
ALTER TABLE `classrooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `class_hours`
--
ALTER TABLE `class_hours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `class_hour_types`
--
ALTER TABLE `class_hour_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `education_years`
--
ALTER TABLE `education_years`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `function_menus`
--
ALTER TABLE `function_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_accesses`
--
ALTER TABLE `role_accesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `subject_blocks`
--
ALTER TABLE `subject_blocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subject_departments`
--
ALTER TABLE `subject_departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
