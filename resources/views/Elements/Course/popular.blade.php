<div class="sidebar-box">
    <form action="#" class="search-form">
        <div class="form-group">
            <span class="icon icon-search"></span>
            <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
        </div>
    </form>
</div>


<div class="sidebar-box ftco-animate">
    <h3>{{ __("Popular Courses") }}</h3>

    @foreach($listPopularCourse as $key => $item)

        <?php

        $course_name    = $item->name ?? "";
        $slug        = $item->slug ?? "";
        $summary        = $item->summary ?? "";
        $picture        = $item->picture ?? "";
        $picture_path   = empty($picture) ? $current_domain.'/images/no_img.jpg' :
            $current_domain.'/uploads/'.$item->company_id.'/Course/'.$item->id."/Picture/".$picture.'?t='.SYSTEM_CACHE;

        $cover        = $course->cover ?? "";
        $cover_path   = empty($cover) ? $current_domain.'/images/no_img.jpg' :
            $current_domain.'/uploads/'.$item->company_id.'/Course/'.$item->id."/Cover/".$cover.'?t='.SYSTEM_CACHE;

        ?>

        <div class="block-21 mb-4 d-flex">
            <a href="{{ url('course/'.$slug) }}" class="blog-img mr-4 border-smooth" style="background-image: url({{ $picture_path }});"></a>
            <div class="text">
                <h3 class="heading"><a href="{{ url('course/'.$slug) }}">{{ $course_name }}</a></h3>
                <p style="text-align: justify; ">{{ $summary }}</p>
                <div class="meta">
                    <div><a href="{{ url('teacher/') }}"><span class="icon-person"></span> {{ __("Teacher") }}</a></div>
                </div>
            </div>
        </div>
    @endforeach


</div>

<div class="sidebar-box ftco-animate">
    <h3>Tag Cloud</h3>
    <ul class="tagcloud m-0 p-0">
        <a href="#" class="tag-cloud-link">School</a>
        <a href="#" class="tag-cloud-link">Kids</a>
        <a href="#" class="tag-cloud-link">Nursery</a>
        <a href="#" class="tag-cloud-link">Daycare</a>
        <a href="#" class="tag-cloud-link">Care</a>
        <a href="#" class="tag-cloud-link">Kindergarten</a>
        <a href="#" class="tag-cloud-link">Teacher</a>
    </ul>
</div>

<div class="sidebar-box ftco-animate">
    <h3>Archives</h3>
    <ul class="categories">
        <li><a href="#">December 2018 <span>(30)</span></a></li>
        <li><a href="#">Novemmber 2018 <span>(20)</span></a></li>
        <li><a href="#">September 2018 <span>(6)</span></a></li>
        <li><a href="#">August 2018 <span>(8)</span></a></li>
    </ul>
</div>


<div class="sidebar-box ftco-animate">
    <h3>Paragraph</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
</div>