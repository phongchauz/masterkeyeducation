<div class="modal fade" id="modalRegistration" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">{{ __("Registration Course") }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-registration" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="classroom_id" hidden>

                            <div class="form-group">
                                <label class="required_label">{{ __("Fullname") }}</label>
                                <input type="text" class="form-control" name="fullname" placeholder="{{ __("Enter fullname") }}">
                            </div>

                            <div class="form-group">
                                <label class="required_label">{{ __("Phone") }}</label>
                                <input type="text" class="form-control" name="phone" placeholder="{{ __("Enter phone") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Email") }}</label>
                                <input type="text" class="form-control" name="contact_email" placeholder="{{ __("Enter email") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Message") }}</label>
                                <textarea type="text" class="form-control" name="content" placeholder="{{ __("Enter content") }}"></textarea>
                            </div>


                            <div class="card-footer text-center">
                                <button type="submit"
                                        class="btn btn-primary font-weight-bold btn-act-lg "
                                >
                                    <i class="fas fa-save"></i> {{ __("Save") }}
                                </button>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<script>

</script>