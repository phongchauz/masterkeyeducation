

<section class="slider-area">
    <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">

        @foreach($listBanner as $banner)
        <?php
            $id = $banner->id;
            $company_id = $banner->company_id;
            $title      = $banner->title;
            $summary    = $banner->summary;
            $slug       = $banner->slug;
            $picture    = $banner->picture;

            $arrTitle   = explode(' ', $title);

            $count      = count($arrTitle);

            $last_title = $count ? $arrTitle[$count - 1] : '';
            $arrTitleAfter   = $count ? array_splice($arrTitle, 0, $count - 1) : [];
            $pre_title  = implode(' ', $arrTitleAfter);

            $picture_path = empty($picture) ? "" : $current_domain."/public/uploads/".$company_id."/Article/".$id."/Picture/".$picture."?t=1";


        ?>
        <!-- single slider item start -->
        <div class="hero-single-slide hero-overlay">
            <div class="hero-slider-item bg-img" data-bg="{{ $picture_path }}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hero-slider-content slide-1">
                                <h2 class="slide-title">{{ $pre_title }} <span>{{ $last_title }}</span></h2>
                                <h4 class="slide-desc">{{ $summary }}</h4>
                                <a href="{{ url('product/'.$slug) }}" class="btn btn-hero">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- single slider item start -->
        @endforeach

    </div>
</section>