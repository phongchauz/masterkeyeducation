<?php
    $introduction_title = isset($site['introduction_title']) ? $site['introduction_title'] : '';
    $introduction_subtitle = isset($site['introduction_subtitle']) ? $site['introduction_subtitle'] : '';
    $introduction_picture = isset($site['introduction_picture']) ? $site['introduction_picture'] : '';
    $introduction_status = isset($site['introduction_status']) ? $site['introduction_status'] : '';

    $picture_path = empty($introduction_picture) ? url('public/images/introduction_noimg.png') :
    url('public/uploads/Site/1/Introduction/'.$introduction_picture);
?>
<div class="container">
    <div class="vc_row wpb_row vc_row-fluid">
        <style>.subtitle.klb_035392:after {
                color: #035392
            }</style>
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="section-heading text-center"><h2 style="color:#151515">About Us</h2>
                        <p class="subtitle klb_035392" style="color:#949393; border-color:#035392">GET TO KNOW
                            US</p></div>
                </div>
            </div>
        </div>
        <style>.kulebe.btn.klb_035392:hover {
                background-color: #035392 !important;
            }</style>
        <div class="wpb_column vc_column_container vc_col-sm-7">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element">
                        <div class="wpb_wrapper"><h3>{{ $introduction_title }}</h3>
                            <p>{{ $introduction_subtitle }}</p>
                        </div>
                    </div>
                    <div class="klb-button vc_custom_1565418497379" style="display:inline-block;"><a
                                class="kulebe btn btn-secondary klb_035392" style="background-color:#E8373D;"
                                href="javascript:;" target="" title="Contact Us">Contact Us</a></div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-5">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="res-margin">
                        <img class="img-fluid rounded" src="{{ $picture_path }}" alt="img-fluid">
                        <div class="ornament-rainbow" data-aos="zoom-out"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>