<div class="container">
    <div class="vc_row wpb_row vc_row-fluid">

        <?php
            foreach ($listBannerCourse as $key => $bannerCourse){
                $id = $bannerCourse->id;
                $picture = $bannerCourse->picture;
                $picture_path = empty($picture) ? url('public/images/subbanner_noimg.png') :
                    url('public/uploads/Course/'.$id.'/'.$picture);
                $bg_color = "#035392";
                if($key == 1){$bg_color = "#e8373d";}
                if($key == 2){$bg_color = "#e4a40f";}

                ?>
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <section id="intro-cards">
                                    <div class="card card-flip">
                                        <div class="card bg-secondary text-light" style="background-color:{{ $bg_color }} !important;">
                                            <div class="p-5"><h5 class="card-title text-light">{{ $bannerCourse->title }}</h5>
                                                <p class="card-text">{{ $bannerCourse->subtitle }}</p>
                                                <a href="javascript:;" title="Contact Us" target="" class="btn d-lg-none">Contact Us</a>
                                            </div>
                                            <img class="card-img" src="{{ $picture_path }}" alt="flip"></div>
                                        <div class="card bg-secondary text-light card-back"
                                             style="background-color:{{ $bg_color }} !important;">
                                            <div class="card-body d-flex justify-content-center align-items-center">
                                                <div class="p-4">
                                                    <h5 class="card-title text-light">{{ $bannerCourse->title }}</h5>
                                                    <p class="card-text">{{ $bannerCourse->subtitle }}</p>
                                                    <a href="javascript:;"  title="Contact Us" target="" class="btn">Contact Us</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                <?php

            }
        ?>

    </div>
</div>