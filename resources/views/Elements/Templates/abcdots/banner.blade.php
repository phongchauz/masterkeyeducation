<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
     class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="container-fluid p-0">
                    <div id="slider" class="slider-full">

                        <?php
                            foreach ($listBanner as $banner){
                                $id = $banner->id;
                                $picture = $banner->picture;
                                $picture_path = empty($picture) ? url('public/images/banner_noimg.png') :
                                    url('public/uploads/Banner/'.$id.'/'.$picture);
                                ?>

                            <div class="ls-slide" data-ls="duration:4000; transition2d:7;">
                                <img src="{{ $picture_path }}"
                                     class="ls-bg" alt="{{ $banner->slogan }}"/>
                                <div class="ls-l header-wrapper"
                                     data-ls="offsetyin:150; durationin:700; delayin:200; easingin:easeOutQuint; rotatexin:20; scalexin:1.4; durationout:400;">
                                    <div class="header-text text-light"><span class="text-light">{{ $banner->slogan }}</span>
                                        <h1>{{ $banner->title }}</h1>
                                        <div class="hidden-small">
                                            <p class="header-p">{{ $banner->subtitle }}</p>
                                            <a class="btn btn-quaternary" href="javascript:;" title="Contact Us" target="">Contact Us</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <?php
                            }
                        ?>






                    </div>
                    <svg version="1.1" id="divider" class="slider-divider" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 1440 126" preserveAspectRatio="none slice" xml:space="preserve">
                        <path class="st0" d="M685.6,38.8C418.7-11.1,170.2,9.9,0,30v96h1440V30C1252.7,52.2,1010,99.4,685.6,38.8z"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row-full-width vc_clearfix"></div>