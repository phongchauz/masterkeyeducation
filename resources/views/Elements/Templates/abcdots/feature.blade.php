
<?php
    $mainFeature = (object)[];
    foreach ($listFeature as $fKey => $feature){
        if($feature->is_main){
            $mainFeature = $feature;
            break;
        }
    }

    $main_picture = $mainFeature->picture;
$main_picture_path = empty($main_picture) ? url('public/images/feature_noimg.png') :
    url('public/uploads/Feature/'.$mainFeature->id.'/'.$main_picture);

?>

<div class="container">
    <div data-vc-full-width="true" data-vc-full-width-init="false"
         class="vc_row wpb_row vc_row-fluid vc_custom_1565677219711 vc_row-has-fill">
        <style>
            .subtitle.klb_e4a40f:after {
                color: #e4a40f
            }
        </style>
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="section-heading text-center"><h2 style="color:#ffffff">{{ $mainFeature->title }}</h2>
                        <p class="subtitle klb_e4a40f" style="color:#ffffff; border-color:#e4a40f">BENEFITS FOR YOU</p></div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">

                        <?php
                            foreach ($listFeature as $feature){
                                if($feature->is_main){continue;}

                                $id = $feature->id;
                                $picture = $feature->picture;
                                $picture_path = empty($picture) ? url('public/images/feature_ico_noimg.png') :
                                    url('public/uploads/Feature/'.$id.'/'.$picture);

                                ?>
                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="feature-with-icon klbicon">
                                            <div class="icon-features">
                                                <img src="{{ $picture_path }}" width="100px" height="94px">
                                            </div>
                                            <h5>{{ $feature->title }}</h5>
                                            <p>{{ $feature->subtitle }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <?php
                            }
                        ?>



                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
            <div class="vc_column-inner vc_custom_1566196775283">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_left">
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                <img width="700" height="729"  src="{{ $main_picture_path }}" class="vc_single_image-img attachment-full"
                                                                                         alt="" loading="lazy"
                                                                                         srcset="{{ $main_picture_path }} 700w, {{ $main_picture_path }} 288w"
                                                                                         sizes="(max-width: 700px) 100vw, 700px"/>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>
</div>