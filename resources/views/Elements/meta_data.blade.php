<?php

$title          = isset($title) && !empty($title) ?$title : $company['name'];
$id             = $company['id'] ?? "-1";
$subject        = $company['subject'] ?? "";
$keyword        = $company['keyword'] ?? "";
$description    = $company['description'] ?? "";
$copyright      = $company['copyright'] ?? "";
$copyright      = str_replace("#YYYY#", date("Y"), $copyright);
$favicon        = $company['favicon'] ?? '';
$favicon_path      = empty($favicon) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$id.'/Favicon/'.$favicon.'?t='.SYSTEM_CACHE;
?>
<title>{{ $title }}</title>
<meta http-equiv="Content-Type" content="text/html; CHARSET=UTF-8">
<!--[if IE 8]><meta http-equiv="X-UA-Compatible" content="IE=8,chrome=1"><![endif]-->
<!--[if IE 9]><meta http-equiv="X-UA-Compatible" content="IE=9,chrome=1"><![endif]-->
<!--[if IE 10]><meta http-equiv="X-UA-Compatible" content="IE=10,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{$description}}">
<meta name="author" content="{{$subject}}">
<meta name="copyright" content="{{$copyright}}">
<meta name="keyword" content="{{$keyword}}">
<meta name="robots" content="noindex,nofollow">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" type="image/png" href="{{$favicon_path}}"/>
