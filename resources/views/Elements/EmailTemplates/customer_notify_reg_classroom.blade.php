
<?php

    $summary    = $email_data["summary"] ?? "";
    $fullname   = $email_data["fullname"] ?? "";
    $signature  = $email_data["signature"] ?? "";

?>

<h4>{{ __("Dear")." ".$fullname }}!</h4>

<p><?php echo $summary;?></p>



<p/>
<p>{{ __("We will check and contact you as soon as possible")."." }}</p>

<p style="margin-top: 30px;"><?php echo __("Best regards,")."<br/>".$signature?></p>

