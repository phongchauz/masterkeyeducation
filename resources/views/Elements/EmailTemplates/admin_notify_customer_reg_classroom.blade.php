
<?php

    $summary    = $email_data["summary"] ?? "";
    $message    = $email_data["message"] ?? "";
    $fullname   = $email_data["fullname"] ?? "";
    $phone      = $email_data["phone"] ?? "";
    $created_at = $email_data["created_at"] ?? "";
    $created_at = $created_at->toArray();
    $created_at = $created_at['formatted'] ?? "";

?>

<h4>{{ __("Dear Admin") }}!</h4>

<p><?php echo $summary;?></p>

<table>
    <tr>
        <td style="width: 30% !important;">{{ __("Registrant") }}</td>
        <td style="width: 69% !important;">: {{ $fullname }}</td>
    </tr>

    <tr>
        <td style="width: 30% !important;">{{ __("Contact phone") }}</td>
        <td style="width: 69% !important;">: {{ $phone }}</td>
    </tr>

    <tr>
        <td style="width: 30% !important;">{{ __("Registration time") }}</td>
        <td style="width: 69% !important;">: {{ $created_at }}</td>
    </tr>

    <tr>
        <td style="width: 30% !important;">{{ __("Message attack") }}</td>
        <td style="width: 69% !important;">: {{ $message }}</td>
    </tr>

</table>

<p/>
<p>{{ __("Please check and feedback soon")."." }}</p>

<p style="margin-top: 30px;"><?php echo __("Best regards,")."<br/>".__("MKE Admin System")?></p>

