@extends("Layouts.".$template)
@section('title', "Test")
@section("content")

    <?php

    $course_name    = $course->name ?? "";
    $summary        = $course->summary ?? "";
    $picture        = $course->picture ?? "";
    $picture_path   = empty($picture) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$course->company_id.'/Course/'.$course->id."/Picture/".$picture.'?t='.SYSTEM_CACHE;

    $cover        = $course->cover ?? "";

    $cover_path   = empty($cover) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$course->company_id.'/Course/'.$course->id."/Cover/".$cover.'?t='.SYSTEM_CACHE;
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    ?>

    <style>

        .table-classmate{
            width: 100%;

        }

        .table-classmate, .table-classmate > thead > tr > th, .table-classmate > tbody > tr > td{
            border: 1px solid #ddd;
            padding: 5px;
        }

        .col-no{
            width: calc((100%/12) * 1);
        }
        .col-classtime{
            width: calc((100%/12) * 6);
        }

        .col-slots{
            width: calc((100%/12) * 2);
        }

        .col-free-slot{
            width: calc((100%/12) * 2);
        }

        .col-action{
            width: calc((100%/12) * 1);
        }

        .btn-registration-course{
            border-radius: 4px !important;
            padding: 5px 10px;
            color: white !important;
        }

        .btn-registration-course:hover{
            color: #1eaaf1 !important;
        }

        .col-classrom-day{
            width: 60% !important;
        }

    </style>

    <section class="hero-wrap hero-wrap-2" style="background-image: url({{ $cover_path }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">{{ $course_name }}</h1>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="{{ url("/") }}">{{ __("Home") }} <i class="ion-ios-arrow-forward"></i></a>
                        </span>
                        <span>{{ __("Course") }} </span>
                    </p>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <h2 class="mb-3">#1. {{ $course_name }}</h2>
                    <p>{{ $summary }}</p>
                    <p>
                        <img src="{{ $picture_path }}" alt="" class="img-fluid border-smooth">
                    </p>

                    <h2 class="mb-3">#2. {{ __("Classroom") }}</h2>

                    <div class="">
                        <table class="table-classmate">
                            <thead>
                                <tr>
                                    <th class="col-no text-center">STT</th>
                                    <th class="col-classtime ">{{ __("Class-time") }}</th>
                                    <th class="col-slots text-center">{{ __("Slots") }}</th>
                                    <th class="col-free-slot text-center">{{ __("Free slot") }}</th>
                                    <th class="col-action text-center">{{ __("Registration") }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $arrDay = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
                                ?>
                                @foreach($listClassroom as $key => $item)
                                    <?php
                                        $id = $item->id;
                                        $class_time = "";



                                        $code = $item->code;
                                        $slot = $item->slot;
                                        $free_slot = $item->free_slot;
                                        $lost = $item->slot;

                                        $registration_url = url('course-registration/'.$code);

                                    ?>

                                    <tr>
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="">

                                            <table>
                                                <?php
                                                    foreach ($arrDay as $day){
                                                        if(!empty($item->{$day} && $item->{$day} !== "")){
                                                            ?>
                                                            <tr>
                                                                <td class="col-classrom-day">* {{ __(ucfirst($day)) }}</td>
                                                                <td class="col-classrom-time">: {{ $item->{$day} }}</td>
                                                            </tr>

                                                            <?php
                                                        }

                                                    }
                                                ?>
                                            </table>

                                        </td>
                                        <td class="text-center">{{ $slot }}</td>
                                        <td class="text-center">{{ $free_slot }}</td>

                                        <td class="text-center">
                                            <a role="button"  href="{{ $registration_url }}"  class="btn btn-primary btn-registration-course"><i class="fas fa-file-signature"></i></a>

                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tag-widget post-tag-container mb-5 mt-5">
                        <div class="tagcloud">
                            <div class="fb-comments" data-href="{{ $actual_link }}" data-width="100%" data-numposts="5"></div>
                        </div>
                    </div>


                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar ftco-animate">

                    @include('Elements.Course.popular', compact('company'))


                </div><!-- END COL -->
            </div>
        </div>
    </section>



@endsection

@section("pagescript")
{{--    <script src="{{asset('public/js/Course/index.js?t=2')}}"></script>--}}
@endsection


