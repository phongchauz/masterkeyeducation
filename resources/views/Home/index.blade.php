@extends("Layouts.".$template)
@section('title', "Test")
@section("content")

    <?php

    $listColumnCheck = [
        'slogan_display_homepage',
        'mission_display_homepage',
        'registration_display_homepage',
        'teacher_display_homepage',
        'course_display_homepage',
        'experience_display_homepage',
        'comment_display_homepage',
        'registration_form_display_homepage',
        'pricing_display_homepage',
        'blog_display_homepage',
    ];

    foreach ($listColumnCheck as $column_check){
        ${$column_check} = isset($site_setting->{$column_check}) ? $site_setting->{$column_check} : STATUS_INACTIVE;
    }

    $mission_label = isset($site_setting->mission_label) ? $site_setting->mission_label : "";
    $mission_content = isset($site_setting->mission_content) ? $site_setting->mission_content : "";

    $slogan_label = isset($site_setting->slogan_label) ? $site_setting->slogan_label : "";
    $slogan_content = isset($site_setting->slogan_content) ? $site_setting->slogan_content : "";

    $registration_label = isset($site_setting->registration_label) ? $site_setting->registration_label : "";
    $registration_content = isset($site_setting->registration_content) ? $site_setting->registration_content : "";
    $registration_picture = isset($site_setting->registration_picture) ? $site_setting->registration_picture : "";
    $registration_picture_path = empty($registration_picture) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$company_id.'/SiteSetting/RegistrationPicture/'.$registration_picture.'?t='.SYSTEM_CACHE;

    $teacher_label = isset($site_setting->teacher_label) ? $site_setting->teacher_label : "";
    $teacher_content = isset($site_setting->teacher_content) ? $site_setting->teacher_content : "";

    $course_label = isset($site_setting->course_label) ? $site_setting->course_label : "";
    $course_content = isset($site_setting->course_content) ? $site_setting->course_content : "";

    $experience_label = isset($site_setting->experience_label) ? $site_setting->experience_label : "";
    $experience_content = isset($site_setting->experience_content) ? $site_setting->experience_content : "";

    $comment_label = isset($site_setting->comment_label) ? $site_setting->comment_label : "";
    $comment_content = isset($site_setting->comment_content) ? $site_setting->comment_content : "";

    $pricing_label = isset($site_setting->pricing_label) ? $site_setting->pricing_label : "";
    $pricing_content = isset($site_setting->pricing_content) ? $site_setting->pricing_content : "";

    $blog_label = isset($site_setting->blog_label) ? $site_setting->blog_label : "";
    $blog_content = isset($site_setting->blog_content) ? $site_setting->blog_content : "";
    ?>

    @include('Layouts.kiddos.banner', ['listData' => $listBanner])




    <?php
    $col_left = $slogan_display_homepage && $mission_display_homepage ? "col-md-5" : "col-md-12";

    $col_right = $slogan_display_homepage && $mission_display_homepage ? "col-md-7" : "col-md-12";
    ?>

    @if($slogan_display_homepage)
        <section class="ftco-section ftco-no-pt ftc-no-pb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 order-md-last wrap-about py-5  wrap-about bg-light">
                        <div class="text px-4 ftco-animate">
                            <h2 class="mb-4">{{ __($slogan_label) }}</h2>
                            <p style="white-space: pre-wrap;">{{ $slogan_content }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if($slogan_display_homepage)
        <section class="ftco-section ftco-no-pt ftc-no-pb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 order-md-last wrap-about py-5  wrap-about ">
                        @include('Layouts.kiddos.mission', ['listData' => $listMission])
                    </div>
                </div>
            </div>
        </section>
    @endif



    @include('Layouts.kiddos.core_value', ['listData' => $listCoreValue])

    @if($registration_display_homepage)
        <section class="ftco-intro" style="background-image: url({{  $registration_picture_path }});" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h2>{{ $registration_label }}</h2>
                        <p class="mb-0">{{ $registration_content }}</p>
                    </div>
                    <div class="col-md-3 d-flex align-items-center">
                        <p class="mb-0"><a href="javascript:;" id="btn-registration" class="btn btn-secondary px-4 py-3">{{ __("Take a Course") }}</a></p>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if($teacher_display_homepage)
        @include('Layouts.kiddos.teacher', ['listData' => $listTeacher])
    @endif

    @if($course_display_homepage)
        @include('Layouts.kiddos.course', ['listData' => $listCourseGroup])
    @endif
    @if($experience_display_homepage)
        @include('Layouts.kiddos.experience', ['listTeacher' => $listTeacher, "listCourse" => $listCourse])
    @endif
    @if($comment_display_homepage)
        @include('Layouts.kiddos.comment', ['listData' => $listComment])
    @endif
    @if($registration_form_display_homepage)
        @include('Layouts.kiddos.registration_form', ['listData' => $listComment, 'listCourse' => $listCourse])
    @endif
    @if($pricing_display_homepage)
        @include('Layouts.kiddos.pricing', ['listData' => []])
    @endif
    @if($blog_display_homepage)
        @include('Layouts.kiddos.blog', ['listData' => []])
    @endif


    @include('Layouts.kiddos.gallery', ['listData' => $listGalleries])












@endsection

@section("pagescript")

@endsection


