@extends("Layouts.metronic_v7")
@section('title', $title)
@section("content")

    <link rel="stylesheet" href="{{  asset('public/css/Admin/access-module.css?t=1') }}">

    <?php
    foreach ($listModule as $key => $item){
    $id = $item['id'];
    $code = $item['code'];
    $name = $item['name'];
    $description = $item['description'];
    $cls_module = $item['cls_module'];
    $url = $code."/dashboard/index/";
    ?>
    <div class="column">
        <div class="pricing-card basic {{ $cls_module }}">
            <div class="pricing-header">
                <span class="plan-title">{{ __("") }}</span>
                <div class="price-circle">
                    <span class="price-title">
                        <span>{{ __($name) }}</span>
                    </span>
                </div>
            </div>
            <div class="badge-box">

            </div>
            <ul>
                <li>{{ __($description) }}</li>
            </ul>
            <div class="buy-button-box">
                <a data-id="{{ $id }}" data-href="{{ url($url) }}" class="buy-now access-module">{{ __("Access") }}</a>
            </div>
        </div>
    </div>
    <?php
    }
    ?>




    <script>
        var titleAdd = "{{ __('Add New') }}",
                titleEdit = "{{ __('Edit') }}";
    </script>

    <script src="{{asset('public/js/Admin/Module/access.js?t='.time())}}"></script>

@endsection
