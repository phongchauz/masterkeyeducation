@extends("Layouts.metronic_v7")
@section('title', $title)
@section("content")
    <?php
    $col_left = "col-2";
    $col_right = "col-10";

    $id = isset($site_setting->id) ? $site_setting->id : -1;
    $company_id = isset($site_setting->company_id) ? $site_setting->company_id : -1;



    $listSectionData = [
        ['name' => "Mission", "column" => 'mission', 'folder_upload' => "MissionPicture", "picture_suggest" => "Picture must constrain with scale is 2000x1333 or ratio is 6:4"],
        ['name' => "Slogan", "column" => 'slogan', 'folder_upload' => "SloganPicture", "picture_suggest" => "Picture must constrain with scale is 2000x1333 or ratio is 6:4"],
        ['name' => "Registration", "column" => 'registration', 'folder_upload' => "RegistrationPicture", "picture_suggest" => "Picture must constrain with scale is 1500x1000 or ratio is 6:4"],
        ['name' => "Teacher", "column" => 'teacher', 'folder_upload' => "TeacherPicture", "picture_suggest" => "Picture must constrain with scale is 800x850 or ratio is same 1:1"],
        ['name' => "Courses", "column" => 'course', 'folder_upload' => "CoursePicture", "picture_suggest" => "Picture must constrain with scale is 800x600 or ratio is 4:3"],
        ['name' => "Experience", "column" => 'experience', 'folder_upload' => "ExperiencePicture", "picture_suggest" => "Picture must constrain with scale is 1500x1100 or ratio is 3:2"],
        ['name' => "Family Comment", "column" => 'comment', 'folder_upload' => "CommentPicture", "picture_suggest" => "Picture must constrain with scale is 800x850 or ratio is same 1:1"],
        ['name' => "Registration Form", "column" => 'registration_form', 'folder_upload' => "RegistrationFormPicture", "picture_suggest" => "Picture must constrain with scale is 1500x1100 or ratio is 3:2"],
        ['name' => "Pricing", "column" => 'pricing', 'folder_upload' => "PricingPicture", "picture_suggest" => "Picture must constrain with scale is 2000x1333 or ratio is 6:4"],
        ['name' => "Blog", "column" => 'blog', 'folder_upload' => "BlogPicture", "picture_suggest" => "Picture must constrain with scale is 2000x1333 or ratio is 6:4"],
    ];

    foreach ($listSectionData as $key => $item){
        $column = $item['column'];
        $folder_upload = $item['folder_upload'];
        ${$column."_label"} = isset($site_setting->{$column."_label"}) ? $site_setting->{$column."_label"} : "";
        ${$column."_content"} = isset($site_setting->{$column."_content"}) ? $site_setting->{$column."_content"} : "";
        ${$column."_guild_picture_path"} = $current_domain.'/public/images/home_guild/'.$column.'.jpg';
        ${$column."_picture"} = isset($site_setting->{$column."_picture"}) ? $site_setting->{$column."_picture"} : "";
        ${$column."_picture_path"} = empty(${$column."_picture"}) ? $current_domain.'/images/no_img.jpg' :
            $current_domain.'/uploads/'.$company_id.'/SiteSetting/'.$folder_upload.'/'.${$column."_picture"}.'?t='.SYSTEM_CACHE;
        ${$column."_display_homepage"} = isset($site_setting->{$column."_display_homepage"}) && $site_setting->{$column."_display_homepage"} ? "checked" : "";

        $data = [
            'label'              => ${$column."_label"},
            'content'            => ${$column."_content"},
            'guild_picture_path' => ${$column."_guild_picture_path"},
            'picture_path'       => ${$column."_picture_path"},
            'display_homepage'   => ${$column."_display_homepage"},
        ];

        $listSectionData[$key]['data'] = $data;

    }


    ?>



    <div class="row">
        <div class="col-md-12">

            <form id="form-data" method="post" enctype="multipart/form-data">
                <input class="hide" name="id" value="{{ $id }}">

                @foreach($listSectionData as $key => $item)
                    <?php
                        $name               = $item['name'];
                        $column             = $item['column'];
                        $data               = $item['data'];
                        $picture_suggest    = $item['picture_suggest'];
                    ?>

                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <h3 class="card-title">{{ __($name) }}</h3>
                                <div class="card-toolbar">
                                    <a href="{{ $data['guild_picture_path'] }}" target="_blank" class="btn btn-icon btn-sm btn-light-success mr-1" title="{{ __("Click to view guid") }}">
                                        <i class="fas fa-image"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="card-body">

                                <div class="form-group row">
                                    <label class="{{ $col_left }} col-form-label">{{ __("Label") }}</label>
                                    <div class="{{ $col_right }}">
                                        <input class="form-control" type="text" name="{{ $column }}_label" value="{{ $data['label'] }}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="{{ $col_left }} col-form-label">{{ __("Content") }}</label>
                                    <div class="{{ $col_right }}">
                                        <textarea class="form-control" type="text" name="{{ $column }}_content">{{ $data['content'] }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="{{ $col_left }} col-form-label">{{ __("Picture") }} <a class="tooltips pointer" title="{{ __($picture_suggest) }}"><i class="fas fa-info-circle text-danger"></i></a></label>
                                    <div class="{{ $col_right }}">
                                        @include("Elements.file_input_upload", [
                                            'domId'         => $column.'_picture',
                                            'nameHtml'      => $column.'_picture',
                                            'multiple'      => 0,
                                            'browseOnZone'  => 1,
                                            'maxFile'       => 1,
                                            'filePath'      => $data['picture_path'],
                                        ])
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="{{ $col_left }} col-form-label">{{ __("Display Homepage") }}</label>
                                    <div class="{{ $col_right }} col-form-label">
                                        <div class="checkbox-inline">
                                            <label class="checkbox checkbox-outline checkbox-success">
                                                <input type="checkbox" name="{{ $column }}_display_homepage" {{ $data['display_homepage'] }}>
                                                <span></span>
                                            </label>

                                        </div>

                                    </div>
                                </div>
                                <!--end: Code-->
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="{{ $col_left }}"></div>
                                    <div class="{{ $col_right }} text-center">
                                        <button type="submit" class="btn btn-primary btn-act-lg mr-2"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                        <button type="reset" class="btn btn-secondary btn-act-lg"><i class="fas fa-sync"></i> {{ __("Refresh") }}</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                @endforeach

            </form>
        </div>

    </div>






@endsection

@section("pagescript")
    <script src="{{asset('public/js/Admin/SiteSetting/index.js?t='.SYSTEM_CACHE)}}"></script>
@endsection


