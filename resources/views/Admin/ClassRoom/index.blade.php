@extends("Layouts.metronic_v7")
@section('title', $title)
@section("content")
    <?php

    ?>
    <style>
        .col-action-width {
            width: 70px;
            max-width: 70px;
        }

        .ng-input-button-container .ng-button {
            height: 34px;
        }

        .ng-button-icon-span {
            margin-top: 2px;
        }

        .table-class-time > tbody > tr > td{
            border: none !important;
            padding: 5px !important;
        }

    </style>

    <?php $placeholder = __('Search by slug, name ...'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom" id="kt_card_2">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label"><i class="fas fa-shapes"></i> {{ __($title) }}</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="javascript:;" class="btn btn-icon btn-sm btn-primary mr-1 btn-add" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="{{ __("Add new") }}">
                            <i class="fa fa-plus icon-nm"></i>
                        </a>
                        <a href="#" class="btn btn-icon btn-sm btn-success mr-1 btn-refresh" data-card-tool="reload" data-toggle="tooltip" data-placement="top" title="{{ __("Refresh") }}">
                            <i class="ki ki-reload icon-nm"></i>
                        </a>

                    </div>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12 table-responsive-lg">

                            @include("Elements.search_box", compact('placeholder'))

                            <table id="searchTable" class="table table-bordered ">
                                <thead>
                                    <tr>
                                        <th  class=" w-40px" rowspan="2">{{ __('No.') }}</th>
                                        <th  class=" w-80px" rowspan="2">{{ __('Code') }}</th>
                                        <th  class=" w-150px" rowspan="2">{{ __('Class time') }}</th>
                                        <th  class=" w-150px" rowspan="2">{{ __('Course') }}</th>
                                        <th  class=" w-100px" rowspan="2">{{ __('Teacher') }}</th>
                                        <th  class=" text-center" colspan="3">{{ __('Slots') }}</th>
                                        <th  class=" " rowspan="2">{{ __('Note') }}</th>
                                        <th  class=" w-100px" rowspan="2">{{ __('Status') }}</th>
                                        <th  class=" w-100px" rowspan="2">{{ __('Action') }}</th>
                                    </tr>
                                    <tr>
                                        <th  class=" w-50px">{{ __('Total') }}</th>
                                        <th  class=" w-50px">{{ __('Registered') }}</th>
                                        <th  class=" w-50px border-rw">{{ __('Free') }}</th>
                                    </tr>

                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        var titleAdd = "{{ __('Add new') }}",
                titleEdit = "{{ __('Edit') }}";

    </script>

    @include("Elements.modal_confirm_delete")
    @include("Admin.Elements.ClassRoom.form")


@endsection

@section("pagescript")
    <script src="{{asset('public/js/Admin/ClassRoom/index.js?t=1')}}"></script>
@endsection


