<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="id" hidden>


                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="required_label">{{ __("Name") }}</label>
                                        <input type="text" class="form-control" name="name" placeholder="{{ __("Enter name") }}">
                                    </div>

                                    <div class="form-group">
                                        <label class="required_label">{{ __("Slug") }}</label>
                                        <input type="text" class="form-control" name="slug" placeholder="{{ __("Enter slug") }}">
                                    </div>

                                    <div class="form-group">
                                        <label class="">{{ __("Summary") }}</label>
                                        <textarea type="text" class="form-control" name="summary" placeholder="{{ __("Enter summary") }}"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">{{ __("Picture") }} <a class="tooltips pointer" title="{{ __("Picture must constrain with scale is 500x500 or ratio is 1:1") }}"><i class="fas fa-info-circle text-danger"></i></a></label>
                                        @include("Elements.file_input_upload", [
                                            'domId'         => 'picture',
                                            'nameHtml'      => 'picture',
                                            'multiple'      => 0,
                                            'browseOnZone'  => 1,
                                            'maxFile'       => 1,
                                        ])
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label">{{ __("Cover") }}</label>
                                        @include("Elements.file_input_upload", [
                                            'domId'         => 'cover',
                                            'nameHtml'      => 'cover',
                                            'multiple'      => 0,
                                            'browseOnZone'  => 1,
                                            'maxFile'       => 1,
                                        ])
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __("More options") }}</label>
                                        <div class="checkbox-list">

                                            <label class="checkbox">
                                                <input type="checkbox"  name="display_homepage">
                                                <span></span> {{ __("Display homepage") }}
                                            </label>

                                            <label class="checkbox">
                                                <input type="checkbox" checked="checked" name="status">
                                                <span></span> {{ __("Active") }}
                                            </label>

                                            <label class="checkbox chk-continue">
                                                <input type="checkbox" name="continue">
                                                <span></span> {{ __("Continue add") }}
                                            </label>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>{{ __("Content") }}</label>
                                        <textarea type="text" class="form-control" name="content" id="content"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        activeSummerNote(490, 'monokai','#content')
    });
</script>