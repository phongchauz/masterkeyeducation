<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="id" hidden>
                            <input type="text" class="form-control " name="status" hidden>
                            <input type="text" class="form-control " name="_token" hidden>

                            <ul class="nav nav-tabs nav-tabs-line">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_classroom">{{ __("Course/Classroom") }}</a>
                                </li>

                            </ul>
                            <div class="tab-content mt-5 " id="tab_classroom">
                                <div class="tab-pane fade show active" id="show_course" role="tabpanel" aria-labelledby="tab_classroom">
                                    <table class="border-0 full-width">
                                        <tr>
                                            <td class="col-md-3 col-lg-2 pl-0"><i class="fas fa-code"></i> {{ __("Classroom") }}</td>
                                            <td class="col-md-9 col-lg-10 col-code"></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-3 col-lg-2 pl-0"><i class="fas fa-book-spells"></i> {{ __("Course") }}</td>
                                            <td class="col-md-9 col-lg-10 col-course"></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>

                            <ul class="nav nav-tabs nav-tabs-line">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_fullanme">{{ __("Parents Information") }}</a>
                                </li>

                            </ul>
                            <div class="tab-content mt-5 " id="tab_fullanme">
                                <div class="tab-pane fade show active" id="show_fullname" role="tabpanel" aria-labelledby="tab_classroom">

                                    <table class="border-0  full-width">
                                        <tr>
                                            <td class="col-md-3 col-lg-2 pl-0"><i class="fas fa-user-tie"></i> {{ __("Fullname") }}</td>
                                            <td class="col-md-9 col-lg-10 col-fullname"></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-3 col-lg-2 pl-0"><i class="fas fa-mobile"></i> {{ __("Phone") }}</td>
                                            <td class="col-md-9 col-lg-10 col-phone"></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-3 col-lg-2 pl-0"><i class="fas fa-envelope"></i> {{ __("Contact email") }}</td>
                                            <td class="col-md-9 col-lg-10 col-email"></td>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                            <ul class="nav nav-tabs nav-tabs-line">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_message">{{ __("Message") }}</a>
                                </li>

                            </ul>
                            <div class="tab-content mt-5 mb-5" id="tab_message">
                                <div class="tab-pane fade show active" id="show_message" role="tabpanel" aria-labelledby="tab_classroom">Tab content 1</div>
                            </div>


                            <div class="card-footer text-center">
                                <a role="button" data-status="2" class="btn btn-primary btn-submit-approval font-weight-bold btn-act-lg"><i class="fas fa-check"></i> {{ __("Accept") }}</a>
                                <a role="button" data-status="0" class="btn btn-danger btn-submit-approval font-weight-bold btn-act-lg"><i class="fas fa-ban"></i> {{ __("Denied") }}</a>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        activeSummerNote(490, 'monokai','#content')
    });
</script>