<?php

use Illuminate\Support\Facades\Config;

$listStatus = [
    ['id' => -1, 'name' => "All"],
    ['id' => 1, 'name' => "New"],
    ['id' => 2, 'name' => "Approved"],
    ['id' => 0, 'name' => "Canceled"],
];
?>



<div class="card card-custom mgbt-10">
    <!--begin::Form-->
    <form method="post" id="form-search">
        <div class="card-body py-0">
            <div class="row">
                <div class="col-md-4 pdr-5">
                    <div class="form-group">
                        <label>{{ __("Keywords") }}:</label>
                        <input type="text" class="form-control" name="s_key_search"  placeholder="{{ __("Mr.Chau") }}"/>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>




                <div class="col-md-2 col-lg-3 pdr-5 pdl-0">
                    <div class="form-group">
                        <label>{{ __("Course") }}:</label>
                        @include("Elements.cbb", [
                                'domeHtml' 		=> 's_course_id',
                                'nameHtml' 		=> 's_course_id',
                                'clsHtml' 		=> 'no-border change-data',
                                'displayField'  => 'name',
                                'isAll'  	    => true,
                                'valueField'  	=> 'id',
                                'listData' 		=> $listCourse,
                            ])

                    </div>
                </div>

                <div class="col-md-2 pdr-5 pdl-0">
                    <div class="form-group">
                        <label>{{ __("Classroom") }}:</label>
                        <div id="s-classroom-area">
                            @include("Elements.cbb", [
                                'domeHtml' 		=> 's_class_room_id',
                                'nameHtml' 		=> 's_class_room_id',
                                'clsHtml' 		=> 'no-border',
                                'displayField'  => 'code',
                                'isAll'  	    => true,
                                'valueField'  	=> 'id',
                                'listData' 		=> $listClassroom,
                            ])
                        </div>

                    </div>
                </div>

                <div class="col-md-2 pdr-5 pdl-0">
                    <div class="form-group">
                        <label>{{ __("Status") }}:</label>
                        @include("Elements.cbb", [
                                'domeHtml' 		=> 's_status',
                                'nameHtml' 		=> 's_status',
                                'clsHtml' 		=> 'no-border',
                                'displayField'  => 'name',
                                'isAll'  	    => false,
                                'valueField'  	=> 'id',
                                'listData' 		=> $listStatus,
                            ])

                    </div>
                </div>


                <div class="col-md-2 col-lg-1 pdl-0">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-block btn-primary mr-2"><i class="far fa-search"></i> {{ __("Search") }}</button>
                    </div>
                </div>
            </div>


        </div>

    </form>
    <!--end::Form-->
</div>
