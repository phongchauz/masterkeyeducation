<div class="modal fade" id="modalEdit" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <form role="form" id="form-data" method="post">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control hide" name="id">

                                <div class="form-group">
                                    <label class="control-label">{{ __("Code") }}(<span class="text-required">*</span>)</label>
                                    <input type="text" class="form-control" name="code" required placeholder="{{ __("Code") }}">
                                </div>


                                <div class="form-group">
                                    <label class="control-label">{{ __("Name") }}(<span class="text-required">*</span>)</label>
                                    <input type="text" class="form-control" name="name" required placeholder="{{ __("Name") }}">
                                </div>



                                <div class="form-group">
                                    <label class="control-label">{{ __("Description") }}</label>
                                    <textarea type="text" class="form-control" name="description"  placeholder="{{ __("Description") }}"></textarea>
                                </div>



                                <div class="form-group">
                                    <label><?php echo __('More options');?>:</label>
                                    <div class="mt-checkbox-list">

                                        <label class="mt-checkbox mt-checkbox-outline" id="status">
                                            <input type="checkbox" name="status" checked> <?php echo __('Active');?>
                                            <span></span>
                                        </label>

                                        <label class="mt-checkbox mt-checkbox-outline continue-add" id="continue">
                                            <input type="checkbox" name="continue"> <?php echo __('Continue add');?>
                                            <span></span>
                                        </label>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-round btn-primary btn-save btn-act-gird btn-same-width">
                        <i class="fas fa-save"></i>
                        {{ __("Save") }}
                    </button>
                    <button type="button" class="btn btn-round dark btn-outline btn-act-gird btn-same-width" data-dismiss="modal">
                        <i class="fas fa-times"></i>
                        {{ __("Close") }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

