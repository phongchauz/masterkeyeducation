
<form method="post" id="form-search">
    <div class="row">
        <div class="col-md-2 pdr-5 pdr-5">
            <div class="form-group">
                <label>{{ __("Product Type") }}</label>
                @include("Elements.cbb", [
                        'domeHtml' 		=> 's_product_type_id',
                        'nameHtml' 		=> 's_product_type_id',
                        'displayField'  => 'name',
                        'isAll'  	    => true,
                        'valueField'  	=> 'id',
                        'listData' 		=> $listProductType,
                    ])

            </div>
        </div>

        <div class="col-md-9  pdl-0">
            <div class="form-group">
                <label>{{ __("Keywords") }}</label>
                <input type="text" class="form-control" name="s_key_search" id="s_key_search"  placeholder="{{ __("Hoa hồng") }}"/>
                <span class="form-text text-muted"></span>
            </div>
        </div>



        <div class="col-md-1 pdl-0">
            <div class="form-group">
                <label>&nbsp;</label>
                <button type="submit"  class="btn btn-block btn-primary mr-2" id="btn-search"><i class="far fa-search"></i> {{ __("Search") }}</button>
            </div>
        </div>

    </div>
</form>
