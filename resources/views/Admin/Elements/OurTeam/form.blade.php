<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="id" hidden>


                            <div class="form-group">
                                <label class="required_label">{{ __("Fullname") }}</label>
                                <input type="text" class="form-control" name="fullname" placeholder="{{ __("Enter fullname") }}">
                            </div>

                            <div class="form-group">
                                <label class="required_label">{{ __("Position") }}</label>
                                <input type="text" class="form-control" name="position" placeholder="{{ __("Enter position") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Facebook Url") }}</label>
                                <input type="text" class="form-control" name="facebook_url" placeholder="{{ __("Enter Facebook Url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Twitter Url") }}</label>
                                <input type="text" class="form-control" name="twitter_url" placeholder="{{ __("Enter Twitter Url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("LinkedIn Url") }}</label>
                                <input type="text" class="form-control" name="linkedin_url" placeholder="{{ __("Enter LinkedIn Url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Instagram Url") }}</label>
                                <input type="text" class="form-control" name="instagram_url" placeholder="{{ __("Enter Instagram Url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Description") }}</label>
                                <textarea type="text" rows="5" class="form-control" name="description" placeholder="{{ __("Enter description") }}"></textarea>
                            </div>



                            <div class="form-group">
                                <label class="control-label">{{ __("Picture") }}</label>
                                @include("Elements.file_input_upload", [
                                    'domId'         => 'picture',
                                    'nameHtml'      => 'picture',
                                    'multiple'      => 0,
                                    'browseOnZone'  => 1,
                                    'maxFile'       => 1,
                                ])
                            </div>



                            <div class="form-group">
                                <label>{{ __("More options") }}</label>
                                <div class="checkbox-list">


                                    <label class="checkbox">
                                        <input type="checkbox"  name="is_default">
                                        <span></span> {{ __("Is Default") }}
                                    </label>

                                    <label class="checkbox">
                                        <input type="checkbox" checked="checked" name="status">
                                        <span></span> {{ __("Active") }}
                                    </label>

                                    <label class="checkbox chk-continue">
                                        <input type="checkbox" name="continue">
                                        <span></span> {{ __("Continue add") }}
                                    </label>

                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

