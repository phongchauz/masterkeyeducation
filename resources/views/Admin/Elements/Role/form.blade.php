<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form-data" method="post" enctype="multipart/form-data">
                                <input type="text" class="form-control " name="id" hidden>
                                <div class="form-group">
                                    <label>{{ __("Is Admin") }}<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" name="is_admin" placeholder="{{ __("Enter is_admin value") }}">
                                </div>

                                <div class="form-group">
                                    <label>{{ __("Name") }}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name" placeholder="{{ __("Enter name") }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __("Description") }}</label>
                                    <textarea type="text" class="form-control" name="description" placeholder="{{ __("Enter Description") }}"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>{{ __("More options") }}</label>
                                    <div class="checkbox-list">

                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked" name="status">
                                            <span></span> {{ __("Active") }}
                                        </label>

                                        <label class="checkbox">
                                            <input type="checkbox" name="continue">
                                            <span></span> {{ __("Continue add") }}
                                        </label>

                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                    <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



        </div>
    </div>
</div>

