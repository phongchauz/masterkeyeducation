<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="id" hidden>

                            <div class="form-group">
                                <label class="required_label">{{ __("Code") }}</label>
                                <input type="text" class="form-control" name="code" placeholder="{{ __("Enter code") }}">
                            </div>

                            <div class="form-group">
                                <label class="required_label">{{ __("Course") }}</label>
                                @include("Elements.cbb", [
                                    'domeHtml' 		=> 'course_id',
                                    'nameHtml' 		=> 'course_id',
                                    'displayField'  => 'name',
                                    'isAll'  	    => false,
                                    'valueField'  	=> 'id',
                                    'listData' 		=> $listCourse,
                                ])
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Teacher") }}</label>
                                @include("Elements.cbb", [
                                    'domeHtml' 		=> 'teacher_id',
                                    'nameHtml' 		=> 'teacher_id',
                                    'displayField'  => 'name',
                                    'isNone'  	    => true,
                                    'valueField'  	=> 'id',
                                    'listData' 		=> $listTeacher,
                                ])
                            </div>

                            <div class="row">
                                <div class="col-md-3 pr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Monday") }}</label>
                                        <input type="text" class="form-control" name="monday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3 plr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Tuesday") }}</label>
                                        <input type="text" class="form-control" name="tuesday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3 plr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Wednesday") }}</label>
                                        <input type="text" class="form-control" name="wednesday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3  pl-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Thursday") }}</label>
                                        <input type="text" class="form-control" name="thursday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 pr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Friday") }}</label>
                                        <input type="text" class="form-control" name="friday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3  plr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Saturday") }}</label>
                                        <input type="text" class="form-control" name="saturday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3  plr-5px">
                                    <div class="form-group">
                                        <label class="">{{ __("Sunday") }}</label>
                                        <input type="text" class="form-control" name="sunday" placeholder="{{ __("18h - 19h30") }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="required_label">{{ __("Slots") }}</label>
                                <input type="text" class="form-control" name="slot" placeholder="{{ __("Enter slots") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Note") }}</label>
                                <textarea type="text" rows="3" class="form-control" name="note" placeholder="{{ __("Enter note") }}"></textarea>
                            </div>

                            <div class="form-group">
                                <label>{{ __("More options") }}</label>
                                <div class="checkbox-list">

                                    <label class="checkbox">
                                        <input type="checkbox" checked="checked" name="status">
                                        <span></span> {{ __("Active") }}
                                    </label>

                                    <label class="checkbox chk-continue">
                                        <input type="checkbox" name="continue">
                                        <span></span> {{ __("Continue add") }}
                                    </label>

                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

