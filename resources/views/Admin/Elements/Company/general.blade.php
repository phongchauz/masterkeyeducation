<?php

$id             = $company['id'] ?? -1;
$name           = $company['name'] ?? '';
$phone_number   = $company['phone_number'] ?? '';
$email          = $company['email'] ?? '';
$address        = $company['address'] ?? '';
$logo           = $company['logo'] ?? '';
$logo_title     = $company['logo_title'] ?? '';
$logo_alt       = $company['logo_alt'] ?? '';
$favicon        = $company['favicon'] ?? '';
$logo_path      = empty($logo) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$id.'/Logo/'.$logo.'?t='.SYSTEM_CACHE;

$favicon_path      = empty($favicon) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$id.'/Favicon/'.$favicon.'?t='.SYSTEM_CACHE;

$status         = isset($company['status']) && $company['status'] ? 'checked' : '';

?>

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{__("General Information")}}
        </h3>
        <div class="card-toolbar">

        </div>
    </div>
    <!--begin::Form-->
    <form id="form-general" method="post" enctype="multipart/form-data">
        <div class="card-body">

            <input type="text" class="form-control " name="id" hidden>
            <div class="form-group">
                <label>{{ __("Name") }}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" value="{{ $name }}" placeholder="{{ __("Enter company name") }}">
            </div>

            <div class="form-group">
                <label>{{ __("Logo") }}</label>
                @include("Elements.file_input_upload", [
                    'domId'         => 'logo',
                    'nameHtml'      => 'logo',
                    'multiple'      => 1,
                    'browseOnZone'  => 1,
                    'maxFile'       => 1,
                    'filePath'      => $logo_path,
                ])
            </div>

            <div class="form-group">
                <label>{{ __("Logo title") }}</label>
                <input type="text" class="form-control" name="logo_title" value="{{ $logo_title }}" placeholder="{{ __("Logo title") }}">
            </div>

            <div class="form-group">
                <label>{{ __("Logo alt") }}</label>
                <input type="text" class="form-control" name="logo_alt" value="{{ $logo_alt }}" placeholder="{{ __("Logo alt") }}">
            </div>

            <div class="form-group">
                <label>{{ __("Favicon") }}</label>
                @include("Elements.file_input_upload", [
                    'domId'         => 'favicon',
                    'nameHtml'      => 'favicon',
                    'multiple'      => 1,
                    'browseOnZone'  => 1,
                    'maxFile'       => 1,
                    'filePath'      => $favicon_path,
                ])
            </div>

            <div class="form-group">
                <label>{{ __("Phone") }}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone_number" value="{{ $phone_number }}" placeholder="{{ __("Enter phone") }}">
            </div>

            <div class="form-group">
                <label>{{ __("Email") }}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="email" value="{{ $email }}" placeholder="{{ __("Enter email") }}">
            </div>

            <div class="form-group">
                <label>{{ __("Address") }}</label>
                <textarea type="text" class="form-control" name="address" placeholder="{{ __("Address") }}">{{ $address }}</textarea>
            </div>

            <div class="form-group">
                <label>{{ __("Status") }}</label>
                <div class="checkbox-list">

                    <label class="checkbox">
                        <input type="checkbox" checked="checked" name="status" {{ $status }}>
                        <span></span> {{ __("Active") }}
                    </label>


                </div>
            </div>
        </div>
        <div class="card-footer text-center">
            <button type="submit" class="btn btn-primary mr-2 btn-act-lg"><i class="fa fa-save"></i> {{ __("Save changes") }}</button>
            <button type="reset" class="btn btn-secondary  btn-act-lg"><i class="fa fa-sync"></i> {{ __("Refresh") }}</button>
        </div>
    </form>
    <!--end::Form-->
</div>
