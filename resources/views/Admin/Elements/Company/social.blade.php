<?php

$listField = [
    ['label' => "Facebook Fanpage", "column" => "facebook_url", "cls_icon" => "fab fa-facebook", "placeholder" => "Enter facebook fanpage url"],
    ['label' => "Youtube Chanel", "column" => "youtube_url", "cls_icon" => "fab fa-youtube", "placeholder" => "Enter youtube chanel url"],
    ['label' => "Twitter", "column" => "twitter_url", "cls_icon" => "fab fa-twitter", "placeholder" => "Enter twitter url"],
    ['label' => "Instagram", "column" => "instagram_url", "cls_icon" => "fab fa-instagram", "placeholder" => "Enter instagram url"],
];

$id             = isset($company['id']) ? $company['id'] : -1;
$facebook_url   = isset($company['facebook_url']) ? $company['facebook_url'] : '';
$youtube_url    = isset($company['youtube_url']) ? $company['youtube_url'] : '';
$twitter_url    = isset($company['twitter_url']) ? $company['twitter_url'] : '';
$instagram_url  = isset($company['instagram_url']) ? $company['instagram_url'] : '';


?>

<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            {{__("General Information")}}
        </h3>
        <div class="card-toolbar">

        </div>
    </div>
    <!--begin::Form-->
    <form id="form-social" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <input type="text" class="form-control " name="id" value="{{ $id }}" hidden>

            @foreach($listField  as $key => $item)
                <?php
                    $value  = isset($company[$item["column"]]) ? $company[$item["column"]] : '';
                ?>
                <div class="form-group">
                    <label>{{ __("Name") }}<span class="text-danger">*</span></label>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="{{ $item["cls_icon"] }}"></i></span>
                        </div>
                        <input type="text" class="form-control" name="{{ $item["column"] }}" value="{{ $value }}" placeholder="{{ __($item["placeholder"]) }}">
                    </div>

                </div>

            @endforeach


        </div>
        <div class="card-footer text-center">
            <button type="submit" class="btn btn-primary mr-2 btn-act-lg"><i class="fa fa-save"></i> {{ __("Save changes") }}</button>
            <button type="reset" class="btn btn-secondary  btn-act-lg"><i class="fa fa-sync"></i> {{ __("Refresh") }}</button>
        </div>
    </form>
    <!--end::Form-->
</div>
