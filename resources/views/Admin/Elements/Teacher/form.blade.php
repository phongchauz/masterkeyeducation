<div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="modalEdit"  aria-modal="true" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="post" enctype="multipart/form-data">
                            <input type="text" class="form-control " name="id" hidden>

                            <div class="row">
                                <div class="col-md-4 p-right-2">
                                    <div class="form-group">
                                        <label class="required_label">{{ __("Code") }}</label>
                                        <input type="text" class="form-control" name="code" placeholder="{{ __("Enter code") }}">
                                    </div>
                                </div>
                                <div class="col-md-8 p-left-2">
                                    <div class="form-group">
                                        <label class="required_label">{{ __("Name") }}</label>
                                        <input type="text" class="form-control" name="name" placeholder="{{ __("Enter name") }}">
                                    </div>
                                </div>
                            </div>





                            <div class="form-group">
                                <label class="">{{ __("Position") }}</label>
                                <input type="text" class="form-control" name="position" placeholder="{{ __("Enter position") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Introduction") }}</label>
                                <textarea type="text" class="form-control" name="introduction" placeholder="{{ __("Enter introduction") }}"></textarea>
                            </div>



                            <div class="form-group">
                                <label class="control-label">{{ __("Picture") }} <a class="tooltips pointer" title="{{ __("Picture must constrain with scale is 800x850 or ratio is same 1:1") }}"><i class="fas fa-info-circle text-danger"></i></a></label>
                                @include("Elements.file_input_upload", [
                                    'domId'         => 'picture',
                                    'nameHtml'      => 'picture',
                                    'multiple'      => 0,
                                    'browseOnZone'  => 0,
                                    'maxFile'       => 1,
                                ])
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Twitter Url") }}</label>
                                <input type="text" class="form-control" name="twitter_url" placeholder="{{ __("Enter twitter url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Facebook Url") }}</label>
                                <input type="text" class="form-control" name="facebook_url" placeholder="{{ __("Enter facebook url") }}">
                            </div>

                            <div class="form-group">
                                <label class="">{{ __("Youtube Url") }}</label>
                                <input type="text" class="form-control" name="youtube_url" placeholder="{{ __("Enter youtube url") }}">
                            </div>
                            <div class="form-group">
                                <label class="">{{ __("Instagram Url") }}</label>
                                <input type="text" class="form-control" name="instagram_url" placeholder="{{ __("Enter instagram url") }}">
                            </div>

                            <div class="form-group">
                                <label>{{ __("More options") }}</label>
                                <div class="checkbox-list">


                                    <label class="checkbox">
                                        <input type="checkbox" checked="checked" name="status">
                                        <span></span> {{ __("Active") }}
                                    </label>

                                    <label class="checkbox chk-continue">
                                        <input type="checkbox" name="continue">
                                        <span></span> {{ __("Continue add") }}
                                    </label>

                                </div>
                            </div>

                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        activeSummerNote(490, 'monokai','#content')
    });
</script>