<?php
$id = isset($user['id']) ? $user['id'] : '';
?>
<form method="post" id="form-access-menu" enctype="multipart/form-data">
    <input name="id" class="hide" value="{{ $userId }}">
    <div class="row">
        <?php
        foreach ($listModuleAccess as $key => $item){
        ?>
        <div class="col-md-6">
            <div class="mt-element-ribbon bg-grey-steel">
                <div class="ribbon ribbon-shadow ribbon-color-success uppercase main-bg-color">{{ $item['name'] }}</div>

                <div class="portlet light bg-inverse">

                    <div class="portlet-body">
                        @include("Admin.Elements.User.block_menu", [
                               'formId'     => "form-access-menu-".$item['id'],
                               'data'       => $item,
                               'roleId'     => -1,
                               'isAdmin'    => -1,
                         ])
                    </div>
                </div>


            </div>
        </div>
        <?php
        }
        ?>
    </div>

    <hr>

    <div class="form-actions text-center">
        <button type="submit" class="btn btn-primary btn-same-width btn-smooth btn-save-access-function" ><i class="far fa-save"></i> <?php echo __("Lưu thay đổi")?></button>
    </div>

</form>
