@extends("Layouts.metronic_v7")
@section('title', $title)
@section("content")
    <?php
        $id = isset($site['id']) ? $site['id'] : null;
        $introduction_title = isset($site['introduction_title']) ? $site['introduction_title'] : '';
        $introduction_subtitle = isset($site['introduction_subtitle']) ? $site['introduction_subtitle'] : '';
        $introduction_status = isset($site['introduction_status']) && $site['introduction_subtitle'] ? "checked" : '';
        $introduction_picture = isset($site['introduction_picture']) ? $site['introduction_picture'] : '';
        $picture_path = empty($introduction_picture) ? "" : url('uploads/Site/1/Introduction/'.$introduction_picture);
    ?>


    <?php $placeholder = __('Search by slug, name ...'); ?>

    <div class="row">
        <div class="col-md-12">

            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">{{ __("Introduction") }}</h3>
                    </div>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form-introduction" method="post" enctype="multipart/form-data">
                                <input type="text" class="form-control " value="{{ $id }}" name="id" hidden>


                                <div class="form-group">
                                    <label class="required_label">{{ __("Title") }}</label>
                                    <input type="text" class="form-control" value="{{ $introduction_title }}" name="introduction_title" placeholder="{{ __("Enter title") }}">
                                </div>

                                <div class="form-group">
                                    <label class="">{{ __("Subtitle") }}</label>
                                    <textarea rows="6" type="text" class="form-control" name="introduction_subtitle" placeholder="{{ __("Enter subtitle") }}">{{ $introduction_subtitle }}</textarea>
                                </div>



                                <div class="form-group">
                                    <label class="control-label">{{ __("Picture") }}</label>
                                    @include("Elements.file_input_upload", [
                                        'domId'         => 'introduction_picture',
                                        'nameHtml'      => 'introduction_picture',
                                        'multiple'      => 0,
                                        'browseOnZone'  => 1,
                                        'maxFile'       => 1,
                                        'filePath'      => $picture_path,
                                    ])
                                </div>



                                <div class="form-group">
                                    <label>{{ __("More options") }}</label>
                                    <div class="checkbox-list">


                                        <label class="checkbox">
                                            <input type="checkbox" checked="{{ $introduction_status }}" name="introduction_status">
                                            <span></span> {{ __("Active") }}
                                        </label>


                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <button type="submit" class="btn btn-primary font-weight-bold btn-act-lg"><i class="fas fa-save"></i> {{ __("Save") }}</button>
                                    <button type="button" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-times"></i> {{ __("Close") }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section("pagescript")
    <script src="{{asset('public/js/Admin/Site/site.js?t=1')}}"></script>
@endsection


