@extends("Layouts.metronic")
@section('title', $title)
@section("content")
    <?php

    ?>
    <style>
        .col-action-width {
            width: 70px;
            max-width: 70px;
        }

        .ng-input-button-container .ng-button {
            height: 34px;
        }

        .ng-button-icon-span {
            margin-top: 2px;
        }
    </style>

    <?php $placeholder = __('Enter code, name ...'); ?>
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <span class="caption-subject bold uppercase"><i class="fa fa-list"></i> {{ __($title) }} </span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default btn-add tooltips" title="{{ __("Add New") }}" href="javascript:;">
                    <i class="far fa-plus"></i>
                </a>



                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="{{ __('Fullscreen') }}" title=""> </a>
            </div>
        </div>
        <div class="portlet-body">
            @include("Elements.search_box", compact('placeholder'))
            <div class="row">
                <div class="col-sm-12 h-scroll">
                    <table id="searchTable" class="table table-bordered table-hover full-width">
                        <thead>
                        <tr>
                            <td class="col-no ">{{ __('No.') }}</td>
                            <td class="col-md-1">{{ __('Code') }}</td>
                            <td class="col-md-3">{{ __('Name') }}</td>
                            <td class="col-md-1 text-center">{{ __('Phone') }}</td>
                            <td class="col-md-2 text-center">{{ __('Contact Email') }}</td>
                            <td class="col-md-3">{{ __('Address') }}</td>
                            <td class="col-md-1 text-center">{{ __('Status') }}</td>
                            <td class="col-md-1"></td>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        var titleAdd = "{{ __('Add New') }}",
                titleEdit = "{{ __('Edit') }}";
    </script>

    @include("Elements.modal_confirm_delete")
    @include("Admin.Elements.Customer.form")
    @include("Elements.modal_waiting")
    <script src="{{asset('public/js/Admin/Customer/index.js?t='.time())}}"></script>

@endsection
