@extends("Layouts.".$template)
@section('title', "Test")
@section("content")

    <?php

    $id             = $classroom->id ?? -1;
    $course_id      = $classroom->course_id ?? -1;
    $course_name    = $classroom->name ?? "";
    $summary        = $classroom->summary ?? "";
    $picture        = $classroom->picture ?? "";
    $slot        = $classroom->slot ?? "";
    $free_slot        = $classroom->free_slot ?? "";

    $picture_path   = empty($picture) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$classroom->company_id.'/Course/'.$course_id."/Picture/".$picture.'?t='.SYSTEM_CACHE;

    $cover        = $classroom->cover ?? "";

    $cover_path   = empty($cover) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$classroom->company_id.'/Course/'.$course_id."/Cover/".$cover.'?t='.SYSTEM_CACHE;

    $arrDay = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    ?>

    <style>

        .table-classmate{
            width: 100%;

        }

        .table-classmate, .table-classmate > thead > tr > th, .table-classmate > tbody > tr > td{
            border: 1px solid #ddd;
            padding: 5px;
        }

        .col-no{
            width: calc((100%/12) * 1);
        }
        .col-classtime{
            width: calc((100%/12) * 6);
        }

        .col-slots{
            width: calc((100%/12) * 2);
        }

        .col-free-slot{
            width: calc((100%/12) * 2);
        }

        .col-action{
            width: calc((100%/12) * 1);
        }

        .btn-registration-course{
            border-radius: 4px !important;
            padding: 5px 10px;
            color: white !important;
        }

        .btn-registration-course:hover{
            color: #1eaaf1 !important;
        }

        .col-classrom-day{
            width: 60% !important;
        }

        .registration-area{
            border: 1px solid #ddd;
            padding: 15px;
            border-radius: 4px;
        }

        .registration-footer{
            padding-top: 10px;
            border-top: 1px solid #ddd;
            margin-top: 15px;
        }

    </style>

    <section class="hero-wrap hero-wrap-2" style="background-image: url({{ $cover_path }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">{{ $course_name }}</h1>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="{{ url("/") }}">{{ __("Home") }} <i class="ion-ios-arrow-forward"></i></a>
                        </span>
                        <span>{{ __("Course") }} </span>
                    </p>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <h2 class="mb-3">#1. {{ $course_name }}</h2>

                    <table>
                        <tr>
                            <td class="col-classrom-day">{{ __("Slots") }}</td>
                            <td class="col-classrom-time">: <span class="text-bold">{{ $free_slot.'/'.$slot }}</span></td>
                        </tr>
                    </table>

                    <p>{{ $summary }}</p>

                    <h2 class="mb-3">#2. {{ __("Class time") }}</h2>

                    <table>
                        <?php
                        foreach ($arrDay as $day){
                        if(!empty($classroom->{$day} && $classroom->{$day} !== "")){
                        ?>
                        <tr>
                            <td class="col-classrom-day">* {{ __(ucfirst($day)) }}</td>
                            <td class="col-classrom-time">: {{ $classroom->{$day} }}</td>
                        </tr>

                        <?php
                        }

                        }
                        ?>
                    </table>


                    <h2 class="mb-3 mt-3">#3. {{ __("Registration") }}</h2>

                    <div class="row">
                        <div class="col-md-12 registration-area">
                            <form id="form-registration" method="post" enctype="multipart/form-data">
                                <input type="text" class="form-control " name="class_room_id" value="{{ $id }}" hidden>
                                <input type="text" class="form-control " name="course_id" value="{{ $course_id }}" hidden>
                                <input type="text" class="form-control " name="_token" value="{{ csrf_token() }}" hidden>

                                <div class="form-group">
                                    <label class="required_label">{{ __("Fullname") }}</label>
                                    <input type="text" class="form-control" name="fullname" placeholder="{{ __("Enter fullname") }}">
                                </div>

                                <div class="form-group">
                                    <label class="required_label">{{ __("Phone") }}</label>
                                    <input type="text" class="form-control" name="phone" placeholder="{{ __("Enter phone") }}">
                                </div>

                                <div class="form-group">
                                    <label class="required_label">{{ __("Contact Email") }}</label>
                                    <input type="text" class="form-control" name="contact_email" placeholder="{{ __("Enter email") }}">
                                </div>

                                <div class="form-group">
                                    <label class="">{{ __("Message") }}</label>
                                    <textarea type="text" class="form-control" name="message" placeholder="{{ __("Enter message") }}"></textarea>
                                </div>

                                <div class="g-recaptcha" data-sitekey="6LcVRdwbAAAAAFpCz-A1kwIY4xxPhgoWVQhhJCTd"></div>

                                <div class=" text-center registration-footer">
                                    <button type="submit"
                                            class="btn btn-primary font-weight-bold btn-act-lg "
                                    >
                                        <i class="fas fa-file-signature"></i> {{ __("Registration") }}
                                    </button>
                                    <button type="reset" class="btn btn-default font-weight-bold btn-act-lg" data-dismiss="modal"><i class="fas fa-sync"></i> {{ __("Refresh") }}</button>
                                </div>
                            </form>
                        </div>
                    </div>



                    <div class="tag-widget post-tag-container mb-5 mt-5">
                        <div class="tagcloud">
                            <div class="fb-comments" data-href="{{ $actual_link }}" data-width="100%" data-numposts="5"></div>
                        </div>
                    </div>


                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar ftco-animate">

                    @include('Elements.Course.popular', compact('company'))


                </div><!-- END COL -->
            </div>
        </div>
    </section>



@endsection

@section("pagescript")
    <script src="{{asset('public/js/Course/index.js?t=2')}}"></script>
@endsection


