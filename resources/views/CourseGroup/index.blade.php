@extends("Layouts.".$template)
@section('title', "Test")
@section("content")

    <?php

    $label = isset($site_setting->course_label) ? $site_setting->course_label : "";
    $content = isset($site_setting->course_content) ? $site_setting->course_content : "";
    $picture = isset($site_setting->course_picture) ? $site_setting->course_picture : "";
    $picture_path = empty($picture) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.MASTER_COMPANY_ID.'/SiteSetting/CoursePicture/'.$picture.'?t='.SYSTEM_CACHE;


    ?>


    <section class="hero-wrap hero-wrap-2" style="background-image: url({{ $picture_path }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">{{ $label }}</h1>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="{{ url("/") }}">{{ __("Home") }} <i class="ion-ios-arrow-forward"></i></a>
                        </span>
                        <span>{{ __("Courses") }} </span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row">

                @foreach($listCourse as $key => $item)

                    <?php
                        $id         = $item->id;
                        $name       = $item->name;
                        $slug       = $item->slug;
                        $class_time = $item->class_time;
                        $summary    = $item->summary;
                        $picture    = $item->picture;

                        $course_path = url("courses/".$slug);
                        $picture_path = empty($picture) ? $current_domain.'/images/no_img.jpg' :
                            $current_domain.'/uploads/'.$item->company_id.'/Course/'.$id."/Picture/".$picture.'?t='.SYSTEM_CACHE;
                    ?>

                    <div class="col-md-6 course d-lg-flex ftco-animate">
                        <div class="img" style="background-image: url({{ $picture_path }});"></div>
                        <div class="text bg-light p-4">
                            <h3><a href="{{ $course_path }}" target="_blank">{{ $name }}</a></h3>
                            <p class="subheading"><span>{{ __("Class time") }}:</span> {{ $class_time }}</p>
                            <p style="text-align: justify;">{{ $summary }}</p>
                        </div>
                    </div>

                @endforeach


            </div>
        </div>
    </section>




@endsection

@section("pagescript")

@endsection


