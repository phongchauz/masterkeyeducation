<?php new \App\Helpers\BaseService; ?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="vi">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include('Elements.meta_data', compact('company'))
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{  asset('public/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{  asset('public/css/animate.css') }}">
    <link rel="stylesheet" href="{{  asset('public/css/font-awesome.css') }}">
{{--    <link rel="stylesheet" href="{{  asset('public/css/fileinput.css') }}">--}}
    <link rel="stylesheet" href="{{  asset('public/css/Admin/admin.css?t='.time()) }}">
    <link rel="stylesheet" href="{{  asset('public/css/Admin/menu-icon.css?t='.time()) }}">
    <link rel="stylesheet" href="{{  asset('public/css/bootstrapselect/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{  asset('public/css/bootstrapselect/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{  asset('public/css/common.css?t='.time()) }}">

    <script src="{{  asset('public/js/jquery.js') }}"></script>
    <script src="{{  asset('public/js/bootstrap.js') }}"></script>
    <script src="{{  asset('public/js/common.js?t='.time()) }}"></script>
    <script src="{{  asset('public/js/active-select.js') }}"></script>
{{--    <script src="{{  asset('public/js/fileinput.js') }}"></script>--}}
    <script src="{{  asset('public/js/bootstrap-select.js') }}"></script>
    <script src="{{  asset('public/js/bootstrap-notify.js') }}"></script>
    <script src="{{  asset('public/js/slide-message.js') }}"></script>

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{  asset('public/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />

    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{  asset('public/assets/templates/metronic/assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('public/assets/templates/metronic/assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{  asset('public/assets/templates/metronic/assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    {{--DATETIMEPICKER--}}
    <script src="{{  asset('public/assets/global/plugins/moment.min.js') }}"></script>
    <link rel="stylesheet" href="{{  asset('public/assets/global/plugins/bootstrap-datetimepicker-2/css/bootstrap-datetimepicker.min.css') }}">
    <script src="{{  asset('public/assets/global/plugins/bootstrap-datetimepicker-2/js/bootstrap-datetimepicker.min.js') }}"></script>
    {{--END DATETIMEPICKER--}}

    {{--FILE INPUT--}}
    <script src="{{  asset('public/assets/file-input/fileinput.min.js') }}"></script>
    <link rel="stylesheet" href="{{  asset('public/assets/file-input/fileinput.min.css') }}">
    {{--END FILE INPUT--}}

    {{--SWIPER--}}
    <script src="{{  asset('public/assets/swiper/swiper.min.js') }}"></script>
    <link rel="stylesheet" href="{{  asset('public/assets/swiper/swiper.min.css') }}">
    {{--END SWIPER--}}

    {{--JSTREE--}}
    <link href="{{  asset('public/assets/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{  asset('public/assets/jstree/dist/jstree.js') }}"></script>
    {{--END JSTREE--}}

    <link href="{{ asset('public/assets/global/plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('public/assets/global/plugins/summernote/summernote.js') }}"></script>
    <script src="{{  asset('public/js/active-summernote.js?t='.time()) }}"></script>

    <script src="{{ asset('public/assets/global/plugins/jquery-number/jquery.number.js?t='.time()) }}"></script>

    <script src="{{ asset('public/js/highcharts/highcharts.js?t='.time()) }}"></script>
    <script src="{{ asset('public/js/highcharts/accessibility.js?t='.time()) }}"></script>
    <script src="{{ asset('public/js/highcharts/export-data.js?t='.time()) }}"></script>
    <script src="{{ asset('public/js/highcharts/exporting.js?t='.time()) }}"></script>
    <script src="{{ asset('public/js/highcharts/highcharts-more.js?t='.time()) }}"></script>

    <style>


        .input-icon>i, .mt-checkbox-list .mt-checkbox, .mt-checkbox-list .mt-radio, .mt-checkbox>input:checked~span:after, .mt-radio-list .mt-checkbox, .mt-radio-list .mt-radio, .mt-radio>input:checked~span:after {
            display: block;
        }


        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user>.dropdown-menu {
            margin-top: -2px;
        }

        table.dataTable {
            width: 100% !important;
        }

        table.dataTable > thead > tr {
            background-color: #D5D8DC;
        }

        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user .dropdown-toggle>img {
            width: 29px;
        }
        .loading-cover{
            z-index: 99999 !important;
        }
        /* set space of sub left menu */
        .page-sidebar .page-sidebar-menu .sub-menu li>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu li>a {
            padding: 6px 5px 6px 25px;
        }

        tr.odd td:first-child,
        tr.even td:first-child {
            /*padding-left: 4em;*/
        }
    </style>

    <style>
        .last-menu > a{
            border-bottom: 1px solid #3d4957 !important;
        }

        .note-editor .btn-group .btn {
            border-radius: 0px !important;
        }

    </style>

    <script>
        APP.API_URL = "{!! url('/').'/' !!}";
        var _token = "{{ csrf_token() }}";
    </script>

    @include('Elements.include_variables')

    <link rel="stylesheet" type="text/css" href="{{  asset('public/assets/global/plugins/bootstrap-table/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/js/datatables/rowGroup.dataTables.min.css')}}"/>
    <script type="text/javascript" src="{{  asset('public/assets/global/plugins/bootstrap-table/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('public/js/datatables/dataTables.rowGroup.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset("public/assets/global/plugins/font-awesome/css/font-awesome.css?t=v2") }}">
    <link rel="stylesheet" href="{{ asset('public/js/fontawesome/css/all.css?t=1') }}" type="text/css" >
    <script type="text/javascript" src="{{  asset('public/js/fontawesome/js/all.js') }}"></script>


</head>
<!-- END HEAD -->

<?php
//    $fullscreen = isset($currentFunction->is_fullscreen) ? $currentFunction->is_fullscreen : 0;
//    $clsFullscreen = $fullscreen ? "page-sidebar-closed" : "";

use Illuminate\Support\Facades\URL;

    $arrCurrentUrl = explode('/',  URL::current());


    $clsFullscreen =  "";
    $clsPageBgSolid = "page-container-bg-solid";

    if(isset($arrCurrentUrl[4]) && $arrCurrentUrl[4] == 'dashboard'){
        $clsPageBgSolid = "";
    }

?>

<body class="page-header-fixed page-sidebar-closed-hide-logo {{ $clsPageBgSolid }}  page-content-white {{ $clsFullscreen }}">

<!-- BEGIN HEADER -->
@include('layouts.metronic.header', compact('company'))
        <!-- END HEADER -->

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    @include('layouts.metronic.menu')
            <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @yield('content')
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
@include('layouts.metronic.footer', compact('company'))
        <!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="{{  asset('public/assets/global/plugins/respond.min.js') }}"></script>
<script src="{{  asset('public/assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{  asset('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{  asset('public/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{--<script src="{{ asset('public/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('public/assets/templates/metronic/assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/templates/metronic/assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/templates/metronic/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<link rel="stylesheet" href="{{ asset('public/assets/global/plugins/jquery-ui/') }}/jquery-ui.min.css">
<script src="{{ asset('public/assets/global/plugins/jquery-ui/') }}/jquery-ui.min.js"></script>
<script src="{{  asset('public/js/socket.io.js') }}"></script>

<script type="text/javascript">

    $(function () {
        $(".tooltips").tooltip();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    $(document).ready(function(){
        activeSelect('.combobox-search', true, 5);
        $( ".input-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy'});
    });


</script>



</body>

</html>
