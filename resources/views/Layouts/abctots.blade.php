<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Home 2 &#8211; Abc Tots &#8211; Kindergarten WordPress Theme</title>
    <meta name='robots' content='noindex, nofollow'/>
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel="alternate" type="application/rss+xml" title="Abc Tots - Kindergarten WordPress Theme &raquo; Feed"
          href="https://klbtheme.com/abctots/feed/"/>
    <link rel="alternate" type="application/rss+xml"
          title="Abc Tots - Kindergarten WordPress Theme &raquo; Comments Feed"
          href="https://klbtheme.com/abctots/comments/feed/"/>
    <link rel="stylesheet" type="text/css"
          href="//klbtheme.com/abctots/wp-content/cache/wpfc-minified/er2z889j/8eu73.css" media="all"/>
    <link rel="https://api.w.org/" href="https://klbtheme.com/abctots/wp-json/"/>
    <link rel="alternate" type="application/json" href="https://klbtheme.com/abctots/wp-json/wp/v2/pages/527"/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://klbtheme.com/abctots/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://klbtheme.com/abctots/wp-includes/wlwmanifest.xml"/>
    <link rel="canonical" href="https://klbtheme.com/abctots/home-2/"/>
    <link rel='shortlink' href='https://klbtheme.com/abctots/?p=527'/>
    <link rel="alternate" type="application/json+oembed"
          href="https://klbtheme.com/abctots/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fklbtheme.com%2Fabctots%2Fhome-2%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="https://klbtheme.com/abctots/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fklbtheme.com%2Fabctots%2Fhome-2%2F&#038;format=xml"/>

    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <link rel="icon" href="https://klbtheme.com/abctots/wp-content/uploads/2019/08/cropped-logo-32x32.png"
          sizes="32x32"/>
    <link rel="icon" href="https://klbtheme.com/abctots/wp-content/uploads/2019/08/cropped-logo-192x192.png"
          sizes="192x192"/>
    <link rel="apple-touch-icon"
          href="https://klbtheme.com/abctots/wp-content/uploads/2019/08/cropped-logo-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="https://klbtheme.com/abctots/wp-content/uploads/2019/08/cropped-logo-270x270.png"/>
    <style id="wp-custom-css">.klbfooterwidget {
            text-align: center;
        }</style>
    <style id="kirki-inline-styles"></style>
    <style data-type="vc_shortcodes-custom-css">.vc_custom_1565677219711 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
            background-color: #035392 !important;
        }

        .vc_custom_1566196899688 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
        }

        .vc_custom_1566291336504 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
            background-image: url(https://klbtheme.com/abctots/wp-content/uploads/2019/08/counterbg.jpg?id=107) !important;
        }

        .vc_custom_1567416424333 {
            padding-top: 90px !important;
        }

        .vc_custom_1567416424334 {
            padding-top: 40px !important;
        }

        .vc_custom_1567416446157 {
            padding-bottom: 90px !important;
        }

        .vc_custom_1567416446158 {
            padding-bottom: 40px !important;
        }

        .vc_custom_1567234489819 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
            background-color: #e4a40f !important;
        }

        .vc_custom_1567234489820 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1567239562933 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
        }

        .vc_custom_1567239562935 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1567410534091 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
            background-image: url(https://klbtheme.com/abctots/wp-content/uploads/2019/08/callout.jpg?id=183) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1567410534092 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1567846584725 {
            padding-top: 90px !important;
            padding-bottom: 48px !important;
        }

        .vc_custom_1567846584726 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1567926241287 {
            margin-bottom: 90px !important;
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1567926241288 {
            margin-bottom: 40px !important;
        }

        .vc_custom_1565419702918 {
            margin-bottom: 90px !important;
        }

        .vc_custom_1565418497379 {
            margin-top: 15px !important;
        }

        .vc_custom_1566196775283 {
            background-image: url(https://klbtheme.com/abctots/wp-content/uploads/2019/08/childrenbg.png?id=70) !important;
            background-position: 0 0 !important;
            background-repeat: no-repeat !important;
        }

        .vc_custom_1565761746493 {
            margin-top: 30px !important;
        }

        .vc_custom_1565761752411 {
            margin-top: 30px !important;
        }

        .vc_custom_1566284468685 {
            padding-top: 56px !important;
            padding-right: 30px !important;
            padding-bottom: 56px !important;
            padding-left: 30px !important;
        }

        .vc_custom_1566288280357 {
            margin-bottom: 10px !important;
        }

        .vc_custom_1566284468685 {
            padding-top: 56px !important;
            padding-right: 30px !important;
            padding-bottom: 56px !important;
            padding-left: 30px !important;
        }

        .vc_custom_1566288715906 {
            margin-bottom: 10px !important;
        }

        .vc_custom_1566284468685 {
            padding-top: 56px !important;
            padding-right: 30px !important;
            padding-bottom: 56px !important;
            padding-left: 30px !important;
        }

        .vc_custom_1566289422623 {
            margin-bottom: 10px !important;
        }

        .vc_custom_1566284468685 {
            padding-top: 56px !important;
            padding-right: 30px !important;
            padding-bottom: 56px !important;
            padding-left: 30px !important;
        }

        .vc_custom_1566289527525 {
            margin-bottom: 10px !important;
        }

        .vc_custom_1566284468685 {
            padding-top: 56px !important;
            padding-right: 30px !important;
            padding-bottom: 56px !important;
            padding-left: 30px !important;
        }

        .vc_custom_1566289661320 {
            margin-bottom: 10px !important;
        }</style>
    <noscript>
        <style>.wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>
<body id="top" class="page-template-default page page-id-527 wpb-js-composer js-comp-ver-6.6.0 vc_responsive">
<div id="preloader">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="preloader-logo">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav id="main-nav" class="navbar-expand-xl fixed-top">
    <div class="row">
        <div class="container-fluid top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="contact-details float-left">
                            <li><a href="#"><i class="fa fa-map-marker"></i>Street name 123 - New York</a></li>
                            <li><a href="mailto:email@yoursite.com"><i class="fa fa-envelope"></i>email@yoursite.com</a>
                            </li>
                            <li><a href="el:+491234567891"><i class="fa fa-phone"></i>(123) 456-789</a></li>
                        </ul>
                        <ul class="social-list float-right list-inline">
                            <li class="list-inline-item"><a title="facebook-f" href="#"><i
                                            class="fab fa-facebook-f"></i></a></li>
                            <li class="list-inline-item"><a title="twitter" href="https://twitter.com/"><i
                                            class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a title="instagram" href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar container-fluid">
            <div class="container">
                <a class="nav-brand text" href="https://klbtheme.com/abctots/"
                   title="Abc Tots &#8211; Kindergarten WordPress Theme">
                    <img src="https://klbtheme.com/abctots/wp-content/uploads/2019/08/logo.png" height="47" width="220"
                         alt="Abc Tots &#8211; Kindergarten WordPress Theme" class="img-fluid">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon"><i class="fas fa-bars"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="menu-menu-1" class="navbar-nav ml-auto">
                        <li class="nav-item menu-item menu-item-type-custom menu-item-object-custom menu-item-home"><a
                                    class="nav-link" href="https://klbtheme.com/abctots/">Home</a></li>
                        <li class="nav-item dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                            <a class="nav-link dropdown-toggle" href="#">Services</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/services-1/">Services
                                        Style 1</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/services-2/">Services
                                        Style 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                            <a class="nav-link dropdown-toggle" href="#">About</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/about-1/">About
                                        Style 1</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/about-2/">About
                                        Style 2</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/team/">Our Team</a>
                                </li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item"
                                            href="https://klbtheme.com/abctots/careers/">Careers</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item"
                                            href="https://klbtheme.com/abctots/pricing/">Pricing</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                            <a class="nav-link dropdown-toggle" href="#">Gallery</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/gallery-1/">Gallery
                                        1</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/gallery-2/">Gallery
                                        2</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/gallery-3/">Gallery
                                        3</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                            <a class="nav-link dropdown-toggle" href="#">Contact</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/contact-1/">Contact
                                        1</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/contact-2/">Contact
                                        2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                            <a class="nav-link dropdown-toggle" href="#">Blog</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a
                                            class="dropdown-item" href="https://klbtheme.com/abctots/blog/">Blog
                                        List</a></li>
                                <li class="nav-item menu-item menu-item-type-post_type menu-item-object-post"><a
                                            class="dropdown-item"
                                            href="https://klbtheme.com/abctots/2019/08/31/20-fun-activities-to-do-with-your-kids/">Blog
                                        Detail</a></li>
                                <li class="nav-item menu-item menu-item-type-custom menu-item-object-custom"><a
                                            class="dropdown-item" href="http://klbtheme.com/abctots/404">404 Page</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>

<div id="page-wrapper">
    @yield('content')
</div>

<svg version="1.1" id="footer-divider" class="secondary" xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1440 126" xml:space="preserve" preserveAspectRatio="none slice">
<path class="st0" d="M685.6,38.8C418.7-11.1,170.2,9.9,0,30v96h1440V30C1252.7,52.2,1010,99.4,685.6,38.8z"/>
</svg>
<footer class="bg-secondary text-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="klbfooterwidget widget_text"><h5>Working Hours</h5>
                    <div class="textwidget">
                        <ul class="list-unstyled mt-3">
                            <li class="mb-1">Monday to Friday</li>
                            <li class="mb-1">Open from 9am &#8211; 6pm</li>
                            <li class="mb-1">Holidays/Weekends &#8211; Closed</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 res-margin">
                <div class="klbfooterwidget footer-column widget_text">
                    <div class="textwidget"><p><img loading="lazy" class="alignnone size-full wp-image-479"
                                                    src="https://klbtheme.com/abctots/wp-content/uploads/2019/10/logo_light.png"
                                                    alt="" width="210" height="45"/></p>
                        <p class="mt-4">Aliquam erat volutpat Aliquam erat volutpat In id fermentum augue, lorem ut
                            pellentesque leo. Maecenas at arcu risus.</p>
                        <ul class="social-list text-center list-inline mt-2">
                            <li class="list-inline-item"><a title="Facebook" href="#"><i class="fab fa-facebook-f"></i>facebook</a>
                            </li>
                            <li class="list-inline-item"><a title="Twitter" href="#"><i class="fab fa-twitter"></i>twitter</a>
                            </li>
                            <li class="list-inline-item"><a title="Instagram" href="#"><i class="fab fa-instagram"></i>instagram</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="klbfooterwidget footer-column widget_text"><h5>Contact Us</h5>
                    <div class="textwidget">
                        <ul class="list-unstyled mt-3">
                            <li class="mb-1"><i class="fas fa-phone margin-icon"></i>(123) 456-789</li>
                            <li class="mb-1"><i class="fas fa-envelope margin-icon"></i><a
                                        href="mailto:email@yoursite.com">email@klbtheme.com</a></li>
                            <li><i class="fas fa-map-marker margin-icon"></i>Street Name 123 &#8211; New York</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="credits col-sm-12"><p>Copyright 2021.KlbTheme . All rights reserved.</p></div>
        </div>
    </div>
    <div class="page-scroll hidden-sm hidden-xs"><a href="#top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
    </div>
</footer>
<style id='klbmobile-inline-css'>
    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row .klb_xs_vc_custom_1565419702918 {
            margin-bottom: 90px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567416424334 {
            padding-top: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567416446158 {
            padding-bottom: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567234489820 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567239562935 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567410534092 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567846584726 {
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }
    }

    @media (max-width: 768px) and (min-width: 320px) {
        .vc_row.klb_xs_vc_custom_1567926241288 {
            margin-bottom: 40px !important;
        }
    }</style>
<link rel="stylesheet" type="text/css" href="//klbtheme.com/abctots/wp-content/cache/wpfc-minified/ew0pssb/8eu73.css"
      media="all"/>
<noscript id="wpfc-google-fonts">
    <link rel='stylesheet' id='abctots-font-teko-css'
          href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700&#038;subset=latin,latin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='abctots-font-opensans-css'
          href='//fonts.googleapis.com/css?family=Nunito:400,700,900&#038;subset=latin,latin-ext' type='text/css'
          media='all'/>
</noscript>

<script id='contact-form-7-js-extra'>var wpcf7 = [];</script>
<script src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/kqdqqr9f/8eu73.js'></script>
<script type='text/javascript' id='wp-polyfill-js-after'>
    ('fetch' in window) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>');
    (document.contains) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.42.0"></scr' + 'ipt>');
    (window.DOMRect) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min.js?ver=3.42.0"></scr' + 'ipt>');
    (window.URL && window.URL.prototype && window.URLSearchParams) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-url.min.js?ver=3.6.4"></scr' + 'ipt>');
    (window.FormData && window.FormData.prototype.keys) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>');
    (Element.prototype.matches && Element.prototype.closest) || document.write('<script src="https://klbtheme.com/abctots/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js?ver=2.0.2"></scr' + 'ipt>');
    ('objectFit' in document.documentElement.style) || document.write('<script src="//klbtheme.com/abctots/wp-content/cache/wpfc-minified/o7fv071/4e9vx.js"></scr' + 'ipt>');
</script>
<script src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/7cagvqsn/4e9vx.js'></script>
<script id='wp-i18n-js-after'>wp.i18n.setLocaleData({'text direction\u0004ltr': ['ltr']});</script>
<script src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/6y4mbouo/4e9vx.js'></script>
<script id='lodash-js-after'>window.lodash = _.noConflict();</script>
<script src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/7jcfq9w3/4e9vx.js'></script>
<script id='wp-api-fetch-js-translations'>(function (domain, translations) {
        var localeData = translations.locale_data[domain] || translations.locale_data.messages;
        localeData[""].domain = domain;
        wp.i18n.setLocaleData(localeData, domain);
    })("default", {"locale_data": {"messages": {"": {}}}});</script>
<script src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/6n0d4crs/8eu73.js'></script>
<script id='wp-api-fetch-js-after'>wp.apiFetch.use(wp.apiFetch.createRootURLMiddleware("https://klbtheme.com/abctots/wp-json/"));
    wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware("64f26fb589");
    wp.apiFetch.use(wp.apiFetch.nonceMiddleware);
    wp.apiFetch.use(wp.apiFetch.mediaUploadMiddleware);
    wp.apiFetch.nonceEndpoint = "https://klbtheme.com/abctots/wp-admin/admin-ajax.php?action=rest-nonce";</script>
<script defer src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/8w2dary7/8eu73.js'></script>
<script defer
        src='https://klbtheme.com/abctots/wp-content/themes/abctots/vendor/layerslider/js/layerslider.load.js?ver=1.0'
        id='layerslider-load-js'></script>
<script defer src='//klbtheme.com/abctots/wp-content/cache/wpfc-minified/9igxdm1v/8eu73.js'></script>
<script>(function jqIsReady_261() {
        if (typeof jQuery === "undefined") {
            jqIsReady_261();
        } else {
            jQuery(document).ready(function ($) {
                "use strict";
                $('#slider').layerSlider({
                    responsive: true,
                    fitScreenWidth: false,
                    responsiveUnder: 1280,
                    layersContainer: 1280,
                    skin: 'outline',
                    hoverPrevNext: true,
                    skinsPath: 'https://klbtheme.com/abctots/wp-content/themes/abctots/vendor/layerslider/skins/',
                    autoStart: true,
                    autoPlayVideos: false,
                    showCircleTimer: false
                });
            });
        }
    })();</script>
<script>document.addEventListener('DOMContentLoaded', function () {
        function wpfcgl() {
            var wgh = document.querySelector('noscript#wpfc-google-fonts').innerText,
                wgha = wgh.match(/<link[^\>]+>/gi);
            for (i = 0; i < wgha.length; i++) {
                var wrpr = document.createElement('div');
                wrpr.innerHTML = wgha[i];
                document.body.appendChild(wrpr.firstChild);
            }
        }

        wpfcgl();
    });</script>
</body>
</html><!-- WP Fastest Cache file was created in 0.36404299736023 seconds, on 02-04-21 6:34:21 -->