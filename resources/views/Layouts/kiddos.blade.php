<!DOCTYPE html>
<html lang="en">
<head>


    @include('Elements.meta_data', compact('company'))

    <link href="https://fonts.googleapis.com/css?family=Spectral+SC:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/animate.css">

    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/magnific-popup.css">

    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/aos.css">

    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/ionicons.min.css">

    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/flaticon.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/icomoon.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/style.css">
    <link rel="stylesheet" href="{{  asset('public/assets/templates/kiddos/') }}/css/custom.css?t={{ SYSTEM_CACHE }}">
    <link rel="stylesheet" href="{{  asset('public/css/') }}/custom.css?t={{ SYSTEM_CACHE }}">

    <link rel="stylesheet" href="{{ asset('public/js/fontawesome/css/all.css?t=1') }}" type="text/css" >
    <script type="text/javascript" src="{{  asset('public/js/fontawesome/js/all.js') }}"></script>

{{--    <script src="{{  asset('public/js/jquery.js') }}"></script>--}}

    <script>
        var _token = "{{ csrf_token() }}";
        var APP = {};
        APP.API_URL = "{!! url('/').'/' !!}";
    </script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    @include('Elements.include_variables')

    <style>

        body {
            font-family: 'Spectral SC', serif;
        }

        .fb_iframe_widget iframe {
            width: 100% !important;
        }
    </style>

</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=1201072193706829&autoLogAppEvents=1" nonce="NaWBwpsH"></script>
@include("Layouts.kiddos.header_contact")

@include("Layouts.kiddos.top_menu")


<!-- END nav -->

@yield('content')


@include("Layouts.kiddos.footer")





<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.min.js"></script>
{{--<script src="{{  asset('public/assets/templates/kiddos/') }}/js/bootstrap.min.js"></script>--}}

<script src="{{  asset('public/js/bootstrap.js') }}"></script>
<script src="{{  asset('public/js/common.js?t='.time()) }}"></script>
<script src="{{  asset('public/js/active-select.js') }}"></script>
<script src="{{  asset('public/js/bootstrap-notify.js') }}"></script>
<script src="{{  asset('public/js/slide-message.js') }}"></script>
<script src="{{  asset('public/js/home.js') }}"></script>


<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery-migrate-3.0.1.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/popper.min.js"></script>

<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.easing.1.3.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.waypoints.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.stellar.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/owl.carousel.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.magnific-popup.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/aos.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/jquery.animateNumber.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/scrollax.min.js"></script>
<script src="{{  asset('public/assets/templates/kiddos/') }}/js/main.js"></script>

@yield('pagescript')

</body>
</html>