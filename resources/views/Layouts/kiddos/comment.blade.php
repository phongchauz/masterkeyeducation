<?php
use App\Helpers\BaseService;
$listData = $listData ?? [];
$comment_label = isset($site_setting->comment_label) ? $site_setting->comment_label : "";
$comment_content = isset($site_setting->comment_content) ? $site_setting->comment_content : "";
$prev_label = BaseService::splitString($comment_label, 'prev');
$last_label = trim(str_replace($prev_label, '', $comment_label));
?>

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <h2 class="mb-4"><span>{{ $prev_label }}</span> {{ $last_label }}</h2>
                <p><?php echo nl2br($comment_content); ?></p>
            </div>
        </div>
        <div class="row ftco-animate justify-content-center">
            <div class="col-md-12">
                <div class="carousel-testimony owl-carousel">

                    @foreach($listData as $key => $item)

                        <?php
                        $id = $item->id;
                        $name = $item->name;
                        $relationship = $item->relationship;
                        $content = $item->content;


                        $picture = $item->picture;
                        $picture_path = empty($picture) ? asset('public/images/kiddos/comment_noimg.jpg'):
                            asset('public/uploads/'.$item->company_id.'/Comment/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);

                        ?>

                            <div class="item">
                                <div class="testimony-wrap d-flex">
                                    <div class="user-img mr-4" style="background-image: url({{  $picture_path }})">
                                    </div>
                                    <div class="text ml-2 bg-light">
                                        <span class="quote d-flex align-items-center justify-content-center">
                                          <i class="icon-quote-left"></i>
                                        </span>
                                        <p>{{ $content }}</p>
                                        <p class="name">{{ $name }}</p>
                                        <span class="position">{{ $relationship }}</span>
                                    </div>
                                </div>
                            </div>

                    @endforeach




                </div>
            </div>
        </div>
    </div>
</section>