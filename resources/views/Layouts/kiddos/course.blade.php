<?php
use App\Helpers\BaseService;
$listData = $listData ?? [];
$course_label = isset($site_setting->course_label) ? $site_setting->course_label : "";
$course_content = isset($site_setting->course_content) ? $site_setting->course_content : "";

$prev_label = BaseService::splitString($course_label, 'prev');
$last_label = trim(str_replace($prev_label, '', $course_label));
?>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-12 text-center heading-section ftco-animate">
                <h2 class="mb-4"><span>{{ $prev_label }}</span> {{ $last_label }}</h2>
                <p><?php echo nl2br($course_content);?></p>
            </div>
        </div>
        <div class="row">

            @foreach($listData as $key => $item)

                <?php
                $id = $item->id;
                $name = $item->name;
                $slug = $item->slug;
//                $class_time = $item->class_time;
                $summary = $item->summary;

                $picture = $item->picture;
                $picture_path = empty($picture) ? asset('public/images/kiddos/course_noimg.jpg'):
                    asset('public/uploads/'.$item->company_id.'/CourseGroup/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);

                $course_url = url('course-group/'.$slug);

                ?>

                    <div class="col-md-6 course d-lg-flex ftco-animate">
                        <div class="img" style="background-image: url({{  $picture_path }});"></div>
                        <div class="text bg-light p-4">
                            <h3><a href="{{ $course_url }}" target="_blank">{{ $name }}</a></h3>
                            <p>{{ $summary }}</p>
                        </div>
                    </div>

            @endforeach



        </div>
    </div>
</section>