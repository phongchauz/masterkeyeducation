<section class="ftco-gallery">
    <div class="container-wrap">
        <div class="row no-gutters">

            @foreach($listData as $key => $item)
                <?php
                    $picture        = $item->picture;
                    $picture_path   =  $current_domain.'/uploads/'.$item->company_id."/Galleries/".$item->id.'/'.$picture.'?t='.SYSTEM_CACHE;
                ?>
                    <div class="col-md-3 ftco-animate">
                        <a href="{{  $picture_path }}" class="gallery image-popup img d-flex align-items-center"
                           style="background-image: url({{  $picture_path }});">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                                <span class="icon-instagram"></span>
                            </div>
                        </a>
                    </div>
            @endforeach
        </div>
    </div>
</section>