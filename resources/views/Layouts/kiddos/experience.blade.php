<?php
use App\Helpers\BaseService;
$listData = $listData ?? [];
$experience_label = isset($site_setting->experience_label) ? $site_setting->experience_label : "";
$experience_content = isset($site_setting->experience_content) ? $site_setting->experience_content : "";
$experience_picture = isset($site_setting->experience_picture) ? $site_setting->experience_picture : "";
$experience_picture_path = empty($experience_picture) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$company_id.'/SiteSetting/ExperiencePicture/'.$experience_picture.'?t='.SYSTEM_CACHE;

$prev_label = BaseService::splitString($experience_label, 'prev');
$last_label = trim(str_replace($prev_label, '', $experience_label));

$total_teacher = count($listTeacher);
$total_course = count($listCourse);
$total_student = isset($listStudent) ? count($listStudent) : 0;
$total_comment = isset($listComment) ? count($listComment) : 0;

?>
<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url({{  $experience_picture_path }});" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section heading-section-black ftco-animate">
                <h2 class="mb-4"><span>{{ $prev_label }}</span> {{ $last_label }}</h2>
                <p>{{ $experience_content }}</p>
            </div>
        </div>
        <div class="row d-md-flex align-items-center justify-content-center">
            <div class="col-lg-10">
                <div class="row d-md-flex align-items-center">
                    <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="icon"><span class="flaticon-doctor"></span></div>
                            <div class="text">
                                <strong class="number" data-number="{{ $total_teacher }}">0</strong>
                                <span>{{ __("Certified Teachers") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="icon"><span class="flaticon-doctor"></span></div>
                            <div class="text">
                                <strong class="number" data-number="{{ $total_student }}">0</strong>
                                <span>{{ __("Successful Kids") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="icon"><span class="flaticon-doctor"></span></div>
                            <div class="text">
                                <strong class="number" data-number="{{ $total_course }}">0</strong>
                                <span>{{ __("Courses") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="icon"><span class="flaticon-doctor"></span></div>
                            <div class="text">
                                <strong class="number" data-number="{{ $total_comment }}">0</strong>
                                <span>{{ __('Happy Parents') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>