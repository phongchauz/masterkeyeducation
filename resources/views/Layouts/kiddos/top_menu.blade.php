
<?php
$name = $company->name ?? "";

$listMenu = [
    ['id' => 1, "page" => "home", "name" => "Home", "url" => "/"],
//    ['id' => 2, "page" => "about", "name" => "About", "url" => "about-us"],
    ['id' => 3, "page" => "teacher", "name" => "Teacher", "url" => "teachers"],
    ['id' => 4, "page" => "course", "name" => "Courses", "url" => "courses"],
//    ['id' => 5, "page" => "pricing", "name" => "Pricing", "url" => "pricing"],
//    ['id' => 6, "page" => "blog", "name" => "Blog", "url" => "blog"],
    ['id' => 7, "page" => "contact", "name" => "Contact", "url" => "contact-us"],

];

$id             = $company["id"] ?? "-1";
$name           = $company["name"] ?? "";
$logo           = $company['logo'] ?? '';
$logo_title     = $company['logo_title'] ?? '';
$logo_alt       = $company['logo_alt'] ?? '';
$logo_path      = empty($logo) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$id.'/Logo/'.$logo.'?t='.SYSTEM_CACHE;
?>

<style>
    .logo-site{
        height: 80px;
    }
</style>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco_navbar ftco-navbar-light" id="ftco-navbar">
    <div class="container d-flex align-items-center">
        <a class="navbar-brand" href="{{ url("/") }}">
            <img class="logo-site" src="{{ $logo_path }}" title="{{ $logo_title }}" alt="{{ $logo_alt }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="{{ __("Toggle navigation") }}">
            <span class="oi oi-menu"></span> {{ __("Menu") }}
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">

                @foreach($listMenu as $key => $item)
                    <?php
                        $active = isset($current_page) && $item["page"] == $current_page ? "active" : "";
                        $a_cls = $key ? "" : "pl-0";
                    ?>
                    <li class="nav-item {{ $active }}">
                        <a href="{{ url($item["url"]) }}" class="nav-link {{ $a_cls }}">{{ __($item["name"]) }}</a>
                    </li>

                @endforeach


                <li class="nav-item"><a href="{{ url("login") }}" class="nav-link">{{ __("Login") }}</a></li>
            </ul>
        </div>
    </div>
</nav>