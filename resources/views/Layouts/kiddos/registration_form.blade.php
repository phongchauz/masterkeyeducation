<?php
use App\Helpers\BaseService;
$registration_form_label = isset($site_setting->registration_form_label) ? $site_setting->registration_form_label : "";
$registration_form_content = isset($site_setting->registration_form_content) ? $site_setting->registration_form_content : "";

$registration_form_picture = isset($site_setting->registration_form_picture) ? $site_setting->registration_form_picture : "";
$registration_form_picture_path = empty($registration_form_picture) ? $current_domain.'/images/no_img.jpg' :
    $current_domain.'/uploads/'.$company_id.'/SiteSetting/RegistrationFormPicture/'.$registration_form_picture.'?t='.SYSTEM_CACHE;

?>

<section class="ftco-section ftco-consult ftco-no-pt ftco-no-pb" style="background-image: url({{  $registration_form_picture_path }});" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-6 py-5 px-md-5 bg-primary">
                <div class="heading-section heading-section-white ftco-animate mb-5">
                    <h2 class="mb-4">{{ $registration_form_label }}</h2>
                    <p>{{ $registration_form_content }}</p>
                </div>
                <form id="form-customer-request" method="post" enctype="multipart/form-data" class="appointment-form ftco-animate">
                    <input class="hide" name="_token" value="{{ csrf_token() }}">
                    <div class="d-md-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" name="first_name" placeholder="{{ __("First name") }}">
                        </div>
                        <div class="form-group ml-md-4">
                            <input type="text" class="form-control" name="last_name" placeholder="{{ __("Last Name") }}">
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group">
                            <div class="form-field">
                                <div class="select-wrap">
                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                    <select name="" id="" class="form-control">
                                        <option value="0">{{ __("Select Your Course") }}</option>
                                        @foreach($listCourse as $course)
                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ml-md-4">
                            <input type="text" class="form-control" name="phone" placeholder="{{ __("Phone") }}">
                        </div>
                    </div>
                    <div class="d-md-flex">
                        <div class="form-group">
                            <textarea cols="30" rows="2" class="form-control" name="message" placeholder="{{ __("Message") }}"></textarea>
                        </div>
                        <div class="form-group ml-md-4">
                            <input type="submit" value="{{ __("Request A Quote") }}" class="btn btn-secondary py-3 px-4">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>