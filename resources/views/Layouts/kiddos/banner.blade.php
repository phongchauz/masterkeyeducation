<?php
use App\Helpers\BaseService;

?>
<section class="home-slider owl-carousel">

    @foreach($listData as $key => $item)

        <?php
            $id = $item->id;
            $title = $item->title;
            $prev_title = BaseService::splitString($title, 'prev');
            $last_title = trim(str_replace($prev_title, '', $title));

            $picture = $item->picture;
            $picture = empty($picture) ? asset('public/images/kiddos/banner_noimg.jpg'):
                asset('public/uploads/'.$item->company_id.'/Banner/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);

        ?>

    <div class="slider-item" style="background-image:url({{  $picture }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
                <div class="col-md-8 text-center ftco-animate" style="display: none">
                    <h1 class="mb-4">{{ $prev_title }} <span>{{ $last_title }}</span></h1>
                    <p><a href="javascript:;" style="display: none;" class="btn btn-secondary px-4 py-3 mt-3">{{ __("Read More") }}</a></p>
                </div>
            </div>
        </div>
    </div>

    @endforeach


</section>