<?php
    $listData = $listData ?? [];
$mission_label = isset($site_setting->mission_label) ? $site_setting->mission_label : "";
$mission_content = isset($site_setting->mission_content) ? $site_setting->mission_content : "";
?>

<style>
    .mission-img{
        height: 30px;
        width: 30px;
    }
</style>

<h2 class="mb-4">{{ $mission_label }}</h2>
<p>{{ $mission_content }}</p>
<div class="row mt-5">

    @foreach($listData as $key => $item)

        <?php
        $id = $item->id;
        $title = $item->title;
        $summary = $item->summary;

        $picture = $item->picture;
        $picture_path = empty($picture) ? asset('public/images/kiddos/course_noimg.jpg'):
            asset('public/uploads/'.$item->company_id.'/Mission/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);


        ?>

        <div class="col-lg-6">
            <div class="services-2 d-flex">
                <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                    <img class="mission-img" src="{{ $picture_path }}">
                </div>
                <div class="text">
                    <h3>{{ $title }}</h3>
                    <p>{{ $summary }}</p>
                </div>
            </div>
        </div>

    @endforeach




</div>

