<?php
use App\Helpers\BaseService;
$listData = $listData ?? [];
$teacher_label = isset($site_setting->teacher_label) ? $site_setting->teacher_label : "";
$teacher_content = isset($site_setting->teacher_content) ? $site_setting->teacher_content : "";

$prev_label = BaseService::splitString($teacher_label, 'prev');
$last_label = trim(str_replace($prev_label, '', $teacher_label));

?>
<section class="ftco-section ftco-no-pb">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <h2 class="mb-4"><span>{{ $prev_label }}</span> {{ $last_label }}</h2>
                <p>{{ $teacher_content }}</p>
            </div>
        </div>
        <div class="row">

            @foreach($listData as $key => $item)

                <?php
                $id = $item->id;
                $name = $item->name;
                $position = $item->position;
                $introduction = $item->introduction;
                $twitter_url = !empty($item->twitter_url) ? $item->twitter_url : "javascript:;";
                $facebook_url = !empty($item->facebook_url) ? $item->facebook_url : "javascript:;";
                $youtube_url = !empty($item->youtube_url) ? $item->youtube_url : "javascript:;";
                $instagram_url = !empty($item->instagram_url) ? $item->instagram_url : "javascript:;";

                $picture = $item->picture;
                $picture_path = empty($picture) ? asset('public/images/kiddos/teacher_noimg.jpg'):
                    asset('public/uploads/'.$item->company_id.'/Teacher/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);

                ?>

                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="staff">
                            <div class="img-wrap d-flex align-items-stretch">
                                <div class="img align-self-stretch" style="background-image: url({{  $picture_path }});"></div>
                            </div>
                            <div class="text pt-3 text-center">
                                <h3>{{ $name }}</h3>
                                <span class="position mb-2">{{ $position }}</span>
                                <div class="faded">
                                    <p>{{ $introduction }}</p>
                                    <ul class="ftco-social text-center">
                                        <li class="ftco-animate"><a href="{{ $twitter_url }}"><span class="icon-twitter"></span></a></li>
                                        <li class="ftco-animate"><a href="{{ $facebook_url }}"><span class="icon-facebook"></span></a></li>
                                        <li class="ftco-animate"><a href="{{ $youtube_url }}"><span class="fas fa-youtube"></span></a></li>
                                        <li class="ftco-animate"><a href="{{ $instagram_url }}"><span class="icon-instagram"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

            @endforeach



        </div>
    </div>
</section>