<?php
    $address        = isset($company['address']) ? $company['address'] : '';
    $phone_number   = isset($company['phone_number']) ? $company['phone_number'] : '';
    $email          = isset($company['email']) ? $company['email'] : '';

    $copyright      = isset($company['copyright']) ? $company['copyright'] : '';
    $copyright      = str_replace("#YYYY#", date("Y"), $copyright);

    $facebook_url   = isset($company['facebook_url']) ? $company['facebook_url'] : '';
    $twitter_url    = isset($company['twitter_url']) ? $company['twitter_url'] : '';
    $youtube_url    = isset($company['youtube_url']) ? $company['youtube_url'] : '';

    $facebook_url   = !empty($facebook_url) && (!strpos($facebook_url, 'http') || !strpos($facebook_url, 'https')) ? "https://".$facebook_url : $facebook_url;
    $twitter_url    = !empty($twitter_url) && (!strpos($twitter_url, 'http') || !strpos($twitter_url, 'https')) ? "https://".$twitter_url : $twitter_url;
    $youtube_url    = !empty($youtube_url) && (!strpos($youtube_url, 'http') || !strpos($youtube_url, 'https')) ? "https://".$youtube_url : $youtube_url;

?>
<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6 col-lg-6">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">Have a Questions?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">{{ $address }}</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">{{ $phone_number }}</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">{{ $email }}</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-md-6 col-lg-6">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">{{ __("Subscribe Us!") }}</h2>
                    <form method="post" id="form-subscribe" class="subscribe-form" enctype="multipart/form-data">
                        <input class="hide" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control mb-2 text-center" name="email" placeholder="{{ __("Enter email address") }}">
                            <input type="submit" value="Subscribe" class="form-control submit px-3">
                        </div>
                    </form>
                </div>
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2 mb-0">Connect With Us</h2>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                        <li class="ftco-animate"><a href="{{ $twitter_url }}"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="{{ $facebook_url }}"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="{{ $youtube_url }}"><span class="icon-youtube"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>{{ $copyright }}</p>
            </div>
        </div>
    </div>
</footer>