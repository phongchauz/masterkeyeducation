<?php
    $num = count($listData);
    $rate = $num == 0 ? 100 : 100/$num;
?>
<style>
    .core-value-img{
        width: 50px;
        height: 50px;
    }

    .col-core-value{
        width: <?php echo $rate?>% !important;
    }
</style>



<section class="ftco-services ftco-no-pb">
    <div class="container">
        <div class="row">

            @foreach($listData as $key => $item)

                <?php
                $id = $item->id;
                $title = $item->title;
                $slug = $item->slug;
                $summary = $item->summary;
                $background_color = !empty($item->background_color) ? $item->background_color : "#fff";

                $picture = $item->picture;
                $picture_path = empty($picture) ? asset('public/images/kiddos/core_value_noimg.jpg'):
                    asset('public/uploads/'.$item->company_id.'/CoreValue/'.$id.'/Picture/'.$picture.'?t='.CACHE_VARIABLE);

                ?>

                    <div class="col-core-value d-flex services align-self-stretch pb-4 px-4 ftco-animate " style="background-color: {{ $background_color }};">
                        <div class="media block-6 d-block text-center" style="width: 100%;">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <img class="core-value-img" src="{{ $picture_path }}">
                            </div>
                            <div class="media-body p-2 mt-3">
                                <h3 class="heading">{{ $title }}</h3>
                                <p><?php echo nl2br($summary);?></p>
                            </div>
                        </div>
                    </div>

            @endforeach


        </div>
    </div>
</section>