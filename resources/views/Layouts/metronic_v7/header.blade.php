<?php

use Illuminate\Support\Facades\Auth;

$authUser = Auth::user();

$avatar = $authUser->avatar;
$avatar_path = empty($avatar) ? $current_domain.'/img/avatar_default.jpg' :
    $current_domain.'/public/uploads/User/'.$authUser->id.'/avatar/'.$avatar;


?>

<style>
    .lbl-back{
        font-size: 13px;
        padding-left: 5px;
        font-weight: bold;
    }
</style>

<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">

        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
            <!--begin::Header Menu-->
            <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">

                <ul class="menu-nav">

                    <?php if(isset($back_url) ){?>

                        @if(isset($is_show_back_url) && $is_show_back_url)

                            <li class="menu-item menu-item-submenu menu-item-rel menu-item-active" >
                                <a href="{{ $back_url }}" class="menu-link">

                                    <span class="menu-text"><i class="fas fa-chevron-double-left"></i>  <span class="lbl-back">{{ __("Back") }}</span></span>

                                </a>

                            </li>
                        @endif

                        <?php } ?>

                </ul>
            </div>
            <!--end::Header Menu-->
        </div>

        <!--begin::Topbar-->
        <div class="topbar">

            <!--begin::Languages-->
            <div class="dropdown">
                <!--begin::Toggle-->
                <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                    <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
                        <img class="h-20px w-20px rounded-sm" src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/226-united-states.svg') }}" alt="" />
                    </div>
                </div>
                <!--end::Toggle-->
                <!--begin::Dropdown-->
                <div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
                    <!--begin::Nav-->
                    <ul class="navi navi-hover py-4">
                        <!--begin::Item-->
                        <li class="navi-item">
                            <a href="#" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<img src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/226-united-states.svg') }}" alt="" />
													</span>
                                <span class="navi-text">English</span>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="navi-item active">
                            <a href="#" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<img src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/128-spain.svg') }}" alt="" />
													</span>
                                <span class="navi-text">Spanish</span>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="navi-item">
                            <a href="#" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<img src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/162-germany.svg') }}" alt="" />
													</span>
                                <span class="navi-text">German</span>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="navi-item">
                            <a href="#" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<img src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/063-japan.svg') }}" alt="" />
													</span>
                                <span class="navi-text">Japanese</span>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="navi-item">
                            <a href="#" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<img src="{{  asset('public/assets/templates/metronicv7/assets/media/svg/flags/195-france.svg') }}" alt="" />
													</span>
                                <span class="navi-text">French</span>
                            </a>
                        </li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Nav-->
                </div>
                <!--end::Dropdown-->
            </div>
            <!--end::Languages-->
            <!--begin::User-->
            <div class="topbar-item">
                <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ $authUser->name }}</span>
                    <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                        <img src="{{ $avatar_path }}">
                    </span>
                </div>
            </div>
            <!--end::User-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>
