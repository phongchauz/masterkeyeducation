@extends("layouts.default")
@section('title', '')
@section("content")

    <!-- Category Megamenu & Banner start -->
    <div class="section">
        <div class="container">
            <div class="row">

                <!-- Menu Start -->
                <div class="col-lg-3">
                    <div class="andro_category-mm">
                        <div class="andro_category-mm-header">
                            <h6> <i class="fas fa-th-large"></i> Categories</h6>
                        </div>
                        <div class="andro_category-mm-body">

                            <ul>
                                <!-- Level 1 -->

                                <li class="andro_category-mm-item andro_category-mm-item-has-children">
                                    <a href="#"> Food </a>
                                    <ul class="andro_category-mm-2-cols">
                                        <!-- Level 2 -->
                                        <li>
                                            <div class="andro_category-mm-banner">
                                                <img src="https://androthemes.com/themes/html/organista/assets/img/megamenu/1.jpg" alt="megamenu img">
                                                <div class="andro_category-mm-banner-desc">
                                                    <h6>Food</h6>
                                                    <p>Experience organic food like never before</p>
                                                </div>
                                            </div>
                                            <ul>
                                                <li> <a href="#">Vegetables & Fruits</a> </li>
                                                <li> <a href="#">Dairy</a> </li>
                                                <li> <a href="#">Vegan Dairy</a> </li>
                                                <li> <a href="#">Meats & Fish</a> </li>
                                                <li> <a href="#">Breads & Cereal</a> </li>
                                                <li> <a href="#">Honey</a> </li>
                                                <li> <a href="#">Jam & Spreads</a> </li>
                                                <li> <a href="#">Legumes</a> </li>
                                                <li> <a href="#">Oils</a> </li>
                                            </ul>
                                            <ul>
                                                <li> <a href="#">Beans</a> </li>
                                                <li> <a href="#">Vegan Food</a> </li>
                                                <li> <a href="#">Distillates</a> </li>
                                                <li> <a href="#">Eggs</a> </li>
                                                <li> <a href="#">Snacks</a> </li>
                                                <li> <a href="#">Syrups</a> </li>
                                                <li> <a href="#">Pickles</a> </li>
                                                <li> <a href="#">Wines & Spirit</a> </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="andro_category-mm-item"> <a href="#"> Keto</a> </li>

                                <li class="andro_category-mm-item andro_category-mm-item-has-children">
                                    <a href="#"> Baskets </a>
                                    <ul class="andro_category-mm-2-cols">
                                        <!-- Level 2 -->
                                        <li>
                                            <div class="andro_category-mm-banner">
                                                <img src="https://androthemes.com/themes/html/organista/assets/img/megamenu/2.jpg" alt="megamenu img">
                                                <div class="andro_category-mm-banner-desc">
                                                    <h6>Baskets</h6>
                                                    <p>Hand made baskets for your organic goods</p>
                                                </div>
                                            </div>
                                            <ul>
                                                <li> <a href="#">Vegetables & Fruits</a> </li>
                                                <li> <a href="#">Dairy</a> </li>
                                                <li> <a href="#">Vegan Dairy</a> </li>
                                                <li> <a href="#">Meats & Fish</a> </li>
                                                <li> <a href="#">Breads & Cereal</a> </li>
                                            </ul>
                                            <ul>
                                                <li> <a href="#">Honey</a> </li>
                                                <li> <a href="#">Jam & Spreads</a> </li>
                                                <li> <a href="#">Legumes</a> </li>
                                                <li> <a href="#">Oils</a> </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="andro_category-mm-item"> <a href="#"> Baby & Kids Care</a> </li>
                                <li class="andro_category-mm-item"> <a href="#"> Home Care</a> </li>
                                <li class="andro_category-mm-item"> <a href="#"> Supplements</a> </li>
                                <li class="andro_category-mm-item"> <a href="#"> Beauty Care</a> </li>
                                <li class="andro_category-mm-item andro_category-mm-item-has-children andro_category-mm-item-expand">
                                    <a href="#"> <i class="fas fa-plus"></i> More Categories</a>
                                    <ul class="andro_category-mm-2-cols">
                                        <!-- Level 2 -->
                                        <li>
                                            <div class="andro_category-mm-banner">
                                                <img src="https://androthemes.com/themes/html/organista/assets/img/megamenu/3.jpg" alt="megamenu img">
                                                <div class="andro_category-mm-banner-desc">
                                                    <h6>More Categories</h6>
                                                    <p>Explore more categories that we offer</p>
                                                </div>
                                            </div>
                                            <ul>
                                                <li> <a href="#">Vegetables & Fruits</a> </li>
                                                <li> <a href="#">Dairy</a> </li>
                                                <li> <a href="#">Vegan Dairy</a> </li>
                                                <li> <a href="#">Meats & Fish</a> </li>
                                                <li> <a href="#">Breads & Cereal</a> </li>
                                            </ul>
                                            <ul>
                                                <li> <a href="#">Honey</a> </li>
                                                <li> <a href="#">Jam & Spreads</a> </li>
                                                <li> <a href="#">Legumes</a> </li>
                                                <li> <a href="#">Oils</a> </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                            </ul>

                        </div>

                    </div>
                </div>
                <!-- Menu End -->

                <!-- Banner Start -->
                <div class="col-lg-9">

                    <div class="andro_flex-menu d-none d-md-block">
                        <ul>
                            <li class="menu-item"> <a href="shop-v1.html">Daily Deals</a> </li>
                            <li class="menu-item menu-item-has-children">
                                <a href="#">Top Sellers</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"> <a href="#">Food</a> </li>
                                    <li class="menu-item"> <a href="#">Baskets</a> </li>
                                    <li class="menu-item"> <a href="#">Keto</a> </li>
                                </ul>
                            </li>
                            <li class="menu-item"> <a href="shop-v3.html">Fresh Arrivals</a> </li>
                            <li class="menu-item menu-item-has-children">
                                <a href="#">Gift Carts</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"> <a href="#">Loyalty System</a> </li>
                                    <li class="menu-item"> <a href="#">Redemption Coupons</a> </li>
                                </ul>
                            </li>
                            <li class="menu-item"> <a href="wishlist.html">Wishlist</a> </li>
                        </ul>
                    </div>

                    <div class="andro_banner banner-1">
                        <div class="andro_banner-slider">
                            <div class="container-fluid">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                        <p>Use code <strong class="custom-primary">ORGANIC991</strong> during checkout</p>
                                        <h1> 40% Sale <span class="fw-400">On Select Products</span> </h1>
                                        <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh. Vestibulum ac diam sit </p>
                                        <a href="shop-v1.html" class="andro_btn-custom">Shop Now</a>
                                    </div>
                                    <div class="col-lg-6 d-none d-lg-block">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/products/2.png" alt="banner img">
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                        <p>Use code <strong class="custom-primary">ORGANIC991</strong> during checkout</p>
                                        <h1> 40% Sale <span class="fw-400">On Select Products</span> </h1>
                                        <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh. Vestibulum ac diam sit</p>
                                        <a href="shop-v1.html" class="andro_btn-custom">Shop Now</a>
                                    </div>
                                    <div class="col-lg-6 d-none d-lg-block">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/products/13.png" alt="banner img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Banner End -->

            </div>
        </div>
    </div>
    <!-- Category Megamenu & Banner end -->

    <!-- Icons Start -->
    <div class="section section-padding pt-0">
        <div class="container">
            <div class="row">

                <div class="col-lg-4">
                    <div class="andro_icon-block">
                        <i class="flaticon-diet"></i>
                        <h5>Guaranteed Organic</h5>
                        <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <svg xmlns="https://www.w3.org/2000/svg">
                            <rect height="500" width="500" class="andro_svg-stroke-shape-anim"></rect>
                        </svg>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="andro_icon-block">
                        <i class="flaticon-harvest"></i>
                        <h5>Daily Harvesting</h5>
                        <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <svg xmlns="https://www.w3.org/2000/svg">
                            <rect height="500" width="500" class="andro_svg-stroke-shape-anim"></rect>
                        </svg>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="andro_icon-block">
                        <i class="flaticon-tag"></i>
                        <h5>Cheap & Healthy</h5>
                        <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <svg xmlns="https://www.w3.org/2000/svg">
                            <rect height="500" width="500" class="andro_svg-stroke-shape-anim"></rect>
                        </svg>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Icons End -->

    <!-- Products Start -->
    <div class="section pt-0 andro_fresh-arrivals">
        <div class="container">

            <div class="section-title flex-title">
                <h4 class="title">Fresh Arrivals</h4>
                <div class="andro_arrows">
                    <i class="fa fa-arrow-left slick-arrow slider-prev"></i>
                    <i class="fa fa-arrow-right slick-arrow slider-next"></i>
                </div>
            </div>

            <div class="andro_fresh-arrivals-slider">

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-featured">
                        <i class="fa fa-star"></i>
                        <span>Featured</span>
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/2.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Kiwi & Strawberry </a> </h5>
                        <div class="andro_product-price">
                            <span>19$</span>
                            <span>29$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <span>4 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-featured">
                        <i class="fa fa-star"></i>
                        <span>Featured</span>
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/6.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Fresh Oranges </a> </h5>
                        <div class="andro_product-price">
                            <span>19$</span>
                            <span>29$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                            </div>
                            <span>5 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-sale">
                        20% Off
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/4.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Cucumbers </a> </h5>
                        <div class="andro_product-price">
                            <span>8$</span>
                            <span>14$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <span>4 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-featured">
                        <i class="fa fa-star"></i>
                        <span>Featured</span>
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/2.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Kiwi & Strawberry </a> </h5>
                        <div class="andro_product-price">
                            <span>19$</span>
                            <span>29$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <span>4 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-featured">
                        <i class="fa fa-star"></i>
                        <span>Featured</span>
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/6.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Fresh Oranges </a> </h5>
                        <div class="andro_product-price">
                            <span>11$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                            </div>
                            <span>5 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="andro_product andro_product-has-controls andro_product-has-buttons">
                    <div class="andro_product-badge andro_badge-sale">
                        20% Off
                    </div>
                    <div class="andro_product-thumb">
                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/4.png" alt="product"></a>
                    </div>
                    <div class="andro_product-body">
                        <h5 class="andro_product-title"> <a href="product-single.html"> Cucumbers </a> </h5>
                        <div class="andro_product-price">
                            <span>8$</span>
                            <span>14$</span>
                        </div>
                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        <div class="andro_rating-wrapper">
                            <div class="andro_rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <span>4 Stars</span>
                        </div>
                    </div>
                    <div class="andro_product-footer">
                        <div class="andro_product-buttons">
                            <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                            <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

            </div>
        </div>
    </div>
    <!-- Products End -->

    <!-- Featured Products Start -->
    <div class="section section-padding pt-0">
        <div class="container">

            <div class="row">
                <div class="col-lg-8">

                    <div class="section-title">
                        <h4 class="title">Featured Products</h4>
                    </div>

                    <div class="row">

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/7.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Ginger </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>19$</span>
                                        <span>29$</span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/8.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Lettuce </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>8$</span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/9.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Tomatoes </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>6$</span>
                                        <span></span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/10.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Corn </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>19$</span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/11.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Onions </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>13$</span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                        <!-- Product Start -->
                        <div class="col-sm-6">
                            <div class="andro_product andro_product-minimal andro_product-has-controls andro_product-has-buttons">
                                <div class="andro_product-thumb">
                                    <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/12.png" alt="product"></a>
                                </div>
                                <div class="andro_product-body">
                                    <h6 class="andro_product-title"> <a href="product-single.html"> Brocolis </a> </h6>
                                    <div class="andro_rating-wrapper">
                                        <div class="andro_rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="andro_product-footer">
                                    <div class="andro_product-price">
                                        <span>19$</span>
                                        <span>29$</span>
                                    </div>
                                    <div class="andro_product-buttons">
                                        <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product End -->

                    </div>

                </div>

                <!-- Sidebar Start -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <!-- Product Categories Start -->
                        <div class="sidebar-widget">
                            <div class="section-title">
                                <h4 class="title">Popular Categories</h4>
                            </div>
                            <ul class="sidebar-widget-list">
                                <li>
                                    <a href="#"> Food <span>(32)</span> </a>
                                    <ul>
                                        <li> <a href="#">Vegetables & Fruits <span>(14)</span> </a> </li>
                                        <li> <a href="#">Dairy <span>(39)</span></a> </li>
                                        <li> <a href="#">Vegan <span>(54)</span> </a> </li>
                                    </ul>
                                </li>
                                <li> <a href="#"> Keto <span>(24)</span> </a> </li>
                                <li> <a href="#"> Baskets <span>(44)</span> </a> </li>
                                <li> <a href="#"> Baby & Kids Care <span>(15)</span> </a> </li>
                                <li> <a href="#"> Home Care <span>(43)</span> </a> </li>
                                <li> <a href="#"> Supplements <span>(75)</span> </a> </li>
                            </ul>

                        </div>
                        <!-- Product Categories End -->

                        <!-- Daily Deals Start -->
                        <div class="sidebar-widget">
                            <div class="section-title">
                                <h4 class="title">Daily Deals</h4>
                            </div>

                            <div class="deals-slider">

                                <!-- Deal Start -->
                                <div class="andro_product">
                                    <div class="andro_product-badge andro_badge-sale">
                                        20% Off
                                    </div>
                                    <div class="andro_product-thumb">
                                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/5.png" alt="product"></a>
                                        <div class="andro_countdown-timer" data-countdown="2021/01/01"></div>
                                    </div>
                                    <div class="andro_product-body">
                                        <h5 class="andro_product-title"> <a href="product-single.html"> Watermelon </a> </h5>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                        <div class="andro_rating-wrapper">
                                            <div class="andro_rating">
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span>4 Stars</span>
                                        </div>
                                    </div>
                                    <div class="andro_product-footer">
                                        <div class="andro_product-price">
                                            <span>8$</span>
                                            <span>14$</span>
                                        </div>
                                        <a href="product-single.html" class="andro_btn-custom btn-sm">Buy Now</a>
                                    </div>

                                </div>
                                <!-- Deal End -->

                                <!-- Deal Start -->
                                <div class="andro_product">
                                    <div class="andro_product-badge andro_badge-sale">
                                        10% Off
                                    </div>
                                    <div class="andro_product-thumb">
                                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/4.png" alt="product"></a>
                                        <div class="andro_countdown-timer" data-countdown="2021/05/06"></div>
                                    </div>
                                    <div class="andro_product-body">
                                        <h5 class="andro_product-title"> <a href="product-single.html"> Cucumbers </a> </h5>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                        <div class="andro_rating-wrapper">
                                            <div class="andro_rating">
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span>4 Stars</span>
                                        </div>
                                    </div>
                                    <div class="andro_product-footer">
                                        <div class="andro_product-price">
                                            <span>8$</span>
                                            <span>14$</span>
                                        </div>
                                        <a href="product-single.html" class="andro_btn-custom btn-sm">Buy Now</a>
                                    </div>

                                </div>
                                <!-- Deal End -->

                                <!-- Deal Start -->
                                <div class="andro_product">
                                    <div class="andro_product-badge andro_badge-sale">
                                        20% Off
                                    </div>
                                    <div class="andro_product-thumb">
                                        <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/6.png" alt="product"></a>
                                        <div class="andro_countdown-timer" data-countdown="2021/09/09"></div>
                                    </div>
                                    <div class="andro_product-body">
                                        <h5 class="andro_product-title"> <a href="product-single.html"> Oranges </a> </h5>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                        <div class="andro_rating-wrapper">
                                            <div class="andro_rating">
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span>4 Stars</span>
                                        </div>
                                    </div>
                                    <div class="andro_product-footer">
                                        <div class="andro_product-price">
                                            <span>8$</span>
                                            <span>14$</span>
                                        </div>
                                        <a href="product-single.html" class="andro_btn-custom btn-sm">Buy Now</a>
                                    </div>

                                </div>
                                <!-- Deal End -->

                            </div>

                        </div>
                        <!-- Daily Deals End -->

                        <!-- Newsletter Start -->
                        <div class="sidebar-widget">
                            <div class="andro_newsletter-form">
                                <h5>Join our newsletter</h5>
                                <p>Get exclusive weekly deals with our newsletter subscription</p>
                                <form method="post">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="newsletter_email" placeholder="Email Address" value="">
                                    </div>
                                    <button type="submit" class="andro_btn-custom secondary btn-block shadow-none" name="button">Join Newsletter</button>
                                </form>
                            </div>
                        </div>
                        <!-- Newsletter End -->

                    </div>
                </div>
                <!-- Sidebar End -->

            </div>

        </div>
    </div>
    <!-- Featured Products End -->

    <!-- Call to action Start -->
    <div class="section pt-0">
        <div class="container">
            <div class="andro_cta-notice secondary-bg pattern-bg">
                <div class="andro_cta-notice-inner">
                    <h3 class="text-white">Buy Today and Get 20% Off</h3>
                    <p class="text-white">Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                    <a href="#" class="andro_btn-custom light">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Call to action End -->

    <!-- Top Picks Start -->
    <div class="section section-padding pt-0">
        <div class="container">

            <div class="section-title">
                <h4 class="title">Top Picks</h4>
            </div>

            <div class="row">

                <!-- Product Start -->
                <div class="col-lg-6">
                    <div class="andro_product andro_product-list andro_product-has-controls andro_product-has-buttons">
                        <div class="andro_product-thumb">
                            <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/2.png" alt="product"></a>
                        </div>
                        <div class="andro_product-body">
                            <div class="andro_rating-wrapper">
                                <div class="andro_rating">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <span>4 Stars</span>
                            </div>
                            <h5 class="andro_product-title"> <a href="product-single.html"> Kiwi & Strawberry </a> </h5>
                            <div class="andro_product-price">
                                <span>19$</span>
                                <span>29$</span>
                            </div>
                            <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>

                        </div>
                        <div class="andro_product-footer">
                            <div class="andro_product-buttons">
                                <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="col-lg-6">
                    <div class="andro_product andro_product-list andro_product-has-controls andro_product-has-buttons">
                        <div class="andro_product-thumb">
                            <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/5.png" alt="product"></a>
                        </div>
                        <div class="andro_product-body">
                            <div class="andro_rating-wrapper">
                                <div class="andro_rating">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                </div>
                                <span>5 Stars</span>
                            </div>
                            <h5 class="andro_product-title"> <a href="product-single.html"> Watermelons </a> </h5>
                            <div class="andro_product-price">
                                <span>11$</span>
                            </div>
                            <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        </div>
                        <div class="andro_product-footer">
                            <div class="andro_product-buttons">
                                <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="col-lg-6">
                    <div class="andro_product andro_product-list andro_product-has-controls andro_product-has-buttons">
                        <div class="andro_product-thumb">
                            <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/3.png" alt="product"></a>
                        </div>
                        <div class="andro_product-body">
                            <div class="andro_rating-wrapper">
                                <div class="andro_rating">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                </div>
                                <span>5 Stars</span>
                            </div>
                            <h5 class="andro_product-title"> <a href="product-single.html"> Kiwi </a> </h5>
                            <div class="andro_product-price">
                                <span>17$</span>
                            </div>
                            <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        </div>
                        <div class="andro_product-footer">
                            <div class="andro_product-buttons">
                                <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="col-lg-6">
                    <div class="andro_product andro_product-list andro_product-has-controls andro_product-has-buttons">
                        <div class="andro_product-badge andro_badge-featured">
                            <i class="fa fa-star"></i>
                            <span>Featured</span>
                        </div>
                        <div class="andro_product-thumb">
                            <a href="product-single.html"><img src="https://androthemes.com/themes/html/organista/assets/img/products/6.png" alt="product"></a>
                        </div>
                        <div class="andro_product-body">
                            <div class="andro_rating-wrapper">
                                <div class="andro_rating">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                </div>
                                <span>5 Stars</span>
                            </div>
                            <h5 class="andro_product-title"> <a href="product-single.html"> Oranges </a> </h5>
                            <div class="andro_product-price">
                                <span>23$</span>
                            </div>
                            <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                        </div>
                        <div class="andro_product-footer">
                            <div class="andro_product-buttons">
                                <a href="#" class="andro_btn-custom primary">Add To Cart</a>
                                <a href="#" data-toggle="modal" data-target="#quickViewModal" class="andro_btn-custom light">Quick View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product End -->

            </div>
        </div>
    </div>
    <!-- Top Picks End -->

    <!-- Blog Posts Start -->
    <div class="section pt-0">
        <div class="container">

            <div class="row">
                <div class="col-lg-8">

                    <div class="section-title">
                        <h4 class="title">From Our Blog</h4>
                    </div>

                    <div class="row">

                        <!-- Article Start -->
                        <div class="col-md-6">
                            <article class="andro_post">
                                <div class="andro_post-thumb">
                                    <a href="post-single.html">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/blog/1.jpg" alt="post">
                                    </a>
                                </div>
                                <div class="andro_post-body">
                                    <div class="andro_post-categories">
                                        <a href="#">Food</a>
                                        <a href="#">Veg</a>
                                    </div>
                                    <div class="andro_post-desc">
                                        <h5> <a href="post-single.html">Difference Is In the Soil</a> </h5>
                                        <span> <span class="fw-600">Posted On</span> <a href="post-single.html" class="andro_post-date">May 20, 2021</a> </span>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                    </div>
                                    <a class="andro_btn-custom btn-block" href="post-single.html"> Read More </a>
                                </div>
                            </article>
                        </div>
                        <!-- Article End -->

                        <!-- Article Start -->
                        <div class="col-md-6">
                            <article class="andro_post">
                                <div class="andro_post-thumb">
                                    <a href="post-single.html">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/blog/2.jpg" alt="post">
                                    </a>
                                </div>
                                <div class="andro_post-body">
                                    <div class="andro_post-categories">
                                        <a href="#">Health</a>
                                    </div>
                                    <div class="andro_post-desc">
                                        <h5> <a href="post-single.html">Planting Season Begins</a> </h5>
                                        <span> <span class="fw-600">Posted On</span> <a href="post-single.html" class="andro_post-date">May 20, 2021</a> </span>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                    </div>
                                    <a class="andro_btn-custom btn-block" href="post-single.html"> Read More </a>
                                </div>
                            </article>
                        </div>
                        <!-- Article End -->

                        <!-- Article Start -->
                        <div class="col-md-6">
                            <article class="andro_post">
                                <div class="andro_post-thumb">
                                    <a href="post-single.html">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/blog/3.jpg" alt="post">
                                    </a>
                                </div>
                                <div class="andro_post-body">
                                    <div class="andro_post-categories">
                                        <a href="#">Food</a>
                                        <a href="#">Dishes</a>
                                    </div>
                                    <div class="andro_post-desc">
                                        <h5> <a href="post-single.html">Freshly Picked Organics</a> </h5>
                                        <span> <span class="fw-600">Posted On</span> <a href="post-single.html" class="andro_post-date">May 20, 2021</a> </span>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                    </div>
                                    <a class="andro_btn-custom btn-block" href="post-single.html"> Read More </a>
                                </div>
                            </article>
                        </div>
                        <!-- Article End -->

                        <!-- Article Start -->
                        <div class="col-md-6">
                            <article class="andro_post">
                                <div class="andro_post-thumb">
                                    <a href="post-single.html">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/blog/4.jpg" alt="post">
                                    </a>
                                </div>
                                <div class="andro_post-body">
                                    <div class="andro_post-categories">
                                        <a href="#">Food</a>
                                    </div>
                                    <div class="andro_post-desc">
                                        <h5> <a href="post-single.html">Make the Perfect Basket</a> </h5>
                                        <span> <span class="fw-600">Posted On</span> <a href="post-single.html" class="andro_post-date">May 20, 2021</a> </span>
                                        <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                                    </div>
                                    <a class="andro_btn-custom btn-block" href="post-single.html"> Read More </a>
                                </div>
                            </article>
                        </div>
                        <!-- Article End -->

                    </div>

                </div>

                <!-- Sidebar Start -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <!-- Testimonials Start -->
                        <div class="sidebar-widget">

                            <div class="section-title">
                                <h4 class="title">Testomonials</h4>
                            </div>

                            <div class="andro_testimonials">
                                <div class="andro_testimonial-item">
                                    <div class="andro_testimonials-inner">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/people/4.jpg" alt="author">
                                        <h5>Jina Flock</h5>
                                        <span>Farmer</span>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
                                    </div>
                                    <ul class="andro_sm">
                                        <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
                                    </ul>
                                </div>
                                <div class="andro_testimonial-item">
                                    <div class="andro_testimonials-inner">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/people/3.jpg" alt="author">
                                        <h5>Miranda Blue</h5>
                                        <span>Chemist</span>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
                                    </div>
                                    <ul class="andro_sm">
                                        <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
                                    </ul>
                                </div>
                                <div class="andro_testimonial-item">
                                    <div class="andro_testimonials-inner">
                                        <img src="https://androthemes.com/themes/html/organista/assets/img/people/2.jpg" alt="author">
                                        <h5>Feleciti Rolling</h5>
                                        <span>Farmer</span>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
                                    </div>
                                    <ul class="andro_sm">
                                        <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li>
                                        <li> <a href="#"> <i class="fab fa-youtube"></i> </a> </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- Testimonials End -->

                        <!-- Image Start -->
                        <div class="sidebar-widget d-none d-lg-block">
                            <div class="andro_auth-mini">
                                <h5>Login</h5>
                                <p>Get crazy benefits today by joining our community</p>
                                <form method="post">

                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Username" name="username" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Password" name="password" value="">
                                    </div>

                                    <div class="form-group text-center">
                                        <a href="#">Forgot Password?</a>
                                    </div>

                                    <button type="submit" class="andro_btn-custom primary btn-block">Login</button>

                                    <div class="andro_auth-seperator">
                                        <span>OR</span>
                                    </div>

                                    <div class="andro_social-login">
                                        <button type="button" class="andro_social-login-btn facebook"><i class="fab fa-facebook-f"></i> Continue with Facebook </button>
                                        <button type="button" class="andro_social-login-btn google"><i class="fab fa-google"></i> Continue with Google</button>
                                    </div>

                                    <p class="mb-0 text-center">Don't have an account? <a href="register.html">Create One</a> </p>

                                </form>
                            </div>
                        </div>
                        <!-- Image End -->

                    </div>
                </div>
                <!-- Sidebar End -->

            </div>
        </div>
    </div>
    <!-- Blog Posts End -->

    <!-- Instagram Start -->
    <div class="row no-gutters">
        <div class="col-lg-4 secondary-bg pattern-bg">
            <div class="andro_instagram">
                <h3 class="text-white">Follow Us On Instagram</h3>
                <a href="#" class="andro_btn-custom light">Follow Us</a>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="row no-gutters">
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/1.jpg" alt="ig">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/2.jpg" alt="ig">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/3.jpg" alt="ig">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/4.jpg" alt="ig">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/5.jpg" alt="ig">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                    <a href="#" class="andro_ig-item">
                        <img src="https://androthemes.com/themes/html/organista/assets/img/ig/6.jpg" alt="ig">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Instagram End -->

@endsection
