@extends("Layouts.".$template)
@section('title', "Test")
@section("content")

    <?php

    $label = isset($site_setting->teacher_label) ? $site_setting->teacher_label : "";
    $content = isset($site_setting->teacher_content) ? $site_setting->teacher_content : "";
    $picture = isset($site_setting->teacher_picture) ? $site_setting->teacher_picture : "";
    $picture_path = empty($picture) ? $current_domain.'/images/no_img.jpg' :
        $current_domain.'/uploads/'.$company_id.'/SiteSetting/TeacherPicture/'.$picture.'?t='.SYSTEM_CACHE;


    ?>


    <section class="hero-wrap hero-wrap-2" style="background-image: url({{ $picture_path }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">{{ $label }}</h1>
                    <p class="breadcrumbs">
                        <span class="mr-2">
                            <a href="{{ url("/") }}">{{ __("Home") }} <i class="ion-ios-arrow-forward"></i></a>
                        </span>
                        <span>{{ __("Teachers") }} </span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pb">
        <div class="container">
            <div class="row">

                @foreach($listTeacher as $key => $item)

                    <?php
                    $id             = $item->id;
                    $code           = $item->code;
                    $name           = $item->name;
                    $position       = $item->position;
                    $introduction   = $item->introduction;
                    $picture        = $item->picture;

                    $teacher_path = url("teachers/".$code);
                    $picture_path = empty($picture) ? $current_domain.'/images/no_img.jpg' :
                        $current_domain.'/uploads/'.$company_id.'/Teacher/'.$id."/Picture/".$picture.'?t='.SYSTEM_CACHE;
                    ?>

                        <div class="col-md-6 col-lg-3 ftco-animate">
                            <div class="staff">
                                <div class="img-wrap d-flex align-items-stretch">
                                    <div class="img align-self-stretch" style="background-image: url({{ $picture_path }});"></div>
                                </div>
                                <div class="text pt-3 text-center">
                                    <h3>{{ $name }}</h3>
                                    <span class="position mb-2">{{ $position }}</span>
                                    <div class="faded">
                                        <p>{{ $introduction }}</p>
                                        <ul class="ftco-social text-center">
                                            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                            <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                                            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                @endforeach



            </div>
        </div>
    </section>






@endsection

@section("pagescript")

@endsection


